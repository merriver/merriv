#pragma once

#include "IEntity.h"

enum State
{
	SHOT,
	RETRACT

};


class sf::Sprite;
class Player;

class Harpoon : public IEntity
{
public:
	Harpoon(AnimatedTexture* p_pxAnimatedSprite, sf::RenderWindow* p_pxWindow, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	~Harpoon();
	void Update(float p_fDeltaTime, Player* p_pxPlayer);
	void UpdateShot(float p_fDeltaTime);
	void UpdateRetract(float p_fDeltaTime, Player* p_pxPlayer);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	int GetState();
	void SetState(int p_iNewState);
	bool IsVisible();
	EENTITYTYPE GetType();



private:
	Harpoon() {};
	AnimatedTexture* m_pxAnimatedTexture;
	sf::Sprite m_pxSprite;
	float m_fAngle;
	float m_fX;
	float m_fY;
	float m_fVelocity = 0;
	int m_iState;
	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bVisible;


};