#pragma once
#include "IState.h"
#include "stdafx.h"
#define MAX_NUMBER_OF_ITEMS 3


class DrawManager;


class LoseState : public IState
{
public:
	LoseState(System& p_xSystem);
	~LoseState();

	void Draw();
	void MoveUp();
	void MoveDown();
	bool Update(float p_fDeltaTime);
	int GetPressedItem() { return selectedItemIndex; }
	IState* NextState();

private:
	int selectedItemIndex;
	int width = 1920;
	int height = 1080;
	int fontSize = 100;
	int selectedFontSize = 150;
	int m_iNextState;
	int m_iScore;
	bool m_bButtonBoolPlay = true;
	bool m_bButtonBoolExit = true;
	bool m_bButtonBoolMenu = true;
	bool m_bQuit = false;
	std::string m_sEnterName;
	sf::Font font;
	sf::Sound* m_xButtonSound;
	sf::Texture m_xCursor;
	sf::Texture m_xMenuTexture;
	sf::Texture m_xNameBarTexture;
	sf::Texture m_xButtonTexture;
	sf::Sprite m_xPlayAgainSprite;
	sf::Sprite m_xExit;
	sf::Sprite m_xMainMenuSprite;
	sf::Sprite m_xNameBarSprite;
	sf::Sprite m_xCursorSprite;
	sf::Sprite m_xMenuSprite;
	sf::Text menu[MAX_NUMBER_OF_ITEMS];
	sf::Text lost;
	sf::Text m_xScoreName;
	sf::Text m_xScoreText;
	sf::Text m_xYourScore;
	sf::String m_xScoreString;
	std::ofstream m_xOutName;
	std::ofstream m_xOutScore;
	sf::Event m_xEvent;
	//sf::String m_sHighScoreName;
	std::string m_sName;
	System m_xSystem;
	
};
#pragma once
