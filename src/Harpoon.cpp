#include "stdafx.h"
#include "Harpoon.h"
#include "AnimatedTexture.h"
#include "Player.h"

Harpoon::Harpoon(AnimatedTexture* p_pxAnimatedTexture, sf::RenderWindow* p_pxWindow, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxAnimatedTexture = p_pxAnimatedTexture;
	m_pxSprite.setTexture(p_pxAnimatedTexture->GetTexture());
	m_pxSprite.setOrigin(sf::Vector2f(100, 50));

	m_fX = p_fX;
	m_fY = p_fY;
	m_fVelocity = 10;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_iState = SHOT;

	sf::Vector2i mousePosition = sf::Mouse::getPosition(*p_pxWindow);
	m_fAngle = atan2(mousePosition.y - m_fY , mousePosition.x - m_fX);

}
Harpoon::~Harpoon()
{
	
	m_pxSprite.setRotation(0);
	m_pxAnimatedTexture = nullptr;

		
	
}

void Harpoon::Update(float p_fDeltaTime, Player* p_pxPlayer)
{
	
	
	if (m_iState == SHOT)
	{
		UpdateShot(p_fDeltaTime);
	}
	if (m_iState == RETRACT)
	{
		UpdateRetract(p_fDeltaTime, p_pxPlayer);
	}

	if(!p_pxPlayer->GetGolden())
	{
		if (m_fX < 0)
		{
		
			m_fX = 0;
			SetState(RETRACT);
	
		}
		if (m_fX + (m_pxSprite.getTextureRect().width / 2) > m_iScreenWidth)
		{
			m_fX = m_iScreenWidth - (m_pxSprite.getTextureRect().width / 2);
			SetState(RETRACT);
		}
		if (m_fY < 0)
		{
			m_fY = 0;
			SetState(RETRACT);
		}
		if ((m_fY + (m_pxSprite.getTextureRect().height / 2)) > m_iScreenHeight)
		{
			m_fY = m_iScreenHeight - (m_pxSprite.getTextureRect().height / 2);
			SetState(RETRACT);
		}
	}
}

void Harpoon::UpdateShot(float p_fDeltaTime)
{
	float cosine = cos(m_fAngle);
	float sine = sin(m_fAngle);
	m_pxSprite.setPosition(sf::Vector2f(m_fX += cosine * m_fVelocity, m_fY += sine * m_fVelocity));


	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setRotation(m_fAngle * 180.0f / M_PI);
	m_pxSprite.setScale(0.5, 0.5);
}
void Harpoon::UpdateRetract(float p_fDeltaTime, Player* p_pxPlayer)
{
	m_fVelocity = 8;
	m_fAngle = atan2(p_pxPlayer->GetY()- 10 + (p_pxPlayer->GetSprite().getTextureRect().height / 2) - m_fY, p_pxPlayer->GetX() + p_pxPlayer->GetSprite().getTextureRect().width-20  - m_fX);
	float cosine = cos(m_fAngle);
	float sine = sin(m_fAngle);
	m_pxSprite.setPosition(sf::Vector2f(m_fX += cosine * m_fVelocity, m_fY += sine * m_fVelocity));


	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setRotation((m_fAngle * 180.0f / M_PI) + 180);
	m_pxSprite.setScale(0.5, 0.5);
}
int Harpoon::GetState()
{
	return m_iState;
}

void Harpoon::SetState(int p_iNewState)
{
	m_iState = p_iNewState;
}

sf::Sprite Harpoon::GetSprite() { return m_pxSprite; }

float Harpoon::GetX() { return m_fX; }

float Harpoon::GetY() { return m_fY; }

bool Harpoon::IsVisible() { return m_fX; }

EENTITYTYPE Harpoon::GetType() { return EENTITYTYPE::ENTITY_HARPOON; }