#pragma once
 
 
 #include "IEntity.h"
 
 class sf::Sprite;
 class SpriteManager;
 
 class Amulet : public IEntity
 {
 public:
	 Amulet(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight);
	 ~Amulet();
	 void Update(float p_fDeltaTime);
	 sf::Sprite GetSprite();
	 EENTITYTYPE GetType();
	 float GetX();
	 float GetY();

 private:
	 AnimatedTexture* m_pxAnimatedTexture;
	 sf::Sprite m_pxSprite;
	 float m_fX;
	 float m_fY;
	 float PosX;
	 float PosY;
	 bool m_bVisible;
 };