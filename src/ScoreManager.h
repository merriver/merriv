#pragma once


class ScoreManager
{
public:
	ScoreManager();
	~ScoreManager();
	void SetHighScore(int p_iHighScore);
	int GetHighScore();

private:
	
	int m_iHighScore = 0;
};