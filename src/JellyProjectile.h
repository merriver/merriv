#pragma once
#include "IEntity.h"


class sf::Sprite;
class Player;
class SpriteManager;
class JellyRoger;


class JellyProjectile : public IEntity
{
public:
	JellyProjectile();
	~JellyProjectile();
	bool Initialize(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight);
	void Update(float p_fDeltaTime);
	sf::Sprite GetSprite();
	AnimatedTexture* GetTexture();
	float GetX();
	float GetY();
	bool IsActive();
	void Deactivate();
	void Activate(int p_fX, int p_fY);
	EENTITYTYPE GetType();
private:
	
	JellyRoger* m_pxJellyRoger;
	AnimatedTexture* m_pxAnimatedTextureDefault;
	sf::Sprite m_pxSprite;
	float m_fSpeed;
	float m_fDirY;
	float m_fDirX;
	float m_fX;
	float m_fY;
	float m_fAngle;
	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bActive = false;
};