#include "stdafx.h"
#include "ScoreManager.h"

ScoreManager::ScoreManager()
{
}

ScoreManager::~ScoreManager()
{
}

void ScoreManager::SetHighScore(int p_iHighScore)
{
	m_iHighScore = p_iHighScore;
}

int ScoreManager::GetHighScore()
{
	return m_iHighScore;
}
