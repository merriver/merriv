/*#include "stdafx.h"
#include "GoldenHarpoon.h"
#include "AnimatedTexture.h"
#include "Collider.h"
#include "Player.h"

GoldenHarpoon::GoldenHarpoon(AnimatedTexture* p_pxAnimatedTexture, sf::RenderWindow * p_pxWindow, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxAnimatedTexture = p_pxAnimatedTexture;
	m_pxSprite.setTexture(p_pxAnimatedTexture->GetTexture());
	m_pxSprite.setOrigin(sf::Vector2f(100, 50));
	m_pxSprite.setColor(sf::Color::Yellow);

	m_fX = p_fX;
	m_fY = p_fY;
	m_fVelocity = 10;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_iState = GSHOT;
	

	sf::Vector2i mousePosition = sf::Mouse::getPosition(*p_pxWindow);
	m_fAngle = atan2(mousePosition.y - m_fY, mousePosition.x - m_fX);
}

GoldenHarpoon::~GoldenHarpoon()
{
	m_pxSprite.setRotation(0);

	m_pxAnimatedTexture = nullptr;

}

void GoldenHarpoon::Update(float p_fDeltaTime, Player * p_pxPlayer)
{
	if(m_iState == GSHOT)
	{
	UpdateShot(p_fDeltaTime);
	}

	if (m_fX + (m_pxSprite.getTextureRect().width / 2) > m_iScreenWidth)
	{
		SetState(GDELETE);
	}
	
	if ((m_fY + (m_pxSprite.getTextureRect().height / 2)) > m_iScreenHeight)
	{
		SetState(GDELETE);
	}
	if (m_fY < 0)
	{
		SetState(GDELETE);
	}
	
}

void GoldenHarpoon::UpdateShot(float p_fDeltaTime)
{
	float cosine = cos(m_fAngle);
	float sine = sin(m_fAngle);
	m_pxSprite.setPosition(sf::Vector2f(m_fX += cosine * m_fVelocity, m_fY += sine * m_fVelocity));


	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setRotation(m_fAngle * 180.0f / M_PI);
	m_pxSprite.setScale(0.5, 0.5);
}


sf::Sprite GoldenHarpoon::GetSprite()
{
	return m_pxSprite;
}

float GoldenHarpoon::GetX()
{
	return m_fX;
}

float GoldenHarpoon::GetY()
{
	return m_fY;
}

int GoldenHarpoon::GetState()
{
	return m_iState;
}

void GoldenHarpoon::SetState(int p_iNewState)
{
	m_iState = p_iNewState;
}

bool GoldenHarpoon::IsVisible()
{
	return m_fX;
}

EENTITYTYPE GoldenHarpoon::GetType()
{
	return EENTITYTYPE::ENTITY_GOLDENHARPOON;
}
*/