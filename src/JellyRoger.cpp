#include "stdafx.h"
#include "Utility.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Player.h"
#include "JellyRoger.h"

JellyRoger::JellyRoger()
{
	
}

JellyRoger::~JellyRoger()
{
	

}

bool JellyRoger::Initialize(SpriteManager * p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight)
{
	m_pxAnimatedTextureDefault = p_pxSpriteManager->CreateAnimatedTexture("../assets/JellyRogerSwims.png");
	m_pxAnimatedTextureDefault->AddFrame(33, 5, 103, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(190, 5, 103, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(337, 5, 126, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(478, 5, 139, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(640, 5, 146, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(793, 5, 153, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(950, 5, 158, 205, 0.1);

	m_pxAnimatedTextureDefault->AddFrame(1151, 5, 69, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1318, 5, 63, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1480, 5, 56, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1625, 5, 79, 205, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1745, 5, 139, 205, 0.1);

	m_pxAnimatedTextureDying = p_pxSpriteManager->CreateAnimatedTexture("../assets/JellyRogerDies.PNG");
	
	m_pxAnimatedTextureDying->AddFrame(1065, 90, 383, 130, 0.3);
	m_pxAnimatedTextureDying->AddFrame(1575, 80, 375, 215, 0.3);
	m_pxAnimatedTextureDying->AddFrame(2118, 88, 333, 130, 0.3);
	m_pxAnimatedTextureDying->AddFrame(116, 35, 210, 258, 0.3);
	m_pxAnimatedTextureDying->AddFrame(620, 65, 330, 180, 0.3);


	m_pxSprite.setTextureRect(m_pxAnimatedTextureDefault->GetRegion());
	m_pxSprite.setTexture(m_pxAnimatedTextureDefault->GetTexture());

	m_fX = p_fX;
	m_fY = p_fY;
	m_pxSprite.setPosition(50, 70);
	m_fSpeed = 2.5f;
	m_fDirX = -1.0f;
	m_fDirY = -0.2f;


	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iscreenHeight;

	return true;
}

void JellyRoger::Update(float p_fDeltaTime)
{
	m_pxAnimatedTextureDefault->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTextureDefault->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTextureDefault->GetRegion());
	if (m_bDying)
	{
		DyingStateUpdate(p_fDeltaTime);
	}

	m_pxSprite.setPosition(m_fX += m_fDirX * m_fSpeed * 2, m_fY += m_fDirY * m_fSpeed);
}

void JellyRoger::DyingStateUpdate(float p_fDeltaTime)
{
	DyingTimer += p_fDeltaTime;
	m_pxAnimatedTextureDying->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTextureDying->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTextureDying->GetRegion());
	if (DyingTimer >= DyingTimerTarget)
	{
		m_pxAnimatedTextureDying->Reset();
		DyingTimer = 0.0;
		m_bActive = false;
		m_bDying = false;
	}
}

sf::Sprite JellyRoger::GetSprite()
{
	return m_pxSprite;
}
AnimatedTexture * JellyRoger::GetTexture()
{
	return m_pxAnimatedTextureDefault;
}

float JellyRoger::GetX()
{
	return m_fX;
}

float JellyRoger::GetY()
{
	return m_fY;
}

bool JellyRoger::IsActive()
{
	return m_bActive;
}

void JellyRoger::Deactivate()
{
	m_bActive = false;
}

void JellyRoger::Kill()
{
	m_bDying = true;
}

void JellyRoger::Activate()
{
	m_bActive = true;
	m_bDying = false;
	m_fX = m_iScreenWidth;
	m_fY = Utility::RandomFloat(200, 900);
}

EENTITYTYPE JellyRoger::GetType()
{
	return EENTITYTYPE::ENTITY_JELLYROGER;
}

bool JellyRoger::IsDying()
{
	return m_bDying;
}
