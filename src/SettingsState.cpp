#include "SettinsState.h"
#include "stdafx.h"
#include <iostream>
#include "DrawManager.h"
#include "GameState.h"
#include "MenuState.h"


SettingsState::SettingsState(System& p_xSystem)
{
	m_xSystem = p_xSystem;
	m_xMenuTexture.loadFromFile("../assets/Start_Menu_2.png");
	m_xMenuSprite.setTexture(m_xMenuTexture);
	m_xCursor.loadFromFile("../assets/muspekare.png");
	m_xCursorSprite.setTexture(m_xCursor);
	m_xCursorSprite.setTextureRect(sf::IntRect(0, 0, 60, 60));
	m_xCursorSprite.setOrigin(30, 30);

	// Laddar in font
	if (!font.loadFromFile("../assets/OldLondon.ttf"))
	{
		std::cout << "Unable to load textfile" << std::endl;
	}
	

	//Utseende meny
	menu[0].setFont(font);
	menu[0].setColor(sf::Color::Blue);
	menu[0].setString("Back");
	menu[0].setPosition(sf::Vector2f(width / 8, height - 900));
	menu[0].setCharacterSize(fontSize);

	Buttons[0].setFont(font);
	Buttons[0].setCharacterSize(100);
	Buttons[0].setColor(sf::Color::White);
	Buttons[0].setString("-");
	Buttons[0].setPosition(sf::Vector2f(width / 8, height - 540));

	Buttons[1].setFont(font);
	Buttons[1].setCharacterSize(100);
	Buttons[1].setColor(sf::Color::White);
	Buttons[1].setString("+");
	Buttons[1].setPosition(sf::Vector2f(width - 500, height - 540));

	Volume[0].setFont(font);
	Volume[0].setCharacterSize(100);
	Volume[0].setColor(sf::Color::Blue);
	Volume[0].setString("0");
	Volume[0].setPosition(sf::Vector2f(width / 5, height - 540));


	selectedItemIndex = 0;
	iVolume = sf::Listener::getGlobalVolume();
}

SettingsState::~SettingsState()
{


}


void SettingsState::Draw()
{
	m_xSystem.m_pxDrawManager->Draw(m_xMenuSprite);
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{

		m_xSystem.m_pxDrawManager->DrawText(menu[i]);
	}
	for (int i = 0; i < MAX_NUMBER_OF_BUTTONS; i++)
	{
		m_xSystem.m_pxDrawManager->DrawText(Buttons[i]);
	}
	for (int i = 0; i < MAX_NUBER_OF_CHOICES; i++)
	{
		m_xSystem.m_pxDrawManager->DrawText(Buttons[i]);
	}

	m_xSystem.m_pxDrawManager->Draw(m_xCursorSprite);
}

void SettingsState::MoveUp()
{

	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setColor(sf::Color::White);
		menu[selectedItemIndex].setCharacterSize(fontSize);
		selectedItemIndex--;
		menu[selectedItemIndex].setColor(sf::Color::Blue);
		menu[selectedItemIndex].setCharacterSize(selectedFontSize);
	}

}

void SettingsState::MoveDown()
{
	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
	{
		menu[selectedItemIndex].setColor(sf::Color::White);
		menu[selectedItemIndex].setCharacterSize(fontSize);
		selectedItemIndex++;
		menu[selectedItemIndex].setColor(sf::Color::Blue);
		menu[selectedItemIndex].setCharacterSize(selectedFontSize);
	}

}

bool SettingsState::Update(float p_fDeltaTime)
{
	
	sf::RenderWindow* xWindow = m_xSystem.m_pxDrawManager->GetRenderWindow();
	sf::Vector2f xMousePosition = static_cast<sf::Vector2f>(sf::Mouse::getPosition(*m_xSystem.m_pxDrawManager->GetRenderWindow()));
	m_xCursorSprite.setPosition(xMousePosition);
	m_xCursorSprite.setTexture(m_xCursor);
	Volume[0].setString(std::to_string(iVolume));
	float xVolume = sf::Listener::getGlobalVolume();

	//v�lja med musen
	if (menu[0].getGlobalBounds().contains(xMousePosition))
	{
		menu[0].setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			return false;
		}
	}
	else
	{
		menu[0].setColor(sf::Color::Black);
	}


	if (menu[1].getGlobalBounds().contains(xMousePosition))
	{
		menu[1].setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			m_xSystem.m_pxDrawManager->Shutdown();
		}
	}
	else
	{
		menu[1].setColor(sf::Color::Black);
	}
	while (m_xSystem.m_pxDrawManager->GetRenderWindow()->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			m_xSystem.m_pxDrawManager->GetRenderWindow()->close();
		}
		if (event.type == sf::Event::MouseButtonPressed)
		{
			if (Buttons[0].getGlobalBounds().contains(xMousePosition))
			{
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					if (iVolume > 0)
					{
						iVolume -= 5;
						sf::Listener::setGlobalVolume(iVolume);
					}
				}
			}


			if (Buttons[1].getGlobalBounds().contains(xMousePosition))
			{
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					if (iVolume < 100)
					{
						iVolume += 5;
						sf::Listener::setGlobalVolume(iVolume);
					}
				}
			}

		}
	}
	if (Buttons[0].getGlobalBounds().contains(xMousePosition))
	{
		Buttons[0].setColor(sf::Color::Red);
	}
	else
	{
		Buttons[0].setColor(sf::Color::Black);
	}

	if (Buttons[1].getGlobalBounds().contains(xMousePosition))
	{
		Buttons[1].setColor(sf::Color::Red);
	}
	else
	{
		Buttons[1].setColor(sf::Color::Black);
	}

	return true;
}

IState* SettingsState::NextState()
{
	return new MenuState(m_xSystem);
}
