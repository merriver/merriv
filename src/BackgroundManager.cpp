#include "stdafx.h"
#include "BackgroundManager.h"
#include "BackgroundElement.h"
#include "DrawManager.h"
#include "Utility.h"
#include "SpriteManager.h"


BackgroundManager::BackgroundManager(SpriteManager* p_pxSpriteManager, int p_iScreenHeight, int p_iScreenWidth)
{
	m_iScreenHeight = p_iScreenHeight;
	m_iScreenWidth = p_iScreenWidth;
	m_pxSpriteManager = p_pxSpriteManager;
	m_xSeafloorObjectTexture = m_pxSpriteManager->GetTexture("../assets/ForegroundObjects.png");
	m_xMiddleGroundTextures = m_pxSpriteManager->GetTexture("../assets/Rock_tiles.png");
	m_xMiddleGroundTextures2 = m_pxSpriteManager->GetTexture("../assets/Rock_tiles_2.png");
	//0-4
	m_axElementRects.push_back(sf::IntRect(160, 60, 612, 584));
	m_axElementRects.push_back(sf::IntRect(1300, 400, 300, 350));
	m_axElementRects.push_back(sf::IntRect(2150, 400, 260, 300));
	m_axElementRects.push_back(sf::IntRect(160, 800, 250, 240));
	m_axElementRects.push_back(sf::IntRect(1990, 780, 175, 230));
	//5-6
	m_axElementRects.push_back(sf::IntRect(38, 95, 970, 700));
	m_axElementRects.push_back(sf::IntRect(1069, 5, 790, 790));
	//7-8
	m_axElementRects.push_back(sf::IntRect(265, 246, 210, 245));
	m_axElementRects.push_back(sf::IntRect(698, 0, 1150, 1080));

	m_apxBackgroundElements.resize(27);
	int iTempIndex = 0;
	{
		auto it = m_apxBackgroundElements.begin();
		while ((it) != m_apxBackgroundElements.end())
		{
			if (iTempIndex <= 2)
			{
				(it)->Initialize(m_xSeafloorObjectTexture, m_axElementRects[0], 1920 + 100, Utility::RandomFloat(1040, 1080), 1, m_iScreenHeight, m_iScreenWidth, false, false);
			}
			else if (iTempIndex <= 5)
			{
				(it)->Initialize(m_xSeafloorObjectTexture, m_axElementRects[1], 0, 885, 1, p_iScreenWidth, p_iScreenHeight, false, false);
			}
			else if (iTempIndex <= 8)
			{
				(it)->Initialize(m_xSeafloorObjectTexture, m_axElementRects[2], 0, 885, 1, p_iScreenWidth, p_iScreenHeight, false, false);
			}
			else if (iTempIndex <= 11)
			{
				(it)->Initialize(m_xSeafloorObjectTexture, m_axElementRects[3], 0, 885, 1, p_iScreenWidth, p_iScreenHeight, false, false);
			}
			else if (iTempIndex <= 14)
			{
				(it)->Initialize(m_xSeafloorObjectTexture, m_axElementRects[4], 0, 885, 1, p_iScreenWidth, p_iScreenHeight, false, false);
			}
			else if (iTempIndex <= 17)
			{
				(it)->Initialize(m_xMiddleGroundTextures, m_axElementRects[5], 0, 885, 2, p_iScreenWidth, p_iScreenHeight, false, false);
			}
			else if (iTempIndex <= 20)
			{
				(it)->Initialize(m_xMiddleGroundTextures, m_axElementRects[6], 0, 885, 2, p_iScreenWidth, p_iScreenHeight, false, false);
			}
			else if (iTempIndex <= 23)
			{
				(it)->Initialize(m_xMiddleGroundTextures2, m_axElementRects[7], 0, 885, 2, p_iScreenWidth, p_iScreenHeight, false, false);
			}
			else if (iTempIndex <= 26)
			{
				(it)->Initialize(m_xMiddleGroundTextures2, m_axElementRects[8], 0, 885, 2, p_iScreenWidth, p_iScreenHeight, false, false);
			}
			iTempIndex++;
			it++;
		}
	}
	m_xSeafloorTexture = m_pxSpriteManager->GetTexture("../assets/Sand_tiles.png");
	m_axSeafloorRects.push_back(sf::IntRect(63, 0, 1810, 195));
	m_axSeafloorRects.push_back(sf::IntRect(63, 613, 1810, 195));

	iTempIndex = 0;
	m_apxBackgroundSeafloor.resize(4);
	{
		auto it = m_apxBackgroundSeafloor.begin();
		while ((it) != m_apxBackgroundSeafloor.end())
		{
			if (iTempIndex <= 1)
			{
				(it)->Initialize(m_xSeafloorTexture, m_axSeafloorRects[0], 0, 885, 1, p_iScreenWidth, p_iScreenHeight, true, false);
			}
			else
			{
				(it)->Initialize(m_xSeafloorTexture, m_axSeafloorRects[1], 0, 885, 1, p_iScreenWidth, p_iScreenHeight, true, false);
			}
			iTempIndex++;
			it++;
		}
	}

	m_apxBackgroundSeafloor[0].Activate(0, 885);
	m_apxBackgroundSeafloor[1].Activate(1810, 885);

	m_xBackgroundTexture2 = m_pxSpriteManager->GetTexture("../assets/SmallBG2.png");
	m_xBackgroundTexture = m_pxSpriteManager->GetTexture("../assets/SmallBG1.png");
	m_apxBackground.resize(9);
	{
		iTempIndex = 0;
		auto it = m_apxBackground.begin();
		while ((it) != m_apxBackground.end())
		{
			if (iTempIndex <= 2)
			{
				(it)->Initialize(m_xBackgroundTexture, m_axBackgroundRects[iTempIndex], 0, 0, 5, p_iScreenWidth, p_iScreenHeight, true, true);
			}
			else
			{
				(it)->Initialize(m_xBackgroundTexture2, m_axBackgroundRects[iTempIndex], 0, 0, 5, p_iScreenWidth, p_iScreenHeight, true, true);
			}

			iTempIndex++;
			it++;
		}
	}
	m_apxBackground[0].Activate(0, 0);

	m_xWaveTexture = m_pxSpriteManager->GetTexture("../assets/Waves.png");
	m_apxWaves.resize(2);
	{
		iTempIndex = 0;
		auto it = m_apxWaves.begin();
		while ((it) != m_apxWaves.end())
		{

			(it)->Initialize(m_xWaveTexture, sf::IntRect(0, 30, 1973, 500), 0, 50, 5, p_iScreenWidth, p_iScreenHeight, true, false);
			it++;
		}
	}
	m_apxWaves[0].Activate(0, 50);
}


BackgroundManager::~BackgroundManager()
{

	m_apxBackground.clear();

	m_apxBackgroundElements.clear();

	m_apxBackgroundSeafloor.clear();

	m_apxWaves.clear();

}

void BackgroundManager::CreateBackgroundElement()
{
	int iChosen = Utility::RandomFloat(0, 26.99);
	if (!m_apxBackgroundElements[iChosen].IsActive())
	{
		m_apxBackgroundElements[iChosen].Activate(2500, 1080);
	}


}

void BackgroundManager::Update(float p_fDeltaTime, DrawManager* p_pxDrawManager, bool p_bCaveMode)
{
	if (!m_bStopMoving)
	{
		if (m_iCurrentBackground == 10)
		{
			m_iCurrentBackground = 0;
		}
		m_fElementTimer += p_fDeltaTime;
		// Create new background if the previous ends
		if (m_apxBackground[m_iCurrentBackground].GetX() + m_apxBackground[m_iCurrentBackground].GetSprite().getGlobalBounds().width <= m_iScreenWidth)
		{
			m_iCurrentBackground++;
			m_apxBackground[m_iCurrentBackground].Activate(1920, 0);

		}

		{
			//Update or delete Background depending on if it's outside of the screen
			auto it = m_apxBackground.begin();
			while (it != m_apxBackground.end() && m_apxBackground.size() != 0)
			{

				(it)->Update(p_fDeltaTime);

				if ((it)->GetX() + (it)->GetSprite().getGlobalBounds().width <= 0)
				{
					(it)->Deactivate();
				}

				it++;
			}
		}

		// Create new Wave if the previous ends
		{
			auto it = m_apxWaves.begin();
			while ((it) != m_apxWaves.end())
			{
				if ((it)->GetX() + (it)->GetSprite().getTextureRect().width < m_iScreenWidth)
				{
					{
						bool bFound = false;
						auto it = m_apxWaves.begin();
						while ((it) != m_apxWaves.end())
						{
							if (!(it)->IsActive() && !bFound)
							{
								(it)->Activate(1919, 50);
								bFound = true;
							}
							it++;
						}

					}
				}
				it++;
			}

		}
		{
			//Update or delete Wave depending on if it's outside of the screen
			auto it = m_apxWaves.begin();
			while (it != m_apxWaves.end() && m_apxWaves.size() != 0)
			{

				if ((it)->IsActive())
				{
					(it)->Update(p_fDeltaTime);

				}
				if ((it)->GetX() + (it)->GetSprite().getGlobalBounds().width < 0)
				{
					(it)->Deactivate();
				}
				it++;
			}
		}


		// Create new seafloor if the previous ends
		{
			auto it = m_apxBackgroundSeafloor.begin();
			while ((it) != m_apxBackgroundSeafloor.end())
			{
				if ((it)->GetX() + (it)->GetSprite().getTextureRect().width <= m_iScreenWidth && (it)->GetX() > 100)
				{
					bool bFound = false;
					auto it = m_apxBackgroundSeafloor.begin();
					while ((it) != m_apxBackgroundSeafloor.end())
					{
						if (!(it)->IsActive() && !bFound)
						{
							(it)->Activate(1919, 885);
							bFound = true;
						}
						it++;
					}
				}
				it++;
			}
		}
		//Update or delete seafloor depending on if it's outside of the screen
		auto it = m_apxBackgroundSeafloor.begin();
		while (it != m_apxBackgroundSeafloor.end() && m_apxBackgroundSeafloor.size() != 0)
		{

			if ((it)->IsActive())
			{
				(it)->Update(p_fDeltaTime);

				if ((it)->GetX() + (it)->GetSprite().getTextureRect().width <= 0)
				{
					(it)->Deactivate();

				}
			}
			it++;
		}




		//Create Background elements now and then;
		if (Utility::RandomFloat(1, 1000) > 990 && m_fElementTimer >= m_fTimerTarget)
		{
			m_fElementTimer = 0.0;

			CreateBackgroundElement();
		}


		//Delete Background Elements outside of the screen
		{
			auto it = m_apxBackgroundElements.begin();
			while (it != m_apxBackgroundElements.end() && m_apxBackgroundElements.size() != 0)
			{
				if ((it)->IsActive())
				{
					(it)->Update(p_fDeltaTime);

					if ((it)->GetX() + (it)->GetSprite().getGlobalBounds().width <= 0)
					{
						(it)->Deactivate();
					}
				}
				it++;
			}
		}
	}
}


void BackgroundManager::DrawBackground(DrawManager* p_pxDrawManager)
{
	{
		auto it = m_apxBackground.begin();
		while (it != m_apxBackground.end() && m_apxBackground.size() != 0)
		{

			if ((it)->IsActive())
			{
				p_pxDrawManager->Draw((it)->GetSprite());

			}
			it++;
		}
	}
	{
		auto it = m_apxBackgroundElements.begin();
		while (it != m_apxBackgroundElements.end() && m_apxBackgroundElements.size() != 0)
		{

			if ((it)->IsActive() && (it)->GetDepth() > 1)
			{
				p_pxDrawManager->Draw((it)->GetSprite());

			}
			it++;
		}
	}
	{
		auto it = m_apxBackgroundSeafloor.begin();
		while (it != m_apxBackgroundSeafloor.end() && m_apxBackgroundSeafloor.size() != 0)
		{

			if ((it)->IsActive())
			{
				p_pxDrawManager->Draw((it)->GetSprite());

			}
			it++;
		}
	}

	{
		auto it = m_apxWaves.begin();
		while (it != m_apxWaves.end() && m_apxBackground.size() != 0)
		{

			if ((it)->IsActive())
			{
				p_pxDrawManager->Draw((it)->GetSprite());

			}
			it++;
		}
	}
}

void BackgroundManager::DrawForeground(DrawManager * p_pxDrawManager)
{
	{
		auto it = m_apxBackgroundElements.begin();
		while (it != m_apxBackgroundElements.end() && m_apxBackgroundElements.size() != 0)
		{

			if ((it)->IsActive() && (it)->GetDepth() <= 1)
			{
				p_pxDrawManager->Draw((it)->GetSprite());

			}
			it++;
		}
	}
}

void BackgroundManager::StopMoving()
{
	m_bStopMoving = true;
}

void BackgroundManager::StartMoving()
{
	m_bStopMoving = false;
}