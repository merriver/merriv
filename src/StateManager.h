#pragma once

class IState;

class StateManager
{
	
	friend class Engine; 
public:
	StateManager();
	~StateManager();
	bool Update();
	void Draw();
private:
	void SetState(IState* p_pxState);
	IState* m_pxCurrentState;
	float m_fFrameTime;
	float m_fAccumulator;
	float m_fTargetTime;
	sf::Event m_xEvent;
	sf::Clock clock;
	sf::Time m_xDeltaTime;
};
