#include "stdafx.h"
#include "EnemyManager.h"
#include "Swordfish.h"
#include "JellyProjectile.h"
#include "JellyRoger.h"
#include "Piranha.h"
#include "Utility.h"
#include "DrawManager.h"
#include "SpriteManager.h"
#include "SoundManager.h"
#include "Player.h"

EnemyManager::EnemyManager(SpriteManager* p_pxSpriteManager, SoundManager* p_pxSoundManager)
{
	
	m_xEnemies.m_axSwordfish.resize(15);
	{
		auto it = m_xEnemies.m_axSwordfish.begin();
		while (it != m_xEnemies.m_axSwordfish.end())
		{
			(it)->Initialize(p_pxSpriteManager, p_pxSoundManager, 1920, 0, 1920, 1080);
			it++;
		}
	}
	m_xEnemies.m_axJellyRoger.resize(15);
	{
		auto it = m_xEnemies.m_axJellyRoger.begin();
		while (it != m_xEnemies.m_axJellyRoger.end())
		{
			(it)->Initialize(p_pxSpriteManager, 1920, 0, 1920, 1080);
			it++;
		}
	}
	m_xEnemies.m_axJellyProjectile.resize(25);
	{
		auto it = m_xEnemies.m_axJellyProjectile.begin();
		while (it != m_xEnemies.m_axJellyProjectile.end())
		{
			(it)->Initialize(p_pxSpriteManager, 1920, 0, 1920, 1080);
			it++;
		}
	}
	m_xEnemies.m_axPiranha.resize(15);
	{
		auto it = m_xEnemies.m_axPiranha.begin();
		while (it != m_xEnemies.m_axPiranha.end())
		{
			(it)->Initialize(p_pxSpriteManager, 1920, 0, 1920, 1080);
			it++;
		}
	}
}

EnemyManager::~EnemyManager()
{
	m_xEnemies.m_axJellyProjectile.clear();
	m_xEnemies.m_axJellyRoger.clear();
	m_xEnemies.m_axPiranha.clear();
	m_xEnemies.m_axSwordfish.clear();
}

void EnemyManager::Update(float p_fDeltaTime, Player* p_pxPlayer)
{
	int iIndex;
	m_fSpawnTimer += p_fDeltaTime;
	if (m_fSpawnTimer >= m_fTimerTarget)
	{
		m_fSpawnTimer = 0.0f;
		m_iSpawnRate -= 0.05f;

	}

	if (Utility::RandomFloat(0, 300) > m_iSpawnRate)
	{
		
		int iChooseType = Utility::RandomFloat(0, 300);
		//Create Swordfish enemy
		if (iChooseType < 150)
		{
			//get an inactive swordfish and make it active
			m_bSearch = true;
			iIndex = 0;
			while (m_bSearch)
			{
				if (!m_xEnemies.m_axSwordfish[iIndex].IsActive())
				{
					m_xEnemies.m_axSwordfish[iIndex].Activate();
					m_bSearch = false;
				}
				else
				{
					iIndex++;
				}
			}
		}

		//Create Piranha enemy
		if (iChooseType < 210 && iChooseType > 150)
		{
			//get an inactive Piranha and make it active
			m_bSearch = true;
			iIndex = 0;
			while (m_bSearch)
			{
				if (!m_xEnemies.m_axPiranha[iIndex].IsActive())
				{
					m_xEnemies.m_axPiranha[iIndex].Activate();
					m_bSearch = false;
				}
				else
				{
					iIndex++;
				}
			}
		}
		//Create JellyRoger enemy
		if (iChooseType > 210)
		{
			//get an inactive JellyRoger and make it active
			m_bSearch = true;
			iIndex = 0;
			while (m_bSearch)
			{
				if (!m_xEnemies.m_axJellyRoger[iIndex].IsActive())
				{
					m_xEnemies.m_axJellyRoger[iIndex].Activate();
					m_bSearch = false;
				}
				else
				{
					iIndex++;
				}
			}

			m_bJollyProjectileBool = true;

		}
	}




	{
		//Update SwordFish
		auto it = m_xEnemies.m_axSwordfish.begin();
		while (it != m_xEnemies.m_axSwordfish.end() && m_xEnemies.m_axSwordfish.size() != 0)
		{
			if ((it)->IsActive())
			{
				(it)->Update(p_fDeltaTime, p_pxPlayer);
			}

			if ((it)->GetX() + (it)->GetSprite().getTextureRect().width <= 0)
			{
				(it)->Deactivate();
			}
			it++;
		}
	}

	

	{
		// Update Piranha
		auto it = m_xEnemies.m_axPiranha.begin();
		while (it != m_xEnemies.m_axPiranha.end() && m_xEnemies.m_axPiranha.size() != 0)
		{

			if ((it)->IsActive())
			{
				(it)->Update(p_fDeltaTime, p_pxPlayer);
			}

			if ((it)->GetX() + (it)->GetSprite().getTextureRect().width <= 0)
			{
				(it)->Deactivate();
			}

			it++;
		}

	}

	//Update Jellyfish
	

	{
		auto it = m_xEnemies.m_axJellyRoger.begin();
		while (it != m_xEnemies.m_axJellyRoger.end() && m_xEnemies.m_axJellyRoger.size() != 0)
		{

			if ((it)->IsActive())
			{
				(it)->Update(p_fDeltaTime);
			}

			if ((it)->GetX() + (it)->GetSprite().getTextureRect().width <= 0)
			{
				(it)->Deactivate();
			}

			it++;
		}
	}

	m_fJollyProjectileTimer += p_fDeltaTime;
	if (m_fJollyProjectileTimer >= 2)
	{
		m_fJollyProjectileTimer = 0.0f;
		auto it = m_xEnemies.m_axJellyRoger.begin();
		while (it != m_xEnemies.m_axJellyRoger.end() && m_xEnemies.m_axJellyRoger.size() != 0)
		{
			m_bSearch = true;
			iIndex = 0.0f;
			
			while (m_bSearch && (it)->IsActive())
			{
				if ((it)->IsDying())
				{
					m_bSearch = false;
				}
				if (!m_xEnemies.m_axJellyProjectile[iIndex].IsActive() && !(it)->IsDying())
				{
					m_xEnemies.m_axJellyProjectile[iIndex].Activate((it)->GetX(), (it)->GetY());
					m_bSearch = false;
				}
				else
				{
					iIndex++;
				}
			}

			it++;
			//m_bJollyProjectileBool = false;
		}

	}
	{
		auto it = m_xEnemies.m_axJellyProjectile.begin();
		while (it != m_xEnemies.m_axJellyProjectile.end() && m_xEnemies.m_axJellyProjectile.size() != 0)
		{

			if ((it)->IsActive())
			{
				(it)->Update(p_fDeltaTime);
			}
			if ((it)->GetX() + (it)->GetSprite().getTextureRect().width <= 0)
			{
				(it)->Deactivate();
			}

			it++;
		}
	}
}

void EnemyManager::Draw(DrawManager* p_pxDrawManager)
{
	//Draw Swordfish
	{
		auto it = m_xEnemies.m_axSwordfish.begin();
		while (it != m_xEnemies.m_axSwordfish.end() && m_xEnemies.m_axSwordfish.size() != 0)
		{

			if ((it)->GetX() < 1920 && (it)->GetX() > 0 - (it)->GetSprite().getTextureRect().width && (it)->IsActive())
			{
				p_pxDrawManager->Draw((it)->GetSprite());
			}
			it++;
		}
	}
	//Draw Piranha
	{
		auto it = m_xEnemies.m_axPiranha.begin();
		while (it != m_xEnemies.m_axPiranha.end() && m_xEnemies.m_axPiranha.size() != 0)
		{

			if ((it)->GetX() < 1920 && (it)->GetX() > 0 - (it)->GetSprite().getTextureRect().width && (it)->IsActive())
			{
				p_pxDrawManager->Draw((it)->GetSprite());
			}
			it++;
		}
	}
	//Draw Jelly Roger
	{
		auto it = m_xEnemies.m_axJellyRoger.begin();
		while (it != m_xEnemies.m_axJellyRoger.end() && m_xEnemies.m_axJellyRoger.size() != 0)
		{

			if ((it)->GetX() < 1920 && (it)->GetX() > 0 - (it)->GetSprite().getTextureRect().width && (it)->IsActive())
			{
				p_pxDrawManager->Draw((it)->GetSprite());
			}

			it++;
		}
	}
	//Draw Jelly Projetile  
	{
		auto it = m_xEnemies.m_axJellyProjectile.begin();
		while (it != m_xEnemies.m_axJellyProjectile.end() && m_xEnemies.m_axJellyProjectile.size() != 0)
		{

			if ((it)->GetX() < 1920 && (it)->GetX() > 0 - (it)->GetSprite().getTextureRect().width && (it)->IsActive())
			{
				p_pxDrawManager->Draw((it)->GetSprite());
			}

			it++;
		}
	}
}

Enemies EnemyManager::GetEnemies()
{
	return m_xEnemies;
}

bool EnemyManager::CheckSwordfishCollision(sf::FloatRect p_xRect)
{
	auto it = m_xEnemies.m_axSwordfish.begin();
	while (it != m_xEnemies.m_axSwordfish.end() && m_xEnemies.m_axSwordfish.size() != 0)
	{

		if ((it)->GetTexture() != nullptr && p_xRect.intersects((it)->GetSprite().getGlobalBounds()) && (it)->IsActive() && !(it)->IsDying())
		{
			m_xDropRect = (it)->GetSprite().getGlobalBounds();
			(it)->Kill();
			return true;
		}
		

		it++;
	}
	return false;
}

bool EnemyManager::CheckJellyRogerCollision(sf::FloatRect p_xRect)
{
	auto it = m_xEnemies.m_axJellyRoger.begin();
	while (it != m_xEnemies.m_axJellyRoger.end() && m_xEnemies.m_axJellyRoger.size() != 0)
	{

		if ((it)->GetTexture() != nullptr && p_xRect.intersects((it)->GetSprite().getGlobalBounds()) && (it)->IsActive() && !(it)->IsDying())
		{
			m_xDropRect = (it)->GetSprite().getGlobalBounds();
			(it)->Kill();
			return true;
		}


		it++;
	}
	return false;
}

bool EnemyManager::CheckJellyProjectileCollision(sf::FloatRect p_xRect)
{
	auto it = m_xEnemies.m_axJellyProjectile.begin();
	while (it != m_xEnemies.m_axJellyProjectile.end() && m_xEnemies.m_axJellyRoger.size() != 0)
	{

		if ((it)->GetTexture() != nullptr && p_xRect.intersects((it)->GetSprite().getGlobalBounds()) && (it)->IsActive())
		{
			m_xDropRect = (it)->GetSprite().getGlobalBounds();
			(it)->Deactivate();

			return true;
		}


		it++;
	}
	return false;
}

bool EnemyManager::CheckPiranhaCollision(sf::FloatRect p_xRect)
{
	auto it = m_xEnemies.m_axPiranha.begin();
	while (it != m_xEnemies.m_axPiranha.end() && m_xEnemies.m_axPiranha.size() != 0)
	{

		if ((it)->GetTexture() != nullptr && p_xRect.intersects((it)->GetCollitionRect())  && (it)->IsActive() && !(it)->IsDying())
		{
			m_xDropRect = (it)->GetSprite().getGlobalBounds();
			(it)->Kill();
			return true;
		}


		it++;
	}
	return false;
}

void EnemyManager::Deactivate()
{
	{
		auto it = m_xEnemies.m_axSwordfish.begin();
		while (it != m_xEnemies.m_axSwordfish.end() && m_xEnemies.m_axSwordfish.size() != 0)
		{
			
			(it)->Deactivate();
			it++;
		}
	}
	{
		auto it = m_xEnemies.m_axPiranha.begin();
		while (it != m_xEnemies.m_axPiranha.end() && m_xEnemies.m_axPiranha.size() != 0)
		{

			(it)->Deactivate();
			it++;
		}
	}
	{
		auto it = m_xEnemies.m_axJellyRoger.begin();
		while (it != m_xEnemies.m_axJellyRoger.end() && m_xEnemies.m_axJellyRoger.size() != 0)
		{

			(it)->Deactivate();
			it++;
		}
	}
	{
		auto it = m_xEnemies.m_axJellyProjectile.begin();
		while (it != m_xEnemies.m_axJellyProjectile.end() && m_xEnemies.m_axJellyProjectile.size() != 0)
		{

			(it)->Deactivate();
			it++;
		}
	}
	m_iSpawnRate = 299;
	m_fSpawnTimer = 0.0f;
}

sf::FloatRect EnemyManager::GetDropRect()
{
	return m_xDropRect;
}

