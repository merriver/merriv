#include "stdafx.h"
#include "Coin.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Utility.h"


Coin::Coin()
{
	
}

Coin::~Coin()
{
	
}

bool Coin::Initialize(SpriteManager * p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxAnimatedTexture = p_pxSpriteManager->CreateAnimatedTexture("../assets/BubbleSheet.png");
	m_pxAnimatedTexture->AddFrame(0, 0, 278, 233, 0.1);
	m_pxAnimatedTexture->AddFrame(278, 0, 278, 233, 0.1);
	m_pxAnimatedTexture->AddFrame(556, 0, 278, 233, 0.1);
	m_pxAnimatedTexture->AddFrame(834, 0, 278, 233, 0.1);
	m_pxAnimatedTexture->AddFrame(1112, 0, 278, 233, 0.1);
	m_pxAnimatedTexture->AddFrame(1390, 0, 278, 233, 0.1);
	m_pxAnimatedTexture->AddFrame(1668, 0, 278, 233, 0.1);
	m_pxAnimatedTexture->AddFrame(1946, 0, 278, 233, 0.1);
	m_pxAnimatedTexture->AddFrame(2224, 0, 278, 233, 0.1);
	m_xSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_xSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_xSprite.setScale(0.50, 0.50);
	m_fX = p_fX;
	m_fY = p_fY;
	m_xSprite.setPosition(m_fX, m_fY);
	return true;
}

void Coin::Update(float p_fDeltaTime)
{
	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_xSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_xSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_xSprite.setScale(0.5, 0.5);

	m_xSprite.setPosition(m_fX += -5, m_fY);
}

sf::Sprite Coin::GetSprite()
{
	return m_xSprite;
}


float Coin::GetX()
{
	return m_fX;
}

float Coin::GetY()
{
	return m_fY;
}

bool Coin::IsActive()
{
	return m_bActive;
}

void Coin::Deactivate()
{
	m_bActive = false;
}

void Coin::Activate()
{
	m_bActive = true;
	m_fX = 1920;
}

EENTITYTYPE Coin::GetType()
{
	return ENTITY_POWERUP;
}
