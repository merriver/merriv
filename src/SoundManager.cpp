#include "stdafx.h"
#include "SoundManager.h"

SoundManager::SoundManager()
{

}


SoundManager::~SoundManager()
{



}

bool SoundManager::Initialize()
{
	return true;
}

void SoundManager::Shutdown()
{
	auto it = m_apxSounds.begin();
	while (it != m_apxSounds.end())
	{
		delete (*it).second;
		it++;
	}
	m_apxSounds.clear();

	auto it2 = m_apxMusic.begin();
	while (it2 != m_apxMusic.end())
	{
		delete (*it2).second;
		it2++;
	}
	m_apxMusic.clear();

}

sf::Sound* SoundManager::CreateSound(const std::string& p_sFilepath)
{
	{
		auto it = m_apxSounds.find(p_sFilepath);
		if (it == m_apxSounds.end())
		{
			auto it = m_apxAudio.find(p_sFilepath);
			if (it == m_apxAudio.end())
			{
				sf::SoundBuffer xAudio;
				xAudio.loadFromFile(p_sFilepath.c_str());
				m_apxAudio.insert(std::pair<std::string, sf::SoundBuffer>(p_sFilepath, xAudio));
				it = m_apxAudio.find(p_sFilepath);
			}

			sf::Sound* xSound = new sf::Sound();
			xSound->setBuffer((it->second));
			m_apxSounds.insert(std::pair<std::string, sf::Sound*>(p_sFilepath, xSound));
			return xSound;
		}
		return (*it).second;
	}


	}

		



sf::Music* SoundManager::CreateMusic(const std::string & p_sFilepath)
{
	{
		auto it = m_apxMusic.find(p_sFilepath);
		if (it == m_apxMusic.end())
		{
			sf::Music* xMusic = new sf::Music();
			xMusic->openFromFile(p_sFilepath.c_str());
			m_apxMusic.insert(std::pair<std::string, sf::Music*>(p_sFilepath, xMusic));
			return xMusic;
		}
		return (*it).second;
	}
	
	sf::Music* xMusic = new sf::Music();
	xMusic->openFromFile(p_sFilepath.c_str());
	m_apxMusic.insert(std::pair<std::string, sf::Music*>(p_sFilepath, xMusic));
	return xMusic;
}
