#pragma once
#include "IEntity.h"


class sf::Sprite;
class Player;
class SpriteManager;
class SoundManager;

enum SwordfishState
{
	NEUTRAL,
	CHARGE,
};

class Swordfish : public IEntity
{
public:
	Swordfish();
	~Swordfish();
	bool Initialize(SpriteManager* p_pxSpriteManager, SoundManager* p_pxSoundManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight);
	void Update(float p_fDeltaTime, Player* p_pxPlayer);
	void NeutralStateUpdate(float p_fDeltaTime, Player* p_pxPlayer);
	void ChargeStateUpdate(float p_fDeltaTime);
	void DyingStateUpdate(float p_fDeltaTime);
	sf::Sprite GetSprite();
	AnimatedTexture* GetTexture();
	float GetX();
	float GetY();
	bool IsActive();
	void Deactivate();
	void Kill();
	void Activate();
	EENTITYTYPE GetType();
	void ReverseDirectionY();
	bool IsDying();

private:
	
	AnimatedTexture* m_pxAnimatedTextureDefault;
	AnimatedTexture* m_pxAnimatedTextureCharge;
	AnimatedTexture* m_pxAnimatedTextureDying;
	sf::Sprite m_pxSprite;
	sf::Sound* m_pxSwordfishChargeSound;
	SoundManager* m_pxSoundManager;
	float m_fSpeed;
	float m_fDirY;
	float m_fDirX;
	float m_fX;
	float m_fY;
	float DyingTimer = 0.0;
	float DyingTimerTarget = 0.7;
	int m_iState;
	int m_iScreenWidth;
	int m_iScreenHeight;
	int m_bIsCharging;
	bool m_bActive = false;
	bool m_bDying = false;
};