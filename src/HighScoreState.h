#pragma once
#include "IState.h"
#include "stdafx.h"

class DrawManager;

class HighScoreState : public IState
{
public:
	HighScoreState(System& p_xSystem, int p_iScore, std::string p_sName);
	~HighScoreState();
	void Draw();
	void SortScore(int p_iScoreData, std::string nameData);
	void Sort(std::array<int, 11> &data);
	bool Update(float p_fDeltaTime);
	IState* NextState();
	

private:
	sf::Font font;
	sf::Text m_xHeadline;
	sf::Text back;
	std::array<sf::Text, 10> m_xScore;
	std::array<sf::Text, 10> m_xName;
	sf::RenderStates testBlend;
	sf::BlendMode blendMode;
	sf::Texture m_xCursor;
	sf::Texture m_xMenuTexture;
	sf::Texture m_xPaperTexture;
	sf::Sprite m_xMenuSprite;
	sf::Sprite m_xCursorSprite;
	sf::Sprite m_xPaperSprite;
	sf::Vector2i m_xMousePosition;
	std::string m_xGetScoreString;
	std::string m_xScoreName;
	


	std::istringstream ss;
	std::string token;
	

	std::ifstream m_xInData;
	std::string m_xReadFromFileString;
	std::ifstream m_xHighScoreFile;
	std::map<int, std::string> highScoreList;

	char scoreChar[20];
	int fontSize = 80;
	int scoreFontSize = 50;
	int width = 1920;
	int height = 1080;
	int m_iScoreData;
	int m_iScore;
	std::string m_xStringScore;
	std::string m_sNameData;
	std::string name = "";
	std::array<int, 11> scoreArray;
	sf::Event m_xEvent;
	sf::RenderWindow* p_pxWindow;
	System m_xSystem;

};