#include "LoseState.h"
#include "stdafx.h"
#include <iostream>
#include "DrawManager.h"
#include "GameState.h"
#include "MenuState.h"
#include "IState.h"
#include "SpriteManager.h"
#include "HighScoreState.h"
#include "ScoreManager.h"
#include "SoundManager.h"
#include "Utility.h"

LoseState::LoseState(System& p_xSystem)
{

	//m_iScore = 9429;

	
	m_xSystem = p_xSystem;
	m_iScore = m_xSystem.m_pxScoreManager->GetHighScore();

	m_xMenuTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/Start_Menu_2.png");
	m_xMenuSprite.setTexture(m_xMenuTexture);

	m_xButtonTexture.loadFromFile("../assets/menyknappar.png");

	m_xCursor = m_xSystem.m_pxSpriteManager->GetTexture("../assets/muspekare.png");
	m_xNameBarTexture.loadFromFile("../assets/Highscorename.png");
	m_xNameBarSprite.setTexture(m_xNameBarTexture);
	m_xNameBarSprite.setPosition((width / 2) - 200, 700);
	m_xNameBarSprite.setOrigin(0,20);

	m_sEnterName = "Enter Name";

	

	m_xButtonSound = m_xSystem.m_pxSoundManager->CreateSound("../assets/MenuSound.wav");


	m_xCursorSprite.setTexture(m_xCursor);
	m_xCursorSprite.setTextureRect(sf::IntRect(0, 0, 60, 60));
	m_xCursorSprite.setOrigin(30, 30);

	// Laddar in font
	if (!font.loadFromFile("../assets/OldLondon.ttf"))
	{
		std::cout << "Unable to load textfile" << std::endl;
	}



	//Utseende meny
	menu[0].setFont(font);
	menu[0].setColor(sf::Color::Red);
	menu[0].setString("Play Again");
	menu[0].setPosition(sf::Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEMS + 1) * 1.5));
	menu[0].setCharacterSize(fontSize);
	menu[0].setOrigin(menu[0].getGlobalBounds().width / 2, menu[0].getGlobalBounds().height / 2);

	menu[1].setFont(font);
	menu[1].setColor(sf::Color::Black);
	menu[1].setString("Main Menu");
	menu[1].setPosition(sf::Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEMS + 1) * 2));
	menu[1].setCharacterSize(fontSize);
	menu[1].setOrigin(menu[1].getGlobalBounds().width / 2, menu[1].getGlobalBounds().height / 2);

	menu[2].setFont(font);
	menu[2].setColor(sf::Color::Black);
	menu[2].setString("Quit");
	menu[2].setPosition(sf::Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEMS + 1) * 2.5));
	menu[2].setCharacterSize(fontSize);
	menu[2].setOrigin(menu[1].getGlobalBounds().width / 2, menu[1].getGlobalBounds().height / 2);

	m_xPlayAgainSprite.setTexture(m_xButtonTexture);
	m_xPlayAgainSprite.setTextureRect(sf::IntRect(85, 316, 443, 207));
	m_xPlayAgainSprite.setPosition((m_xSystem.m_iScreenWidth / 2) - 165, (m_xSystem.m_iScreenHeight / 2) - 325);
	m_xPlayAgainSprite.setScale(0.8, 0.8);

	m_xMainMenuSprite.setTexture(m_xButtonTexture);
	m_xMainMenuSprite.setTextureRect(sf::IntRect(85, 64, 443, 207));
	m_xMainMenuSprite.setPosition((m_xSystem.m_iScreenWidth / 2) - 165, (m_xSystem.m_iScreenHeight / 2) - 175);
	m_xMainMenuSprite.setScale(0.8, 0.8);

	m_xExit.setTexture(m_xButtonTexture);
	m_xExit.setTextureRect(sf::IntRect(85, 1325, 443, 207));
	m_xExit.setPosition((m_xSystem.m_iScreenWidth / 2) - 165, (m_xSystem.m_iScreenHeight / 2) - 25);
	m_xExit.setScale(0.8, 0.8);





	m_xYourScore.setFont(font);
	m_xYourScore.setCharacterSize(100);
	m_xYourScore.setPosition((width / 2) - 220, 750);
	m_xYourScore.setString("Your Score");
	m_xYourScore.setColor(sf::Color::Black);

	m_xScoreString = std::to_string(m_iScore);
	m_xScoreText.setFont(font);
	m_xScoreText.setCharacterSize(80);
	m_xScoreText.setPosition((width / 2) - m_xScoreString.getSize() * 19, 850);
	m_xScoreText.setString(m_xScoreString);
	m_xScoreText.setColor(sf::Color::Black);


	

	lost.setFont(font);
	lost.setColor(sf::Color::Red);
	lost.setString("You Lost!");
	lost.setPosition(sf::Vector2f(width / 3, 20));
	lost.setCharacterSize(200);

	m_xScoreName.setCharacterSize(60);
	m_xScoreName.setFont(font);
	m_xScoreName.setColor(sf::Color::Black);
	m_xScoreName.setPosition((width / 2) , 700);
	m_xScoreName.setString(m_sEnterName);
	m_xScoreName.setOrigin(m_sEnterName.size() / 2, 30);
	m_xScoreName.setPosition((width / 2) - m_sEnterName.size() * 12, 700);
	

	selectedItemIndex = 0;
}

LoseState::~LoseState()
{


}


void LoseState::Draw()
{
	
	m_xSystem.m_pxDrawManager->Draw(m_xMenuSprite);

	m_xSystem.m_pxDrawManager->Draw(m_xMainMenuSprite);
	m_xSystem.m_pxDrawManager->Draw(m_xExit);
	m_xSystem.m_pxDrawManager->Draw(m_xPlayAgainSprite);


	m_xSystem.m_pxDrawManager->DrawText(lost);

	m_xSystem.m_pxDrawManager->Draw(m_xNameBarSprite);

	m_xSystem.m_pxDrawManager->DrawText(m_xScoreName);

	m_xSystem.m_pxDrawManager->DrawText(m_xScoreText);

	m_xSystem.m_pxDrawManager->DrawText(m_xYourScore);
	

	m_xSystem.m_pxDrawManager->Draw(m_xCursorSprite);
	

	
}

void LoseState::MoveUp()
{

	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setColor(sf::Color::Black);
		menu[selectedItemIndex].setCharacterSize(fontSize);
		selectedItemIndex--;
		menu[selectedItemIndex].setColor(sf::Color::Red);
		menu[selectedItemIndex].setCharacterSize(selectedFontSize);
	}

}

void LoseState::MoveDown()
{
	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
	{
		menu[selectedItemIndex].setColor(sf::Color::Black);
		menu[selectedItemIndex].setCharacterSize(fontSize);
		selectedItemIndex++;
		menu[selectedItemIndex].setColor(sf::Color::Red);
		menu[selectedItemIndex].setCharacterSize(selectedFontSize);
	}

}


//void LoseState::GetScore(int p_iScore)
//{
//	m_iScore = p_iScore;
//}
//
//int LoseState::SetScore()
//{
//	return m_iScore;
//}

bool LoseState::Update(float p_fDeltaTime)
{

	sf::Vector2f xMousePosition = static_cast<sf::Vector2f>(sf::Mouse::getPosition(*m_xSystem.m_pxDrawManager->GetRenderWindow()));
	m_xCursorSprite.setPosition(xMousePosition);
	m_xCursorSprite.setTexture(m_xCursor);

	

	if (m_xPlayAgainSprite.getGlobalBounds().contains(xMousePosition))
	{


		m_xPlayAgainSprite.setTexture(m_xButtonTexture);
		m_xPlayAgainSprite.setTextureRect(sf::IntRect(647, 316, 443, 207));
		if (m_xButtonSound->getStatus() != sf::Sound::Playing && m_bButtonBoolPlay)
		{
			m_xButtonSound->setPitch(Utility::RandomFloat(0.7, 1.3));
			m_xButtonSound->play();
			m_bButtonBoolPlay = false;

		}


		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			m_iNextState = 0;
			return false;

		}

	}

	else
	{
		m_xPlayAgainSprite.setTexture(m_xButtonTexture);
		m_xPlayAgainSprite.setTextureRect(sf::IntRect(85, 316, 443, 207));
		m_bButtonBoolPlay = true;
	}

	if (m_xMainMenuSprite.getGlobalBounds().contains(xMousePosition))
	{

		m_xMainMenuSprite.setTexture(m_xButtonTexture);
		m_xMainMenuSprite.setTextureRect(sf::IntRect(647, 64, 443, 207));
		if (m_xButtonSound->getStatus() != sf::Sound::Playing && m_bButtonBoolMenu)
		{
			m_xButtonSound->setPitch(Utility::RandomFloat(0.7, 1.3));
			m_xButtonSound->play();
			m_bButtonBoolMenu = false;
		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			m_iNextState = 1;
			return false;
		}

	}
	else
	{
		m_xMainMenuSprite.setTexture(m_xButtonTexture);
		m_xMainMenuSprite.setTextureRect(sf::IntRect(85, 64, 443, 207));
		m_bButtonBoolMenu = true;
	}


	if (m_xExit.getGlobalBounds().contains(xMousePosition))
	{

		m_xExit.setTexture(m_xButtonTexture);
		m_xExit.setTextureRect(sf::IntRect(647, 1325, 443, 207));
		if (m_xButtonSound->getStatus() != sf::Sound::Playing && m_bButtonBoolExit)
		{
			m_xButtonSound->setPitch(Utility::RandomFloat(0.7, 1.3));
			m_xButtonSound->play();
			m_bButtonBoolExit = false;
		}

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			m_iNextState = 100;
			return false;
		}

	}
	else
	{
		m_xExit.setTexture(m_xButtonTexture);
		m_xExit.setTextureRect(sf::IntRect(85, 1325, 443, 207));
		m_bButtonBoolExit = true;
	}


	while (m_xSystem.m_pxDrawManager->GetRenderWindow()->pollEvent(m_xEvent))
	{
		if (m_xEvent.type == sf::Event::Closed)
		{
			m_bQuit = true;
			return false;
		}
		if (m_xEvent.type == sf::Event::TextEntered)
		{
			// Handle ASCII characters only

			if ((m_xEvent.text.unicode >= 32 && m_xEvent.text.unicode <= 126) && m_sName.size() <= 12)
			{
				
					m_sName += static_cast<char>(m_xEvent.text.unicode);
					m_xScoreName.setString(m_sName);
					std::cout << "ASCII character typed: " << static_cast<char>(m_xEvent.text.unicode) << std::endl;
					m_xScoreName.setPosition((width / 2) - m_sName.size() * 12, 700);
				//m_sHighScoreName.insert(m_sName);
				

			}
			else if (m_xEvent.text.unicode == 13)
			{
				//m_iScore = 24363;
				m_iNextState = 2;
				return false;
			}

			else if (m_xEvent.text.unicode == 8 && m_sName.size() > 0)
				m_sName.erase(m_sName.size() - 1, 1);
				m_xScoreName.setString(m_sName);
				m_xScoreName.setPosition((width / 2) - m_sName.size() * 12, 700);
		
		}

	}



	return true;
}

IState* LoseState::NextState()
{
	if (m_bQuit)
	{
		return nullptr;
	}
	if(m_iNextState == 0)
		return new GameState(m_xSystem);

	if (m_iNextState == 1)
		return new MenuState(m_xSystem);
	if (m_iNextState == 2)
		return new HighScoreState(m_xSystem, m_iScore, m_sName);
	else
	{
		return nullptr;
	}
}
