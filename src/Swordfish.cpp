#include "stdafx.h"
#include "Swordfish.h"
#include "AnimatedTexture.h"
#include "Utility.h"
#include "SpriteManager.h"
#include "Player.h"
#include "SoundManager.h"
#include "FishFood.h"



Swordfish::Swordfish()
{
	
}

Swordfish::~Swordfish()
{

}

bool Swordfish::Initialize(SpriteManager * p_pxSpriteManager, SoundManager * p_pxSoundManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight)
{
	m_pxAnimatedTextureDefault = p_pxSpriteManager->CreateAnimatedTexture("../assets/SwordfishUniversal.png");
	m_pxAnimatedTextureDefault->AddFrame(0, 0, 306, 112, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(306, 0, 306, 112, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(612, 0, 306, 112, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(918, 0, 306, 112, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1224, 0, 306, 112, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1530, 0, 306, 112, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1836, 0, 306, 112, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(2142, 0, 306, 112, 0.1);
	

	m_pxAnimatedTextureCharge = p_pxSpriteManager->CreateAnimatedTexture("../assets/SwordfishUniversal.png");
	m_pxAnimatedTextureCharge->AddFrame(1531, 630, 309, 143, 0.05);
	m_pxAnimatedTextureCharge->AddFrame(1827, 630, 309, 143, 0.05);
	m_pxAnimatedTextureCharge->AddFrame(2137, 630, 309, 143, 0.05);
	m_pxAnimatedTextureCharge->AddFrame(1827, 630, 309, 143, 0.05);

	m_pxAnimatedTextureDying = p_pxSpriteManager->CreateAnimatedTexture("../assets/SwordfishUniversal.png");
	
	m_pxAnimatedTextureDying->AddFrame(0, 250, 306, 261, 0.1);
	m_pxAnimatedTextureDying->AddFrame(306, 250, 306, 261, 0.1);
	m_pxAnimatedTextureDying->AddFrame(612, 250, 306, 261, 0.1);
	m_pxAnimatedTextureDying->AddFrame(918, 250, 306, 240, 0.1);
	m_pxAnimatedTextureDying->AddFrame(1224, 250, 306, 261, 0.1); 
	m_pxAnimatedTextureDying->AddFrame(1530, 250, 306, 261, 0.1);
	m_pxAnimatedTextureDying->AddFrame(1836, 250, 306, 261, 0.1);

	m_pxSwordfishChargeSound = p_pxSoundManager->CreateSound("../assets/SwordfishCharge.wav");


	m_pxSprite.setTextureRect(m_pxAnimatedTextureDefault->GetRegion());
	m_pxSprite.setTexture(m_pxAnimatedTextureDefault->GetTexture());

	m_bIsCharging = false;
	m_fX = p_fX;
	m_fY = p_fY;
	m_pxSprite.setPosition(50, 70);
	m_fSpeed = 4.0f;
	m_fDirX = -1.0f;
	m_fDirY = 0.0f;
	while (m_fDirY == 0)
	{
		m_fDirY = Utility::RandomFloat(-1.0f, 1.0f);
	}
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iscreenHeight;

	m_iState = NEUTRAL;

	return true;
}

void Swordfish::Update(float p_fDeltaTime, Player* p_pxPlayer)
{
	if (m_bDying)
	{
		DyingStateUpdate(p_fDeltaTime);
	}
	else if (p_pxPlayer->GetFishFood() != nullptr)
	{
		float fAngle = atan2(p_pxPlayer->GetFishFood()->GetY() - m_fY, p_pxPlayer->GetFishFood()->GetX() - m_fX);
		float cosine = cos(fAngle);
		float sine = sin(fAngle);
		m_pxSprite.setPosition(sf::Vector2f(m_fX += cosine * m_fSpeed, m_fY += sine * m_fSpeed));
	}
	else if (m_iState == NEUTRAL)
	{
		NeutralStateUpdate(p_fDeltaTime, p_pxPlayer);
	}

	else if (m_iState == CHARGE)
	{
		ChargeStateUpdate(p_fDeltaTime);
	}
	if (m_fY <= 200 || m_fY >= 900)
	{
		ReverseDirectionY();
	}

}

void Swordfish::NeutralStateUpdate(float p_fDeltaTime, Player* p_pxPlayer)
{
	m_bIsCharging = false;
	m_pxAnimatedTextureDefault->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTextureDefault->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTextureDefault->GetRegion());

	m_pxSprite.setPosition(m_fX += m_fDirX * m_fSpeed * 2, m_fY += m_fDirY * m_fSpeed);
	
	if (p_pxPlayer->GetY() + (p_pxPlayer->GetSprite().getTextureRect().height / 2) > m_fY && p_pxPlayer->GetY() < m_fY && p_pxPlayer->GetX() < m_fX)
	{
		m_iState = CHARGE;
		m_fDirY = 0.0f;
		m_fSpeed = 15.0f;
		m_pxSwordfishChargeSound->play();
	}

	if (m_fY <= 100 || m_fY >= 850)
	{
		ReverseDirectionY();
	}
	
}

void Swordfish::ChargeStateUpdate(float p_fDeltaTime)
{
	m_pxAnimatedTextureCharge->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTextureCharge->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTextureCharge->GetRegion());
	m_pxSprite.setPosition(m_fX += m_fDirX * m_fSpeed, m_fY += m_fDirY * m_fSpeed);

	
}

void Swordfish::DyingStateUpdate(float p_fDeltaTime)
{
	m_fSpeed = -4;
	DyingTimer += p_fDeltaTime;
	m_pxAnimatedTextureDying->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTextureDying->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTextureDying->GetRegion());
	if (DyingTimer >= DyingTimerTarget)
	{
		m_iState = NEUTRAL;
		m_pxAnimatedTextureDying->Reset();
		DyingTimer = 0.0;
		m_bActive = false;
		m_bDying = false;
	}
}

sf::Sprite Swordfish::GetSprite() { return m_pxSprite; }

AnimatedTexture* Swordfish::GetTexture()
{
	return m_pxAnimatedTextureCharge;
}

float Swordfish::GetX() { return m_fX; }

float Swordfish::GetY() { return m_fY; }

bool Swordfish::IsActive() { return m_bActive; }

void Swordfish::Deactivate()
{
	m_bActive = false;
	m_iState = NEUTRAL;
}

void Swordfish::Kill()
{
	m_bDying = true;
}

void Swordfish::Activate()
{

	m_bActive = true;
	m_bDying = false;
	m_bIsCharging = false;
	m_fX = m_iScreenWidth;

	m_fY = Utility::RandomFloat(200,900);
	m_fDirY = Utility::RandomFloat(-1.0f, 1.0f);
	m_fSpeed = 4.0f;
}


EENTITYTYPE Swordfish::GetType() { return EENTITYTYPE::ENTITY_SWORDFISH; }

void Swordfish::ReverseDirectionY() { m_fDirY *= -1; }

bool Swordfish::IsDying()
{
	return m_bDying;
}

