#include "stdafx.h"
#include "Player.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Harpoon.h"
#include "FishFood.h"
#include "SoundManager.h"
#include "GoldenHarpoon.h"
#include "Utility.h"
#include "PowerUpHUD.h"


Player::Player(SpriteManager* p_pxSpriteManager, sf::RenderWindow* p_pxWindow, SoundManager* p_pxSoundManager, PowerUpHUD* p_pxPowerUpHUD, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxPowerUpHUD = p_pxPowerUpHUD;
	m_pxSpriteManager = p_pxSpriteManager;
	m_pxAnimatedTexture = m_pxSpriteManager->CreateAnimatedTexture("../assets/SkipSwims.png");
	m_pxAnimatedTexture->AddFrame(0, 0, 240, 185, 0.1);
	m_pxAnimatedTexture->AddFrame(243, 0, 240, 185, 0.15);
	m_pxAnimatedTexture->AddFrame(486, 0, 240, 185, 0.1);
	m_pxAnimatedTexture->AddFrame(729, 0, 240, 185, 0.1);
	m_pxAnimatedTexture->AddFrame(972, 0, 240, 185, 0.1);
	m_pxAnimatedTexture->AddFrame(1215, 0, 240, 185, 0.1);
	m_pxAnimatedTexture->AddFrame(1458, 0, 240, 185, 0.1);

	m_pxAnimatedTextureStunned = m_pxSpriteManager->CreateAnimatedTexture("../assets/SkipElectrocution.png");
	m_pxAnimatedTextureStunned->AddFrame(0, 0, 256, 165, 0.1);
	m_pxAnimatedTextureStunned->AddFrame(256, 0, 256, 165, 0.1);
	m_pxAnimatedTextureStunned->AddFrame(512, 0, 256, 165, 0.1);
	m_xBubbleSkip.loadFromFile("../assets/Bubblaforskip.png");
	m_xBubbleSkipSprite.setOrigin(174.5f, 109.5f);
	
	
	
	m_xShoot = p_pxSoundManager->CreateSound("../assets/ShootHarpoon.wav");
	
	m_xTakeDamage = p_pxSoundManager->CreateSound("../assets/PlayerTakeDamage3.wav");
	m_xTakeDamage2 = p_pxSoundManager->CreateSound("../assets/PlayerTakeDamage2.wav");
	m_xGoldenHarpoonSound = p_pxSoundManager->CreateSound("../assets/GoldenProjectile.wav");
	m_xShootSound = p_pxSoundManager->CreateSound("../assets/ShootSound.wav");
	m_xTakeDamage->setVolume(30);
	m_xTakeDamage2->setVolume(30);

	m_xPowerUp = p_pxSoundManager->CreateSound("../assets/PowerUpSound.wav");
	
	m_pxAnimatedTextureShooting = m_pxSpriteManager->CreateAnimatedTexture("../assets/SkipShooting.png");
	m_pxAnimatedTextureShooting->AddFrame(0, 0, 313, 150, 0.1);
	m_pxAnimatedTextureShooting->AddFrame(313, 0, 313, 150, 0.1);
	m_pxAnimatedTextureShooting->AddFrame(626, 0, 313, 150, 0.1);
	m_pxAnimatedTextureShooting->AddFrame(939, 0, 313, 150, 0.1);

	m_pxAnimatedTextureHarpoon = m_pxSpriteManager->CreateAnimatedTexture("../assets/Harpoon.png");
	m_pxAnimatedTextureHarpoon->AddFrame(0, 0, 380, 144, 0.5);

	m_pxAnimatedTextureGoldenHarpoon = p_pxSpriteManager->CreateAnimatedTexture("../assets/GoldenHarpoon2.png");
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(0, 0, 324, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(328, 0, 337, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(660, 0, 331, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(992, 0, 327, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(1318, 0, 337, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(1655, 0, 331, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(1989, 0, 324, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(2312, 0, 328, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(2643, 0, 337, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(2979, 0, 321, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(3304, 0, 328, 150, 0.05);
	m_pxAnimatedTextureGoldenHarpoon->AddFrame(3644, 0, 332, 150, 0.05); 

	m_xRopeTexture.loadFromFile("../assets/Rope.png");
	m_xRopeTexture.setRepeated(true);
	m_xRopeSprite.setTexture(m_xRopeTexture);
	m_xRopeSprite.setTextureRect(sf::IntRect(17, 30, 355, 80));


	m_xCollitionRect = sf::FloatRect(60, 50, 120, 70);
	m_xSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_xSprite.setPosition(sf::Vector2f(200,200));
	m_pxWindow = p_pxWindow;
	m_pxSoundManager = p_pxSoundManager;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	m_iState = DEFAULT;
	m_bIsCharging = false;
	//m_bActiveHarpoon = false;
	m_pxHarpoon = nullptr;
}
Player::~Player()
{

	if (m_pxHarpoon != nullptr)
	{
		delete m_pxHarpoon;
		m_pxHarpoon = nullptr;
	}
	if (m_pxFishFood != nullptr)
	{
		delete m_pxFishFood;
		m_pxFishFood = nullptr;
	}

}
void Player::Update(float p_fDeltaTime, sf::View* p_pxView)
{
	m_pxBools = m_pxPowerUpHUD->GetBools();
	if (m_iState == DEFAULT)
	{
		NormalStateUpdate(p_fDeltaTime);
	}

	if (m_iState == AMULET)
	{
		AmuletStateUpdate(p_fDeltaTime);
	}

	if (m_iState == STUNNED)
	{
		StunnedStateUpdate(p_fDeltaTime);
	}


	if (m_pxHarpoon != nullptr && GetCollitionRect().intersects(m_pxHarpoon->GetSprite().getGlobalBounds()))
	{
		delete m_pxHarpoon;
		m_pxHarpoon = nullptr;
	}

	
	

	if (m_fX < 150)
	{
		m_fX = 150;
	}
	if (m_fX + m_xSprite.getTextureRect().width > m_iScreenWidth)
	{
		m_fX = m_iScreenWidth - m_xSprite.getTextureRect().width;
	}
	if (m_fY < 0)
	{
		m_fY = 0;
	}
	if (m_fY + m_xSprite.getTextureRect().height > m_iScreenHeight)
	{
		m_fY = m_iScreenHeight - m_xSprite.getTextureRect().height;
	}

	m_xCollitionRect = sf::FloatRect(m_fX + 60, m_fY + 50, 120, 70);

	if (m_pxHarpoon != nullptr && m_pxHarpoon->GetX() > 1920)
	{
		delete m_pxHarpoon;
		m_pxHarpoon = nullptr;
		m_bGoldenHarpoon = false;
	}

	if (m_pxHarpoon != nullptr && m_pxHarpoon->GetY() > 1080)
	{
		delete m_pxHarpoon;
		m_pxHarpoon = nullptr;
		m_bGoldenHarpoon = false;
	}

	if (m_pxHarpoon != nullptr && m_pxHarpoon->GetX() < 0 )
	{
		delete m_pxHarpoon;
		m_pxHarpoon = nullptr;
		m_bGoldenHarpoon = false;
	}
	if (m_pxHarpoon != nullptr && m_pxHarpoon->GetY() < 0)
	{
		delete m_pxHarpoon;
		m_pxHarpoon = nullptr;
		m_bGoldenHarpoon = false;
	}


	if (m_pxFishFood != nullptr && m_pxFishFood->GetX() > 1920)
	{
		delete m_pxFishFood;
		m_pxFishFood = nullptr;
	}

	if (m_pxFishFood != nullptr && m_pxFishFood->GetY() > 1080)
	{
		delete m_pxFishFood;
		m_pxFishFood = nullptr;
	}

	if (m_pxFishFood != nullptr && m_pxFishFood->GetX() < 0)
	{
		delete m_pxFishFood;
		m_pxFishFood = nullptr;
	}
	if (m_pxFishFood != nullptr && m_pxFishFood->GetY() < 0)
	{
		delete m_pxFishFood;
		m_pxFishFood = nullptr;
	}

	if (m_bShotShake)
	{ 
		CameraShake(p_pxView, p_fDeltaTime);
	}

}


void Player::NormalStateUpdate(float p_fDeltaTime)
{
	if (m_bHit == false)
	{
		m_xSprite.setColor(sf::Color::White);
	}
	if (!m_bShoot)
	{
		m_pxAnimatedTexture->Update(p_fDeltaTime);
		m_xSprite.setTexture(m_pxAnimatedTexture->GetTexture());
		m_xSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
		m_xSprite.setColor(sf::Color(255, 255, 255, 255));
	
	}
	else
	{
	


		m_fShootTimer += p_fDeltaTime;
		m_pxAnimatedTextureShooting->Update(p_fDeltaTime);
		m_xSprite.setTexture(m_pxAnimatedTextureShooting->GetTexture());
		m_xSprite.setTextureRect(m_pxAnimatedTextureShooting->GetRegion());
		m_xSprite.setColor(sf::Color(255, 255, 255, 255));
		if (m_fShootTimer >= 0.4f)
		{
			m_bShoot = false;
			m_fShootTimer = 0.0f;
		}
	}
	CheckInput();
	if (m_pxHarpoon != nullptr)
	{
		m_pxHarpoon->Update(p_fDeltaTime, this);

		m_xRopeSprite.setPosition(m_fX + m_xSprite.getTextureRect().width, m_fY + (m_xSprite.getTextureRect().height / 2));
		m_xRopeSprite.setOrigin(0, 0);
		float xAngle = atan2((m_pxHarpoon->GetY()) - ((GetY() + m_xSprite.getTextureRect().height) / 2) , m_pxHarpoon->GetX() - GetX() + m_xSprite.getTextureRect().width);
		m_xRopeSprite.setRotation(((xAngle * 180) / M_PI));
		m_xRopeSprite.setScale((sqrt(pow(m_pxHarpoon->GetX() - m_fX, 2) + pow(m_pxHarpoon->GetY() - m_fY, 2))) / 355, 0.1);
	}
	if (m_pxFishFood != nullptr)
	{
		m_pxFishFood->Update(p_fDeltaTime);
		
	}
}

void Player::AmuletStateUpdate(float p_fDeltaTime)
{
	NormalStateUpdate(p_fDeltaTime);
	//m_xSprite.setColor(sf::Color(255, 255, 51, 60));


}

void Player::StunnedStateUpdate(float p_fDeltaTime)
{
	m_pxAnimatedTextureStunned->Update(p_fDeltaTime);
	m_xSprite.setTexture(m_pxAnimatedTextureStunned->GetTexture());
	m_xSprite.setTextureRect(m_pxAnimatedTextureStunned->GetRegion());
	m_fStunnedTimer += p_fDeltaTime;
	if (m_fStunnedTimer >= 2)
	{
		m_iState = DEFAULT;
		m_fStunnedTimer = 0.0;
	}
	if (m_pxHarpoon != nullptr)
	{
		m_pxHarpoon->Update(p_fDeltaTime, this);
	}
	if (m_pxFishFood != nullptr)
	{
		m_pxFishFood->Update(p_fDeltaTime);

	}
}
/*
void Player::GHarpoonStateUpdate(float p_fDeltaTime)
{
	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_xSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_xSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());

	CheckInput();
}*/


void Player::CheckInput()
{	
	if (m_bBoatHit == true)
	{
		m_fVelY += 2;
	}

	if (m_bHit == true)
	{
		m_fVelX -= 0.7;
	}

	else
	{ 
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && m_fVelX <= m_fVelMax)
		{
			m_fVelX += 1;
		}
		else if (m_fVelX > 0)
		{
			m_fVelX -= 0.1;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) && m_fVelX >= -m_fVelMax)
	{
		m_fVelX -= 1;
	}
	else if (m_fVelX < 0)
	{
		m_fVelX += 0.1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && m_fVelY <= m_fVelMax)
	{
		m_fVelY += 1;
	}
	else if (m_fVelY > 0)
	{
		m_fVelY -= 0.1;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && m_fVelY >= -m_fVelMax)
	{
		m_fVelY -= 1;
	}
	else if (m_fVelY < 0)
	{
		m_fVelY += 0.1;
		if (m_fVelY > -0.1)
		{
			m_fVelY = 0;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && m_bIsCharging == false)
	{
		m_bIsCharging = true;
		m_fVelMax = 8;
	}
	else if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && m_bIsCharging == true)
	{
		m_bIsCharging = false;
		m_fVelMax = 4;
	}
	

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_pxHarpoon == nullptr)
	{
		
		
		m_pxHarpoon = new Harpoon(m_pxAnimatedTextureHarpoon, m_pxWindow, m_fX + m_xSprite.getTextureRect().width + 10, m_fY + (m_xSprite.getTextureRect().height / 2), m_iScreenWidth, m_iScreenHeight);
		m_bIsShooting = true;
		m_bShoot = true;

		m_fRandomPitch = Utility::RandomFloat(0.8, 1.2);
		m_xShoot->setPitch(m_fRandomPitch);
		m_xShoot->play();
		m_xShootSound->play();
		m_bShotShake = true;
	}
	
	
	if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
	{
		if (m_pxGoldenHarpoon == nullptr && m_pxBools.m_bGoldenHarpoon && m_pxBools.m_bGoldenHarpoonSelected)
		{
		
				m_pxHarpoon = new Harpoon(m_pxAnimatedTextureGoldenHarpoon, m_pxWindow, m_fX + m_xSprite.getTextureRect().width + 10, m_fY + (m_xSprite.getTextureRect().height / 2), m_iScreenWidth, m_iScreenHeight);
				m_bIsShooting = true;
				m_bShoot = true;
				m_xGoldenHarpoonSound->play(); 
				m_fRandomPitch = Utility::RandomFloat(0.8, 1.2);
				m_xShoot->setPitch(m_fRandomPitch);
				m_xShoot->play();
				m_xShootSound->play();
				m_bShotShake = true;
				m_bGoldenHarpoon = true;
				m_pxPowerUpHUD->RemovePowerup(HUD_HARPOON);
			
		}
		if (m_pxFishFood == nullptr && m_pxBools.m_bFishFood && m_pxBools.m_bFishFoodSelected)
		{
			m_pxFishFood = new FishFood(m_pxSpriteManager, m_pxWindow, m_fX + m_xSprite.getTextureRect().width + 100, m_fY + (m_xSprite.getTextureRect().height / 2), m_iScreenWidth, m_iScreenHeight);

			m_pxPowerUpHUD->RemovePowerup(HUD_FISHFOOD);
		}
		if (m_pxBools.m_bAmulet && m_pxBools.m_bAmuletSelected)
		{
			SetState(AMULET);
			m_pxPowerUpHUD->RemovePowerup(HUD_AMULET);
		}

		
	}
	m_xSprite.setPosition(sf::Vector2f(m_fX += m_fVelX, m_fY += m_fVelY));
	



}

void Player::SetState(int p_iNewState)
{
	m_iState = p_iNewState;
}


void Player::SetHitColorRed()
{
	m_xSprite.setColor(sf::Color::Red);
}

void Player::SetHitColorWhite()
{
	m_xSprite.setColor(sf::Color::White);
}

void Player::IsHit(bool m_bSetHit)
{
	m_bHit = m_bSetHit;
}

void Player::IsBoatHit(bool m_bSetBoatHit)
{
	m_bBoatHit = m_bSetBoatHit;
}

bool Player::GetHit()
{
	return m_bHit;
}

bool Player::GetBoatHit()
{
	return m_bBoatHit;
}


FishFood * Player::GetFishFood()
{
	return m_pxFishFood;
}

int Player::GetState()
{
	return m_iState;
}
sf::Sprite Player::GetSprite() { return m_xSprite; }

sf::Sprite Player::GetRopeSprite()
{
	return m_xRopeSprite;
}

sf::FloatRect Player::GetCollitionRect()
{
	return m_xCollitionRect;
}

Harpoon* Player::GetHarpoon() { return m_pxHarpoon; }



float Player::GetX() { return m_fX; }

float Player::GetY() { return m_fY; }

bool Player::GetCharge() { return m_bIsCharging; }

bool Player::GetGolden()
{
	return m_bGoldenHarpoon;
}

void Player::SetGolden()
{
	m_bGoldenHarpoon = true;
}

void Player::CameraShake(sf::View* p_pxView, float p_fDeltaTime)
{
	if(m_bGoldenHarpoon)
	{
		p_pxView->setCenter(Utility::RandomFloat(930, 980), Utility::RandomFloat(510, 570));
	}
	else
	{
		p_pxView->setCenter(Utility::RandomFloat(950, 970), Utility::RandomFloat(530, 550));
	}
	m_fCameraTimer += p_fDeltaTime;
	if (m_fCameraTimer >= 0.35 && !m_bGoldenHarpoon)
	{
		p_pxView->setCenter(m_iScreenWidth / 2, m_iScreenHeight / 2);
		m_bShotShake = false;
		m_fCameraTimer = 0;
	}
	else if (m_fCameraTimer >= 0.4 && m_bGoldenHarpoon)
		{
			p_pxView->setCenter(m_iScreenWidth / 2, m_iScreenHeight / 2);
			m_bShotShake = false;
			m_fCameraTimer = 0;
		}
	
	
}

void Player::GetTakeDamageSound()
{
	m_iRandomSound = Utility::RandomFloat(1, 20);
	if(m_iRandomSound < 10)
	{
		m_xTakeDamage->play();
	}
	else if (m_iRandomSound > 10)
	{
		m_xTakeDamage2->play();
	}
}

void Player::GetPowerUpSound()
{
	m_xPowerUp->play();
}

EENTITYTYPE Player::GetType() { return EENTITYTYPE::ENTITY_PLAYER; }


