#pragma once

#include "IEntity.h"


class sf::Sprite;
class SpriteManager;


class BackgroundElement
{
public:
	BackgroundElement();
	~BackgroundElement();
	void Initialize(sf::Texture p_xTexture, sf::IntRect p_xTextureRect, float p_fX, float p_fY, int depth, int p_iScreenWidth, int p_iscreenHeight, bool p_bScale, bool p_bIsBackground);
	void Update(float p_fDeltaTime);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	int GetDepth();
	bool IsActive();
	void Deactivate();
	void Activate(float p_fX, float p_fY);

private:
	sf::Sprite m_xSprite;
	sf::Texture m_xTexture;
	float m_fSpeed;
	float m_fX;
	float m_fY;
	int m_iDepth;
	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bActive = false;
}; 