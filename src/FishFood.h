#pragma once


#include "IEntity.h"

class sf::Sprite;
class SpriteManager;
class Collider;

class FishFood : public IEntity
{
public:
	FishFood(SpriteManager* p_pxSpriteManager, sf::RenderWindow* p_pxWindow, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight);
	~FishFood();
	void Update(float p_fDeltaTime);
	sf::Sprite GetSprite();
	EENTITYTYPE GetType();
	float GetX();
	float GetY();

private:
	AnimatedTexture* m_pxAnimatedTexture;
	sf::Sprite m_pxSprite;
	float m_fX;
	float m_fY;
	float PosX;
	float PosY;
	float m_fAngle;
	float m_fVelocity;
	bool m_bVisible;
}; 