#pragma once

#include "IEntity.h"

class sf::Sprite;
class SpriteManager;
class Collider;

class GoldenHarpoonPickup : public IEntity
{
public:
	GoldenHarpoonPickup(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight);
	~GoldenHarpoonPickup();
	void Update(float p_fDeltaTime);
	sf::Sprite GetSprite();
	Collider* GetCollider();
	EENTITYTYPE GetType();
	float GetX();
	float GetY();

private:
	AnimatedTexture* m_pxAnimatedTexture;
	sf::Sprite m_pxSprite;
	Collider* m_pxCollider;
	float m_fX;
	float m_fY;
	float PosX;
	float PosY;
	bool m_bVisible;
};