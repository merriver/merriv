#include "stdafx.h"
#include "Piranha.h"
#include "AnimatedTexture.h"
#include "Utility.h"
#include "SpriteManager.h"
#include "Player.h"
#include "FishFood.h"

Piranha::Piranha()
{
	

}

Piranha::~Piranha()
{
}

bool Piranha::Initialize(SpriteManager * p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxAnimatedTexture = p_pxSpriteManager->CreateAnimatedTexture("../assets/PiranhaSwims.png");
	m_pxAnimatedTexture->AddFrame(0, 0, 260, 300, 0.1);
	m_pxAnimatedTexture->AddFrame(313, 0, 260, 300, 0.1);
	m_pxAnimatedTexture->AddFrame(613, 0, 260, 300, 0.1);
	m_pxAnimatedTexture->AddFrame(905, 0, 260, 300, 0.1);

	m_pxAnimatedTextureDying = p_pxSpriteManager->CreateAnimatedTexture("../assets/pirayadies.PNG");
	m_pxAnimatedTextureDying->AddFrame(125, 15, 245, 215, 0.1);
	m_pxAnimatedTextureDying->AddFrame(620, 0, 235, 225, 0.1);
	m_pxAnimatedTextureDying->AddFrame(1150, 70, 200, 145, 0.1);
	m_pxAnimatedTextureDying->AddFrame(1670, 80, 160, 130, 0.1);
	m_pxAnimatedTextureDying->AddFrame(2165, 75, 175, 150, 0.1);
	m_pxAnimatedTextureDying->AddFrame(2675, 70, 137, 180, 0.1);
	m_pxAnimatedTextureDying->AddFrame(3185, 80, 170, 165, 0.1);
	m_pxAnimatedTextureDying->AddFrame(3655, 75, 180, 150, 0.1);

	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());

	m_fX = p_fX;
	m_fY = p_fY;
	m_pxSprite.setPosition(50, 70);
	m_fSpeed = 4.0f;
	m_fDirX = -1.0f;
	m_fDirY = 0.0f;
	m_fRand = Utility::RandomFloat(1.0f, 10.0f);

	if (m_fRand >= 1.0f && m_fRand <= 5)
	{
		m_fDirY = -2.0f;
	}
	else if (m_fRand > 5 && m_fRand <= 10.0f)
	{
		m_fDirY = 2.0;
	}
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iScreenHeight;
	return true;
}

void Piranha::Update(float p_fDeltaTime, Player* p_pxPlayer)
{
	if (m_bDying)
	{
		DyingStateUpdate(p_fDeltaTime);
	}
	else if (p_pxPlayer->GetFishFood() != nullptr)
	{
		float fAngle = atan2(p_pxPlayer->GetFishFood()->GetY() - m_fY, p_pxPlayer->GetFishFood()->GetX() - m_fX);
		float cosine = cos(fAngle);
		float sine = sin(fAngle);
		m_pxSprite.setPosition(sf::Vector2f(m_fX += cosine * m_fSpeed, m_fY += sine * m_fSpeed));
	}
	else
	{
		m_pxAnimatedTexture->Update(p_fDeltaTime);
		m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
		m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
		m_pxSprite.setPosition(m_fX += m_fDirX * m_fSpeed, m_fY += m_fDirY * m_fSpeed);
		m_xCollitionRect = sf::FloatRect(m_fX + 10, m_fY + 30, 120, 100);
	}
	
	
	if (m_fY <= 100 || m_fY >= 850)
	{
		ReverseDirectionY();
	}
	
	
}

void Piranha::DyingStateUpdate(float p_fDeltaTime)
{
	DyingTimer += p_fDeltaTime;
	m_pxAnimatedTextureDying->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTextureDying->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTextureDying->GetRegion());
	if (DyingTimer >= DyingTimerTarget)
	{
		m_pxAnimatedTextureDying->Reset();
		DyingTimer = 0.0;
		m_bActive = false;
		m_bDying = false;
	}
}

sf::Sprite Piranha::GetSprite()
{
	return m_pxSprite;
}

AnimatedTexture * Piranha::GetTexture()
{
	return m_pxAnimatedTexture;
}

float Piranha::GetX()
{
	return m_fX;
}

float Piranha::GetY()
{
	return m_fY;
}

bool Piranha::IsActive()
{
	return m_bActive;
}

void Piranha::Kill()
{
	m_bDying = true;
}

void Piranha::Deactivate()
{
	m_bActive = false;
}

void Piranha::Activate()
{
	m_bActive = true;
	m_fX = m_iScreenWidth;
	m_fY = Utility::RandomFloat(200.0f, 800.0f);
	m_pxSprite.setPosition(m_fX, m_fY);
	m_fRand = Utility::RandomFloat(1.0f, 10.0f);

	if (m_fRand >= 1.0f && m_fRand <= 5)
	{
		m_fDirY = -2.0f;
	}
	else if (m_fRand > 5 && m_fRand <= 10.0f)
	{
		m_fDirY = 2.0;
	}
}

EENTITYTYPE Piranha::GetType()
{
	return EENTITYTYPE::ENTITY_PIRANHA;
}

sf::FloatRect Piranha::GetCollitionRect()
{
	return m_xCollitionRect;
}


void Piranha::ReverseDirectionY()
{
	m_fDirY *= -1.0f;
}

bool Piranha::IsDying()
{
	return m_bDying;
}
