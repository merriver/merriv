#pragma once


class AnimatedTexture;

class SpriteManager
{
public:
	SpriteManager();
	~SpriteManager();


	AnimatedTexture* CreateAnimatedTexture(const std::string& p_sFilepath);
	sf::Texture GetTexture(const std::string& p_sFilepath);
	void LoadTextures();
	

private:
	std::map<std::string, sf::Texture> m_apxTextures;
	std::vector<AnimatedTexture*> m_apxAnimatedTextures;
};