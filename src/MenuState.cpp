#include "MenuState.h"
#include "stdafx.h"
#include <iostream>
#include "DrawManager.h"
#include "GameState.h"
#include "SettinsState.h"
#include "SpriteManager.h"
#include "IState.h"
#include "SoundManager.h"
#include "Utility.h"
#include "HighScoreState.h"

MenuState::MenuState(System& p_xSystem)
{
	m_xSystem = p_xSystem;

	m_xSkipSound = m_xSystem.m_pxSoundManager->CreateSound("../assets/TickleSound.wav");

	m_xMenuTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/Start_Menu_2.png");
	m_xButtonTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/menyknappar.png");
	m_xMenuTexture.loadFromFile("../assets/Start_Menu_3.png");
	m_xSkipTexture.loadFromFile("../assets/SkiponCliff.png");
	m_xSkipLine = m_xSystem.m_pxSoundManager->CreateSound("../assets/RandomSkipLine2.wav");
	m_xButtonSound = m_xSystem.m_pxSoundManager->CreateSound("../assets/MenuSound.wav");

	m_xSkipSprite.setTexture(m_xSkipTexture);
	m_xSkipSprite.setPosition(160, 400);
	m_xSkipSprite.rotate(-20);
	m_xSkipSprite.setOrigin(150, 175);

	m_xCursor = m_xSystem.m_pxSpriteManager->GetTexture("../assets/muspekare.png");
	m_xCursorSprite.setTexture(m_xCursor);
	m_xCursorSprite.setTextureRect(sf::IntRect(0, 0, 60, 60));
	m_xCursorSprite.setOrigin(30, 30);
	
	m_xPlay.setTexture(m_xButtonTexture);
	m_xPlay.setTextureRect(sf::IntRect(85, 316, 443, 207));
	m_xPlay.setPosition((m_xSystem.m_iScreenWidth / 2) + 400, (m_xSystem.m_iScreenHeight / 2) - 400);
	m_xPlay.setScale(0.8, 0.8);

	m_xHighScore.setTexture(m_xButtonTexture);
	m_xHighScore.setTextureRect(sf::IntRect(85, 570, 443, 207));
	m_xHighScore.setPosition((m_xSystem.m_iScreenWidth / 2) + 400, (m_xSystem.m_iScreenHeight / 2) - 200);
	m_xHighScore.setScale(0.8, 0.8);

	m_xSettings.setTexture(m_xButtonTexture);
	m_xSettings.setTextureRect(sf::IntRect(85, 820, 443, 207));
	m_xSettings.setPosition((m_xSystem.m_iScreenWidth / 2) + 400, (m_xSystem.m_iScreenHeight / 2));
	m_xSettings.setScale(0.8, 0.8);

	m_xExit.setTexture(m_xButtonTexture);
	m_xExit.setTextureRect(sf::IntRect(85, 1325, 443, 207));
	m_xExit.setPosition((m_xSystem.m_iScreenWidth / 2) + 400, (m_xSystem.m_iScreenHeight / 2) + 200);
	m_xExit.setScale(0.8, 0.8);
	m_xMenuSprite.setTexture(m_xMenuTexture);
	

	// Laddar in font
	if (!font.loadFromFile("../assets/OldLondon.ttf"))
	{
		std::cout << "Unable to load textfile" << std::endl;
	}

	//Utseende meny
	menu[0].setFont(font);
	menu[0].setColor(sf::Color::Blue);
	menu[0].setString("Play");
	menu[0].setPosition(sf::Vector2f(width / 3, height / (MAX_NUMBER_OF_ITEMS + 1) * 1));
	menu[0].setCharacterSize(fontSize);

	menu[1].setFont(font);
	menu[1].setColor(sf::Color::White);
	menu[1].setString("Settings");
	menu[1].setPosition(sf::Vector2f(width / 3, height / (MAX_NUMBER_OF_ITEMS + 1) * 2));
	menu[1].setCharacterSize(fontSize);

	menu[2].setFont(font);
	menu[2].setColor(sf::Color::White);
	menu[2].setString("Exit");
	menu[2].setPosition(sf::Vector2f(width / 3, height / (MAX_NUMBER_OF_ITEMS + 1) * 3));
	menu[2].setCharacterSize(fontSize);
}

MenuState::~MenuState()
{
	

}


void MenuState::Draw()
{
	/*for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{
		
		m_xSystem.m_pxDrawManager->DrawText(menu[i]);
	}
	*/

	
	m_xSystem.m_pxDrawManager->Draw(m_xMenuSprite);

	m_xSystem.m_pxDrawManager->Draw(m_xPlay);
	m_xSystem.m_pxDrawManager->Draw(m_xSettings);
	m_xSystem.m_pxDrawManager->Draw(m_xExit);
	m_xSystem.m_pxDrawManager->Draw(m_xHighScore);
	//m_xSystem.m_pxDrawManager->Draw(m_xSkipSprite);
	m_xSystem.m_pxDrawManager->Draw(m_xCursorSprite);
	
	

}

void MenuState::MoveUp()
{

	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setColor(sf::Color::White);
		menu[selectedItemIndex].setCharacterSize(fontSize);
		selectedItemIndex--;
		menu[selectedItemIndex].setColor(sf::Color::Blue);
		menu[selectedItemIndex].setCharacterSize(selectedFontSize);
	}

}

void MenuState::MoveDown()
{
	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
	{
		menu[selectedItemIndex].setColor(sf::Color::White);
		menu[selectedItemIndex].setCharacterSize(fontSize);
		selectedItemIndex++;
		menu[selectedItemIndex].setColor(sf::Color::Blue);
		menu[selectedItemIndex].setCharacterSize(selectedFontSize);
	}

}

bool MenuState::Update(float p_fDeltaTime)
{

	sf::Vector2f xMousePosition = static_cast<sf::Vector2f>(sf::Mouse::getPosition(*m_xSystem.m_pxDrawManager->GetRenderWindow()));
	m_xCursorSprite.setPosition(xMousePosition);
	m_xCursorSprite.setTexture(m_xCursor);

	while (m_xSystem.m_pxDrawManager->GetRenderWindow()->pollEvent(m_xEvent))
	{
		if (m_xEvent.type == sf::Event::Closed)
		{
			m_bQuit = true;
			return false;
		}
	}

	if (m_bViewBool && m_fMoveViewY <= 1620)
	{
		m_fMoveViewY += 5;
		m_fMoveSkip += 6;
		m_xSystem.m_pxDrawManager->GetView()->setCenter(960, m_fMoveViewY);
		m_xSystem.m_pxDrawManager->SetView();

		m_xSkipSprite.setTexture(m_xSkipTexture);

		m_xSkipSprite.setPosition(200, m_fMoveSkip);
		

		if (m_fMoveViewY >= 1620)
		{
			iMenu = 0;
			return false;
		}

	}


		if (m_xPlay.getGlobalBounds().contains(xMousePosition))
		{

			
			m_xPlay.setTexture(m_xButtonTexture);
			m_xPlay.setTextureRect(sf::IntRect(647, 316, 443, 207));
			if (m_xButtonSound->getStatus() != sf::Sound::Playing && m_bButtonBoolPlay)
			{
				m_xButtonSound->setPitch(Utility::RandomFloat(0.7, 1.3));
				m_xButtonSound->play();
				m_bButtonBoolPlay = false;
				
			}


			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				if(m_xSkipLine->getStatus() != sf::Sound::Playing)
				{
					m_xSkipLine->play();
				}
				m_bViewBool = true;
				m_xSkipSprite.setRotation(160);
				
			}

		}   

		else
		{
			m_xPlay.setTexture(m_xButtonTexture);
			m_xPlay.setTextureRect(sf::IntRect(85, 316, 443, 207));
			m_bButtonBoolPlay = true;
		}

		if (m_xHighScore.getGlobalBounds().contains(xMousePosition))
		{


			m_xHighScore.setTexture(m_xButtonTexture);
			m_xHighScore.setTextureRect(sf::IntRect(647, 570, 443, 207));
			if (m_xButtonSound->getStatus() != sf::Sound::Playing && m_bButtonBoolHigh)
			{
				m_xButtonSound->setPitch(Utility::RandomFloat(0.7, 1.3));
				m_xButtonSound->play();
				m_bButtonBoolHigh = false;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{

				iMenu = 1;
				return false;

			}

		}
		else
		{
			m_xHighScore.setTexture(m_xButtonTexture);
			m_xHighScore.setTextureRect(sf::IntRect(85, 570, 443, 207));
			m_bButtonBoolHigh = true;
		}


		if (m_xSettings.getGlobalBounds().contains(xMousePosition))
		{


			m_xSettings.setTexture(m_xButtonTexture);
			m_xSettings.setTextureRect(sf::IntRect(647, 820, 443, 207));
			if (m_xButtonSound->getStatus() != sf::Sound::Playing && m_bButtonBoolSettings)
			{
				m_xButtonSound->setPitch(Utility::RandomFloat(0.7, 1.3));
				m_xButtonSound->play();
				m_bButtonBoolSettings = false;

			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				
				iMenu = 2;
				return false;
			}

		}
		else
		{
			m_xSettings.setTexture(m_xButtonTexture);
			m_xSettings.setTextureRect(sf::IntRect(85, 820, 443, 207));
			m_bButtonBoolSettings = true;
		}

		if (m_xExit.getGlobalBounds().contains(xMousePosition))
		{

			m_xExit.setTexture(m_xButtonTexture);
			m_xExit.setTextureRect(sf::IntRect(647, 1325, 443, 207));
			if (m_xButtonSound->getStatus() != sf::Sound::Playing && m_bButtonBoolExit)
			{
				m_xButtonSound->setPitch(Utility::RandomFloat(0.7, 1.3));
				m_xButtonSound->play();
				m_bButtonBoolExit = false;
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
			iMenu = 100;
			return false;
			}

		}
		else
		{
			m_xExit.setTexture(m_xButtonTexture);
			m_xExit.setTextureRect(sf::IntRect(85, 1325, 443, 207));
			m_bButtonBoolExit = true;
		}

		return true;
	
}

IState* MenuState::NextState()
{
	if (m_bQuit)
	{
		return nullptr;
	}
	if (iMenu == 0)
	{
		return new GameState(m_xSystem);
	}
	else if (iMenu == 1)
	{
		return new HighScoreState(m_xSystem, scoreInt, m_scorestring);
	}
	if (iMenu == 2)
	{
		return new SettingsState(m_xSystem);
	}
	else
	{
		return nullptr;
	}
}
