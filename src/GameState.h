#pragma once
#include "IState.h"
#include "Ship.h"

class IState;
class Player;
class Harpoon;
class Swordfish;
class HealthBar;
class BackgroundElement;
class BackgroundManager;
class Amulet;
class JellyRoger;
class JellyProjectile;
class Coin;
class FishFoodPickup;
class GoldenHarpoon;
class GoldenHarpoonPickup;
class Piranha;
class Bubble;
class AnimatedTexture;
class sf::RenderWindow;
class Ship;
class PowerUpHUD;


class GameState : public IState
{
public:
	GameState(System& p_xSystem);
	~GameState();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();
private:
	sf::Texture m_xHealthTexture;
	sf::Texture m_xMultiplierTexture;
	sf::Texture m_xPlankTexture;
	sf::Texture m_xLungTexture;
	sf::Texture m_xScorePlankTexture;
	sf::Texture m_xCursor;
	sf::Texture m_xTutorialTexture;
	sf::Texture m_xBubbleSkipTexture;
	sf::Texture m_xPowerUpHudBackground;
	sf::Texture m_xHealthTextureBackground;
	sf::Texture m_xGoldHeap;
	float m_iMaxAir;
	float m_iPlayerAir;
	BackgroundManager* m_pxBackgroundManager;
	void CheckCollision(float p_fDeltaTime);
	std::vector <sf::Sprite> m_aXDebugmouse;
	
	std::vector <Coin> m_paxCoins;
	Amulet* m_pxAmulet = nullptr;
	GoldenHarpoon* m_pxGoldenHarpoon = nullptr;
	GoldenHarpoonPickup* m_pxGoldenHarpoonPickup = nullptr;
	System m_xSystem;
	sf::Texture m_pxTexture;
	sf::Texture m_xAssetPlankTexture;
	Player* m_pxPlayer;
	FishFoodPickup* m_pxFishFoodPickup;
	Ship m_xShip;
	ScoreManager* m_pxScoreManager;
	PowerUpHUD* m_pxPowerUpHUD;
	sf::Sprite m_xHealthBar;
	sf::Sprite m_xHealthPlank;
	sf::Sprite m_xMultiplierBar;
	sf::Sprite m_xLungs;
	sf::Sprite m_xScorePlank;
	sf::Sprite m_xCursorSprite;
	sf::Sprite m_xTutorialSprite;
	sf::Sprite m_xBubbleSkip;
	sf::Sprite m_xPowerUpHudBackgroundSprite;
	sf::Sprite m_xPlankSprite;
	sf::Sprite m_xHealthBarOutline;
	sf::Sprite m_xGoldHeapSprite;
	sf::Music* m_xBackgroundMusic;
	sf::Sound* m_xEnemyDamage;
	sf::Sound* m_xEnemyDamage2;
	sf::Sound* m_xRndmSkipLine;
	sf::Sound* m_xRndmSkipLine2;
	sf::Sound* m_xRndmSkipLine3;
	sf::Sound* m_xRndmSkipLine4;
	sf::Sound* m_xRndmSkipLine5;
	sf::Sound* m_xHeartBeat;
	sf::Sound* m_xBigBreath;
	sf::Sound* m_xShipSound;
	sf::Sound* m_xCoinSound;
	sf::Sound* m_xPiranhaBite;
	AnimatedTexture* m_pxHealthBartest;

	int m_iScreenWidth;
	int m_iScreenHeight;
	int m_iStartTime;
	
	sf::Font m_xScoreFont;

	sf::Text m_xScoreText;
	sf::Text m_xMultiPlierText;
	sf::Text m_xImmune; 
	sf::Text m_xLevelTimer;

	
	int healthbarint;
	int m_iScore = 0;
	int m_iScoreFontSize;
	int m_iMultiFontSize = 70;
	float m_fBubbleSize = 1;
	float m_fBubbleOriginX = 0;
	float m_fBubbleOriginY = 0;
	float m_fStateTimer = 0;
	float m_fLevelTimer = 0.0f;
	float m_fCoinTimer = 0;
	float m_fCoinTimerTarget = 5.0f;
	float m_fBubbleTimer = 0;
	float m_fBubbleTimerTarget = 5.0f;
	float m_fTimerTarget = 1;
	float m_fMultiplier;
	float m_fWinTimer = 0;
	float m_fHitTimer = 0;
	float m_fShipTimer = 0;
	int m_iMultiplier;
	bool m_bQuit = false;
	bool m_bMultiplierCheck;
	bool m_bCaveMode = false;
	bool m_bSoundCheck = true;
	bool m_bSpawnCoins = false;
	bool m_bShoot;
	bool m_bSpawnBubbles = false;
	bool m_bTutorial = true;
	bool m_bPause = false;
	bool m_bUnderWater = false;
	bool m_bShipSound = true;

	sf::String m_xScoreString;
	sf::String m_xMultiplierString;
	sf::String m_xLevelTimerString;

	sf::Event m_xEvent;
	float volumetimer = 0;
	float soundtimer = 500;
	float m_fRandomPitch;
	int volume = 10;
	int m_iRndmEnemySound;
	int m_iRndmSkipLine;

};