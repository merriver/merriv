#pragma once

#include "IEntity.h"
#include "PowerUpHUD.h"

class sf::Sprite;
class Harpoon;
class GoldenHarpoon;
class SpriteManager;
class FishFood;
class SoundManager;
class PowerUpHUD;


enum PlayerState
{
	DEFAULT,
	STUNNED,
	AMULET,
	GOLDEN

};

class Player : public IEntity
{
public:
	Player(SpriteManager* p_pxSpriteManager, sf::RenderWindow* p_pxWindow, SoundManager* p_pxSoundManager,PowerUpHUD* p_pxPowerUpHUD, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	~Player();
	void Update(float p_fDeltaTime, sf::View* p_pxView);
	void CheckInput();
	void NormalStateUpdate(float p_fDeltaTime);
	void AmuletStateUpdate(float p_fDeltaTime);
	void StunnedStateUpdate(float p_fDeltaTime);
	void SetState(int p_iNewState);
	void SetHitColorRed();
	void SetHitColorWhite();
	void IsHit(bool);
	void IsBoatHit(bool);
	bool GetHit();
	bool GetBoatHit();
	SpriteManager* m_pxSpriteManager;
	FishFood* GetFishFood();
	sf::Sprite GetSprite();
	sf::Sprite GetRopeSprite();
	sf::FloatRect GetCollitionRect();
	Harpoon* GetHarpoon();


	int GetState();
	float GetX();
	float GetY();
	bool GetCharge();
	bool GetGolden();
	void SetGolden();
	void CameraShake(sf::View* p_pxView, float p_fDeltaTime);
	void GetTakeDamageSound();
	void GetPowerUpSound();
	bool m_bGoldenHarpoon = false;
	
	EENTITYTYPE GetType();
	


private:
	Player() {};
	AnimatedTexture* m_pxAnimatedTexture;
	AnimatedTexture* m_pxAnimatedTextureShooting;
	AnimatedTexture* m_pxAnimatedTextureHarpoon;
	AnimatedTexture* m_pxAnimatedTextureGoldenHarpoon;
	AnimatedTexture* m_pxSkipBubble;
	AnimatedTexture* m_pxAnimatedTextureStunned;
	GoldenHarpoon* m_pxGoldenHarpoon;
	PowerUpHUD* m_pxPowerUpHUD;
	sf::Texture m_xRopeTexture;
	sf::Texture m_xBubbleSkip;
	sf::Sprite m_xBubbleSkipSprite;
	sf::Sprite m_xSprite;
	sf::Sprite m_xRopeSprite;
	sf::RenderWindow* m_pxWindow;
	sf::Sound* m_xShoot;
	sf::Sound* m_xTakeDamage;
	sf::Sound* m_xTakeDamage2;
	sf::Sound* m_xPowerUp;
	sf::Sound* m_xGoldenHarpoonSound;
	sf::Sound* m_xShootSound;
	Harpoon* m_pxHarpoon;
	FishFood* m_pxFishFood;
	sf::FloatRect m_xCollitionRect;
	SoundManager* m_pxSoundManager;
	bools m_pxBools;

	bool m_bIsShooting = false;
	bool m_bShoot = false;
	bool m_bIsCharging;
	bool m_bHit;
	bool m_bBoatHit;
	bool m_bShotShake = false;
	float m_fBubbleTimer = 0;
	float m_fX;
	float m_fY;
	float m_fVelX = 0;
	float m_fVelY = 0;
	float m_fVelMax = 4;
	float m_fStunnedTimer;
	float m_fCurrentCharge;
	float m_fChargeDuration = 5;
	float m_fRandomPitch;
	float m_fShootTimer;
	float m_fCameraTimer;
	int m_iState;
	int m_iScreenWidth;
	int m_iScreenHeight;
	int m_iRandomSound;
	int m_iSkipHit;
	


};