#include "stdafx.h"
#include "Amulet.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Utility.h"
 
 
 Amulet::Amulet(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight)
  {
	 m_pxAnimatedTexture = p_pxSpriteManager->CreateAnimatedTexture("../assets/Amulett.png");
	 m_pxAnimatedTexture->AddFrame(0, 0, 252, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(255, 0, 242, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(503, 0, 238, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(754, 0, 245, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(1006, 0, 249, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(1255, 0, 241, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(1503, 0, 247, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(1755, 0, 246, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(2007, 0, 251, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(2252, 0, 249, 250, 0.05);
	 m_pxAnimatedTexture->AddFrame(2506, 0, 245, 250, 0.05);
	 m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	 m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	 m_pxSprite.setScale(0.6, 0.6);
	 m_fX = p_fX;
	 m_fY = p_fY;
	 m_pxSprite.setPosition(m_fX, m_fY);
	}
 
 Amulet::~Amulet()
  {
  }
 
 void Amulet::Update(float p_fDeltaTime)
  {
	 m_pxAnimatedTexture->Update(p_fDeltaTime);
	 m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	 m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());

	 m_pxSprite.setPosition(m_fX += -5, m_fY += 1);
}
 
 sf::Sprite Amulet::GetSprite()
  {
	 return m_pxSprite;
 }
 

 
 float Amulet::GetX()
  {
	 return m_fX;
	 }
 
 float Amulet::GetY()
  {
	 return m_fY;
	 }
 
 EENTITYTYPE Amulet::GetType()
 {
	 return ENTITY_POWERUP;
 }
