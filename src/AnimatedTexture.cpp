#include "stdafx.h"
#include "AnimatedTexture.h"


//m_pxMyTempAnimatedTexture = m_xSystem.m_pxSpriteManager->CreateAnimatedTexture("filepath"); <-get spritesheet
//m_pxMyTempAnimatedTexture->AddFrame(0, 0, 40, 16, 0.1f); <- first frame, do same for rest of frames

AnimatedTexture::AnimatedTexture(sf::Texture p_xTexture)
{
	m_iIndex = 0;
	m_fCurrentDuration = 0.0f;
	m_xTexture = p_xTexture;
}

void AnimatedTexture::AddFrame(int p_iX, int p_iY, int p_iWidth, int p_iHeight, float p_fDuration)
{
	FrameData data;
	data.m_xRegion.left= p_iX;
	data.m_xRegion.top = p_iY;
	data.m_xRegion.width = p_iWidth;
	data.m_xRegion.height = p_iHeight;
	data.m_fDuration = p_fDuration;
	m_axFrames.push_back(data);
	m_xRegion = data.m_xRegion;
}


void AnimatedTexture::Update(float p_fDeltaTime)
{
	m_fCurrentDuration += p_fDeltaTime;
	if (m_fCurrentDuration >= m_axFrames.at(m_iIndex).m_fDuration)
	{
		if (m_iIndex < m_axFrames.size()-1)
		{
			m_iIndex++;
		}
		else
		{
			m_iIndex = 0;
		}
		m_xRegion = m_axFrames[m_iIndex].m_xRegion;
		m_fCurrentDuration = 0.0f;
	}
}

void AnimatedTexture::Reset()
{
	m_fCurrentDuration = 0.0f;
	m_iIndex = 0;
}

sf::Texture& AnimatedTexture::GetTexture()
{
	return m_xTexture;
}

sf::IntRect AnimatedTexture::GetRegion()
{
	return m_xRegion;
}