#pragma once
#include "IEntity.h"


class sf::Sprite;
class Player;
class SpriteManager;




class JellyRoger : public IEntity
{
public:
	JellyRoger();
	~JellyRoger();
	bool Initialize(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight);
	void Update(float p_fDeltaTime);
	void DyingStateUpdate(float p_fDeltaTime);
	sf::Sprite GetSprite();
	AnimatedTexture* GetTexture();
	float GetX();
	float GetY();
	bool IsActive();
	void Deactivate();
	void Kill();
	void Activate();
	EENTITYTYPE GetType();
	bool IsDying();
private:
	
	AnimatedTexture* m_pxAnimatedTextureDefault;
	AnimatedTexture* m_pxAnimatedTextureDying;
	sf::Sprite m_pxSprite;
	float m_fSpeed;
	float m_fDirY;
	float m_fDirX;
	float m_fX;
	float m_fY;
	float DyingTimer = 0.0;
	float DyingTimerTarget = 0.8;
	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bActive = false;
	bool m_bDying = false;

};