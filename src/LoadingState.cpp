#include "stdafx.h"
#include "MenuState.h"
#include "LoadingState.h"
#include "DrawManager.h"
#include "SpriteManager.h"



LoadingState::LoadingState(System& p_xSystem)
{
	m_xSystem = p_xSystem;
	m_xLogoTexture.loadFromFile("../assets/Logga.png");
	m_xLogoSprite.setTexture(m_xLogoTexture);
	m_fLogoSize = 0.005;
	m_xLogoSprite.setOrigin(743, 600);
	m_xLogoSprite.setPosition((width / 2), (height / 2));

	font.loadFromFile("../assets/OldLondon.ttf");

	m_xLoadingText.setFont(font);
	m_xLoadingText.setPosition(1600, 900);
	m_xLoadingText.setCharacterSize(70);
	m_xLoadingText.setColor(sf::Color::White);
	
}

LoadingState::~LoadingState()
{
	
}

void LoadingState::Draw()
{
	m_xSystem.m_pxDrawManager->Draw(m_xLogoSprite);
	m_xSystem.m_pxDrawManager->DrawText(m_xLoadingText);
}

bool LoadingState::Update(float p_fDeltaTime)
{
	if (m_fLogoSize >= 0.5)
	{
		m_xLoadingText.setString("Loading...");
	}

	m_xLogoSprite.setScale(m_fLogoSize, m_fLogoSize);
	if (m_fLogoSize <= 0.8)
	{
		//m_fLoadingTimer += 0.5;
		m_fLogoSize += 0.01;
	}
	else
	{
		m_xSystem.m_pxSpriteManager->LoadTextures();
		return false;
	}
	return true;
}

IState * LoadingState::NextState()
{
	return new MenuState(m_xSystem);
}


