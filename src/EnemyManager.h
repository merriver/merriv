#pragma once

class Swordfish;
class JellyRoger;
class JellyProjectile;
class Piranha;
class Player;
class DrawManager;
class SpriteManager;
class SoundManager;

struct Enemies
{
	std::vector<Swordfish> m_axSwordfish;
	std::vector<JellyRoger> m_axJellyRoger;
	std::vector<JellyProjectile> m_axJellyProjectile;
	std::vector<Piranha> m_axPiranha;
};

class EnemyManager
{
public:
	EnemyManager(SpriteManager* p_pxSpriteManager, SoundManager* p_pxSoundManager);
	~EnemyManager();
	void Update(float p_fDeltaTime, Player* p_pxPlayer);
	void Draw(DrawManager* p_pxDrawManager);
	Enemies GetEnemies();
	bool CheckSwordfishCollision(sf::FloatRect p_xRect);
	bool CheckJellyRogerCollision(sf::FloatRect p_xRect);
	bool CheckJellyProjectileCollision(sf::FloatRect p_xRect);
	bool CheckPiranhaCollision(sf::FloatRect p_xRect);
	void Deactivate();
	sf::FloatRect GetDropRect();
private:
	Enemies m_xEnemies;
	
	int m_iSpawnRate = 299;
	float m_fSpawnTimer = 0.0f;
	float m_fJollyProjectileTimer = 0.0f;
	float m_fTimerTarget = 20.0f;

	bool m_bJollyProjectileBool = false;
	bool m_bSearch;
	sf::FloatRect m_xDropRect;
};