#pragma once
#include "IEntity.h"
#include "stdafx.h"


class GoldenHarpoonPickup;
class FishFood;
class Amulet;
class DrawManager;
class SpriteManager;
class sf::Sprite;
enum Powerups
{
	HUD_AMULET,
	HUD_HARPOON,
	HUD_FISHFOOD
};

struct bools
{

	bool m_bGoldenHarpoon = false;
	bool m_bAmulet = false;
	bool m_bFishFood = false;
	bool m_bGoldenHarpoonSelected = false;
	bool m_bAmuletSelected = true;
	bool m_bFishFoodSelected = false;
};

class PowerUpHUD : public IEntity
{
public:
	PowerUpHUD(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight);
	~PowerUpHUD();
	void MoveToNext();

	bool Update(float p_fDeltaTime);
	int GetPressedItem() { return selectedItemIndex; }
	
	
	bools GetBools();


	sf::Sprite GetSprite();
	sf::Sprite GetHarpoonSprite();
	sf::Sprite GetFishFoodSprite();
	sf::Sprite GetPilSprite();

	int selectedItemIndex;
	void AddPowerup(int p_iPowerup);
	void RemovePowerup(int p_iPowerup);
	float GetX();
	float GetY();
	EENTITYTYPE GetType();
	
private:
	
	SpriteManager* m_pxSpriteManager;
	sf::Texture m_xTextureGoldenHarpoon;
	sf::Texture m_xTextureFishFood;
	sf::Texture m_xTextureAmulet;
	sf::Texture m_xTexturePil;
	sf::Sprite m_xSprite;
	sf::RenderWindow* p_pxWindow;
	sf::Vector2i xMousePosition;
	sf::Event m_xEvent;
	sf::Texture m_xMenuTexture;
	sf::Texture m_xButtonTexture;
	sf::Texture m_xCursor;
	sf::Sprite m_xCursorSprite;
	sf::Sprite m_xAmulet;
	sf::Sprite m_xGoldenHarpoon;
	sf::Sprite m_xFishFood;
	sf::Sprite m_xPil;
	sf::RenderWindow* m_pxWindow;
	bools m_xBools;
	

	float m_fX;
	float m_fY;
	int PosX;
	int PosY;
	int m_iScreenWidth;
	int m_iScreenHeight;

};
