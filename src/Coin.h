#pragma once


#include "IEntity.h"

class sf::Sprite;
class SpriteManager;
class Collider;

class Coin : public IEntity
{
public:
	Coin();
	~Coin();
	bool Initialize(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	void Update(float p_fDeltaTime);
	sf::Sprite GetSprite();
	EENTITYTYPE GetType();
	float GetX();
	float GetY();
	bool IsActive();
	void Deactivate();
	void Activate();

private:
	AnimatedTexture* m_pxAnimatedTexture;
	sf::Sprite m_xSprite;
	float m_fX;
	float m_fY;
	float PosX;
	float PosY;
	bool m_bActive = false;
};