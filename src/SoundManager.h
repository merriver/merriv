#pragma once

class sf::Sound;
class sf::Music;

class SoundManager
{
public:
	SoundManager();
	~SoundManager();

	bool Initialize();
	void Shutdown();
	

	sf::Sound* CreateSound(const std::string& p_sFilepath);

	sf::Music* CreateMusic(const std::string& p_sFilepath);

private:
	std::map<std::string, sf::Sound*> m_apxSounds;
	std::map<std::string, sf::SoundBuffer> m_apxAudio;
	std::map<std::string, sf::Music*> m_apxMusic;
};