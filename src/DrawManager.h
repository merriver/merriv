#pragma once

class sf::Sprite;
class sf::RenderWindow;

class DrawManager
{
public:
	DrawManager();
	~DrawManager();


	bool Initialize(int p_iWidth, int p_iHeight);
	void Shutdown();
	void Clear();
	void Present();
	void Draw(sf::Sprite p_pxSprite);
	void DrawText(sf::Text p_pxText);
	void DrawText(sf::Text p_pxText, sf::BlendMode p_xBlend);
	sf::View* GetView();
	void SetView();

	sf::RenderWindow* GetRenderWindow();
private:
	sf::View* m_pxview;
	sf::RenderWindow* m_pxWindow;
	
};
