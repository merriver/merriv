#include "stdafx.h"
#include "Engine.h"
#include "DrawManager.h"
#include "SpriteManager.h"
#include "StateManager.h"
#include "Sprite.h"
#include "ScoreManager.h"
#include "GameState.h"
#include "IState.h"
#include "MenuState.h"
#include <iostream>
#include "HighScoreState.h"
#include "SoundManager.h"
#include "LoseState.h"
#include <ctime>
#include "EnemyManager.h"
#include "WinState.h"
#include "LoadingState.h"

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#pragma comment(lib, "sfml-audio.lib")
#endif

const int SCREENWIDTH = 1920;
const int SCREEHEIGHT = 1080;

Engine::Engine()
{
	m_bRunning = false;
	m_pxDrawManager = nullptr;
	m_pxSpriteManager = nullptr;
	m_pxStateManager = nullptr;
}

Engine::~Engine()
{

}


bool Engine::Initialize()
{
	// The initialize function will intialize libraries the program depends on and all manager we will create.

	srand((unsigned int)time(0)); //Initializes a random seed for when we will be using rand() in the future


	// Creates a new DrawManager and calls Initialize with width / height parameters.
	m_pxDrawManager = new DrawManager();
	if (m_pxDrawManager->Initialize(SCREENWIDTH, SCREEHEIGHT) == false)
	{
		return false;
	}


	m_pxSpriteManager = new SpriteManager();

	//m_pxSpriteManager->LoadTextures();

	m_pxStateManager = new StateManager();

	m_pxSoundManager = new SoundManager();
	m_pxEnemyManager = new EnemyManager(m_pxSpriteManager, m_pxSoundManager);

	m_pxScoreManager = new ScoreManager();

	
	System system;
	system.m_iScreenWidth = SCREENWIDTH;
	system.m_iScreenHeight = SCREEHEIGHT;
	system.m_pxDrawManager = m_pxDrawManager;
	system.m_pxSpriteManager = m_pxSpriteManager;
	system.m_pxSoundManager = m_pxSoundManager;
	system.m_pxScoreManager = m_pxScoreManager;

	system.m_pxEnemyManager = m_pxEnemyManager;

	m_pxStateManager->SetState(new WinState(system));

	m_bRunning = true;


	return true;
}

void Engine::Shutdown()
{
	// The shutdown function will quit, delete and shutdown everything we have started up or created in initialize (In reverse order of creation)
	delete m_pxScoreManager;
	m_pxScoreManager = nullptr;

	delete m_pxEnemyManager;
	m_pxEnemyManager = nullptr;

	m_pxSoundManager->Shutdown();
	delete m_pxSoundManager;
	m_pxSoundManager = nullptr;

	delete m_pxStateManager;
	m_pxStateManager = nullptr;

	delete m_pxSpriteManager;
	m_pxSpriteManager = nullptr;




	// Shuts down the drawmanager before deleting the object and nulling the pointer.
	m_pxDrawManager->Shutdown();
	delete m_pxDrawManager;
	m_pxDrawManager = nullptr;

	
	
}

void Engine::Update()
{
	// Our engines core loop
	while (m_bRunning)
	{
		m_pxDrawManager->Clear();
		if (m_pxStateManager->Update() == false)
		{
			m_bRunning = false;
		}
		m_pxStateManager->Draw();
		m_pxDrawManager->Present();
	}

}

