#include "stdafx.h"
#include "BackgroundElement.h"
#include "Utility.h"
#include "SpriteManager.h"

//TODO: Make Manager

BackgroundElement::BackgroundElement()
{

}

BackgroundElement::~BackgroundElement()
{

}

void BackgroundElement::Initialize(sf::Texture p_xTexture, sf::IntRect p_xTextureRect, float p_fX, float p_fY, int p_iDepth, int p_iScreenWidth, int p_iscreenHeight, bool p_bScale, bool p_bIsBackground)
{
	m_xTexture = p_xTexture;
	if (p_bIsBackground)
	{
		m_xTexture.setSmooth(true);
	}
	m_xSprite.setTexture(m_xTexture);
	m_xSprite.setTextureRect(p_xTextureRect);
	if (!p_bScale)
	{
		m_xSprite.setOrigin(m_xSprite.getGlobalBounds().width / 2, m_xSprite.getGlobalBounds().height);
		if (p_iDepth != 2)
		{
			//float fScale = Utility::RandomFloat(0.5, 0.7);
			//m_xSprite.setScale(fScale, fScale);
		}
	}
	if (p_bIsBackground)
	{
		m_xSprite.setScale(4, 4);
	}
	m_fSpeed = 5 / p_iDepth;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iDepth = p_iDepth;
	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iscreenHeight;

	m_xSprite.setPosition(m_fX, m_fY);
}

void BackgroundElement::Update(float p_fDeltaTime)
{
	m_xSprite.setPosition(m_fX += -m_fSpeed, m_fY);
}

sf::Sprite BackgroundElement::GetSprite() { return m_xSprite; }

float BackgroundElement::GetX() { return m_fX; }

float BackgroundElement::GetY() { return m_fY; }

int BackgroundElement::GetDepth() { return m_iDepth; }

bool BackgroundElement::IsActive()
{

	return m_bActive;
}

void BackgroundElement::Deactivate()
{
	m_bActive = false;
	m_fX = 1920;
}

void BackgroundElement::Activate(float p_fX, float p_fY)
{
	m_bActive = true;
	m_fX = p_fX;
	m_fY = p_fY;
}