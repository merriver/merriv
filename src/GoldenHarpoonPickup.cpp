#include "stdafx.h"
#include "GoldenHarpoonPickup.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"

#include "Utility.h"


GoldenHarpoonPickup::GoldenHarpoonPickup(SpriteManager * p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight)
{
	m_pxAnimatedTexture = p_pxSpriteManager->CreateAnimatedTexture("../assets/GoldenHarpoon.png");
	m_pxAnimatedTexture->AddFrame(0, 0, 328, 150, 0.05);
	m_pxAnimatedTexture->AddFrame(328, 0, 328, 150, 0.05);
	m_pxAnimatedTexture->AddFrame(658, 0, 324, 150, 0.05);
	m_pxAnimatedTexture->AddFrame(991, 0, 334, 150, 0.05);
	m_pxAnimatedTexture->AddFrame(1321, 0, 332, 150, 0.05);
	m_pxAnimatedTexture->AddFrame(1653, 0, 329, 150, 0.05);
	m_pxAnimatedTexture->AddFrame(1986, 0, 332, 150, 0.05);
	m_pxAnimatedTexture->AddFrame(2318, 0, 334, 150, 0.05);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setColor(sf::Color::Yellow);
	m_fX = p_fX;
	m_fY = p_fY;
	m_pxSprite.setPosition(m_fX, m_fY);

}
GoldenHarpoonPickup::~GoldenHarpoonPickup()
{
}

void GoldenHarpoonPickup::Update(float p_fDeltaTime)
{
	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());

	m_pxSprite.setPosition(m_fX += -5, m_fY);
}

sf::Sprite GoldenHarpoonPickup::GetSprite()
{
	return m_pxSprite;
}

Collider * GoldenHarpoonPickup::GetCollider()
{
	return m_pxCollider;
}

EENTITYTYPE GoldenHarpoonPickup::GetType()
{
	return EENTITYTYPE::ENTITY_POWERUP2;
}

float GoldenHarpoonPickup::GetX()
{
	return m_fX;
}

float GoldenHarpoonPickup::GetY()
{
	return m_fY;
}
