#pragma once

class SpriteManager;
class DrawManager;
class SoundManager;
class ScoreManager;
class EnemyManager;


// A struct that we create in the Engine so we can send important pointers
// to potential future states for them to use.
struct System
{
	int m_iScreenWidth;
	int m_iScreenHeight;
	SpriteManager* m_pxSpriteManager;
	DrawManager* m_pxDrawManager;
	SoundManager* m_pxSoundManager;
	ScoreManager* m_pxScoreManager;
	EnemyManager* m_pxEnemyManager;
};

// Interface class, incomplete class with pure virtual functions that need to be
// both declared and defines in derived classes. Only virtual will give the option
// to declare and define the function while pure virtual forces it.
class IState
{
public:
	virtual ~IState() {};
	virtual bool Update(float p_fDeltaTime) = 0;
	virtual void Draw() = 0;
	virtual IState* NextState() = 0;
};