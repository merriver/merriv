#include "stdafx.h"
#include "GameState.h"
#include "Player.h"
#include "DrawManager.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Harpoon.h"
#include "Swordfish.h"
#include "JellyRoger.h"
#include "JellyProjectile.h"
#include "Utility.h"
#include "BackgroundElement.h"
#include "Amulet.h"
#include "MenuState.h"
#include "SoundManager.h"
#include "LoseState.h"
#include "BackgroundManager.h"
#include "Coin.h"
#include "GoldenHarpoonPickup.h"
#include "FishFoodPickup.h"
#include "FishFood.h"
#include "WinState.h"
#include "Piranha.h"
#include "Ship.h"
#include "Bubble.h"
#include "EnemyManager.h"
#include "PowerUpHUD.h"
#include "ScoreManager.h"


GameState::GameState(System& p_xSystem)
{
	m_xSystem = p_xSystem;

	m_xSystem.m_pxDrawManager->GetView()->setCenter(960, 540);
	m_bQuit = false;
	m_bShipSound = true;
	m_pxBackgroundManager = new BackgroundManager(m_xSystem.m_pxSpriteManager, m_xSystem.m_iScreenHeight, m_xSystem.m_iScreenWidth);
	m_pxPowerUpHUD = new PowerUpHUD(m_xSystem.m_pxSpriteManager, 200, 200, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);

	m_pxPlayer = new Player(m_xSystem.m_pxSpriteManager,m_xSystem.m_pxDrawManager->GetRenderWindow(), m_xSystem.m_pxSoundManager, m_pxPowerUpHUD, 200, 200, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);


	m_fCoinTimerTarget = 1;
	m_fLevelTimer = 0;

	m_fBubbleTimerTarget = 1;

	m_xBackgroundMusic = m_xSystem.m_pxSoundManager->CreateMusic("../assets/BackgroundSong2.wav");

	m_xEnemyDamage = m_xSystem.m_pxSoundManager->CreateSound("../assets/Enemytakedamage3.wav");
	m_xEnemyDamage2 = m_xSystem.m_pxSoundManager->CreateSound("../assets/Enemytakedamage3.wav");
	m_xRndmSkipLine = m_xSystem.m_pxSoundManager->CreateSound("../assets/RandomSkipLine1.wav");
	m_xRndmSkipLine2 = m_xSystem.m_pxSoundManager->CreateSound("../assets/RandomSkipLine2.wav");
	m_xRndmSkipLine3 = m_xSystem.m_pxSoundManager->CreateSound("../assets/RandomSkipLine3.wav");
	m_xRndmSkipLine4 = m_xSystem.m_pxSoundManager->CreateSound("../assets/RandomSkipLine4.wav");
	m_xRndmSkipLine5 = m_xSystem.m_pxSoundManager->CreateSound("../assets/RandomSkipLine5.wav");
	m_xBigBreath = m_xSystem.m_pxSoundManager->CreateSound("../assets/TakeBigBreath.wav");
	m_xHeartBeat = m_xSystem.m_pxSoundManager->CreateSound("../assets/Heartbeat.wav");
	m_xShipSound = m_xSystem.m_pxSoundManager->CreateSound("../assets/SquatLegday.wav");
	m_xCoinSound = m_xSystem.m_pxSoundManager->CreateSound("../assets/Coins.wav");
	m_xPiranhaBite = m_xSystem.m_pxSoundManager->CreateSound("../assets/PiranhaBite.wav");
	m_xCursor = m_xSystem.m_pxSpriteManager->GetTexture("../assets/muspekare.png");
	

	m_xCursorSprite.setTexture(m_xCursor);
	m_xCursorSprite.setTextureRect(sf::IntRect(0, 0, 60, 60));
	m_xCursorSprite.setOrigin(30, 30);

	m_xBubbleSkipTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/Bubblaforskip.png");
	/*m_pxPowerUpHUD = m_pxPowerUpHUD()*/

	m_xPowerUpHudBackground.loadFromFile("../assets/HudTry.png");
												
	m_xPowerUpHudBackgroundSprite.setTexture(m_xPowerUpHudBackground);// f�r power up HUD
	m_xPowerUpHudBackgroundSprite.setColor(sf::Color(255, 255, 255, 128));
	m_xPowerUpHudBackgroundSprite.setPosition(150, 590);
	m_xPowerUpHudBackgroundSprite.setRotation(90);
	m_xPowerUpHudBackgroundSprite.setScale(1, 1.1);

	m_xLungTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/Lungor.png");
	m_xPlankTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/HealthbarPlank.png");

	m_xScorePlankTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/ScorePlank.png");

	m_xAssetPlankTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/Asset_Plank2.png");

	m_xScorePlank.setTexture(m_xScorePlankTexture);
	m_xScorePlank.setPosition(-15, 1080); // HAR FLYTTAS TILL SIDAN
	m_xScorePlank.rotate(270);
	m_xScorePlank.setScale(0.7, 1);


	m_xPlankSprite.setTexture(m_xAssetPlankTexture);
	m_xPlankSprite.setScale(0.7f, 1.0f);
	m_xPlankSprite.setPosition(-2, 0);

	// Luftm�tare Git correct                    Har FLYTTAS TILL SIDAN
	m_xHealthPlank.setTexture(m_xPlankTexture);
	m_xHealthPlank.setPosition(170,0);
	m_xHealthPlank.setScale(1, 1.25);
	m_xHealthPlank.rotate(90);

	m_xLungs.setTexture(m_xLungTexture);
	m_xLungs.setOrigin(41, 27);
	m_xLungs.setPosition(80, 460);
	/*m_xLungs.rotate(90);*/

	m_xBubbleSkip.setTexture(m_xBubbleSkipTexture);
	m_xBubbleSkip.setPosition(m_pxPlayer->GetX(), m_pxPlayer->GetY());


	


	m_xBackgroundMusic->play();
	m_xBackgroundMusic->setLoop(true);
	m_xBackgroundMusic->setPitch(1.2);
	m_xBackgroundMusic->setVolume(10);

	m_iMaxAir = 300;
	m_iPlayerAir = 300;


	sf::IntRect m_xHealthRect(0, 0, m_iPlayerAir, 60);
	sf::IntRect m_xMultiplierRect(0, 0, m_fMultiplier, 40);


	m_xHealthTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/HealthbarLong.png");
	m_xHealthTextureBackground = m_xSystem.m_pxSpriteManager->GetTexture("../assets/HealthbarlongBackground.png");

	m_xHealthBarOutline.setTexture(m_xHealthTextureBackground);
	m_xHealthBarOutline.setPosition(35, 460); 
	m_xHealthBarOutline.scale(1.3f, 1.3f);
	m_xHealthBarOutline.rotate(270);
	
	


	m_xHealthBar.setTexture(m_xHealthTexture);
	m_xHealthBar.setTextureRect(m_xHealthRect);
	
	m_xHealthBar.setPosition(35, 460); 
	m_xHealthBar.scale(1.3f, 1.3f);
	m_xHealthBar.rotate(270);



	//Multiplier

	if (!m_xScoreFont.loadFromFile("../assets/OldLondon.ttf"))
	{
		//error
	}


	m_fMultiplier = 0;
	m_iMultiplier = 1;

	m_xMultiplierString = std::to_string(m_iMultiplier);
	m_xMultiPlierText.setFont(m_xScoreFont);
	m_xMultiPlierText.setColor(sf::Color(242, 200, 154, 255));
	m_xMultiPlierText.setString(m_xMultiplierString);
	m_xMultiPlierText.setPosition(80,927); //                    HAR FLYtTA NER
	m_xMultiPlierText.setCharacterSize(m_iMultiFontSize);


	m_xMultiplierTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/MultiplierbarLong.png");


	m_xMultiplierBar.setTexture(m_xMultiplierTexture);
	m_xMultiplierBar.setTextureRect(m_xMultiplierRect);
	//m_xMultiplierBar.setScale(m_fMultiplier, 1);
	m_xMultiplierBar.setPosition(15, 900); // HAR FLYTTA NER
	m_xMultiplierBar.setScale(0.8, 1);
	


	//Level Timer Debug
	m_xLevelTimerString = std::to_string(m_iScore);
	m_xLevelTimer.setFont(m_xScoreFont);
	m_xLevelTimer.setColor(sf::Color::Red);
	m_xLevelTimer.setString(m_xScoreString);
	m_xLevelTimer.setPosition(0, 0); // FLYTA NER?
	m_xLevelTimer.setCharacterSize(50);


	//Score  FLYTTA NER

	m_iScoreFontSize = 40;
	m_iScore = 0;
	m_xScoreString = std::to_string(m_iScore);
	m_xScoreText.setFont(m_xScoreFont);
	m_xScoreText.setColor(sf::Color(242, 200, 154, 255));
	m_xScoreText.setString(m_xScoreString);
	m_xScoreText.setPosition(20, 850);
	m_xScoreText.setCharacterSize(m_iScoreFontSize);

	//(


	//Amulett text

	m_xImmune.setFont(m_xScoreFont);
	m_xImmune.setColor(sf::Color::White);
	m_xImmune.setString(m_xMultiplierString);
	m_xImmune.setPosition(m_pxPlayer->GetX(), m_pxPlayer->GetY() + 100);
	m_xImmune.setCharacterSize(m_iScoreFontSize);
	m_xImmune.setString("Immune");

	m_xTutorialTexture = m_xSystem.m_pxSpriteManager->GetTexture("../assets/TutorialScreen.png");
	m_xTutorialSprite.setTexture(m_xTutorialTexture);
	m_xTutorialSprite.setScale(0.75, 0.75);
	m_xTutorialSprite.setOrigin(m_xTutorialSprite.getGlobalBounds().width / 2, m_xTutorialSprite.getGlobalBounds().height / 2);
	m_xTutorialSprite.setPosition((m_xSystem.m_iScreenWidth / 2) - 200, m_xSystem.m_iScreenHeight / 2);
	m_paxCoins.resize(20);
	{
		auto it = m_paxCoins.begin();
		while (it != m_paxCoins.end())
		{
			(it)->Initialize(m_xSystem.m_pxSpriteManager, 0, 880, 1920, 1080);
			it++;
		}
	}
	m_xShip.Initialize();
	m_xShip.Deactivate();
	m_xGoldHeap = m_xSystem.m_pxSpriteManager->GetTexture("../assets/VictoryGoldCrew.png");
	m_xGoldHeapSprite.setTexture(m_xGoldHeap);
	m_xGoldHeapSprite.setScale(0.4, 0.4);
	m_xGoldHeapSprite.setPosition(1920, 700);
}

GameState::~GameState()
{
	if (m_pxGoldenHarpoonPickup != nullptr)
	{
		delete m_pxGoldenHarpoonPickup;
		m_pxGoldenHarpoonPickup = nullptr;
	}
	if (m_pxFishFoodPickup != nullptr)
	{
		delete m_pxFishFoodPickup;
		m_pxFishFoodPickup = nullptr;
	}
	if (m_pxAmulet != nullptr)
	{
		delete m_pxAmulet;
		m_pxAmulet = nullptr;
	}

	m_paxCoins.clear();

	delete m_pxPlayer;
	m_pxPlayer = nullptr;

	delete m_pxBackgroundManager;
	m_pxBackgroundManager = nullptr;

	delete m_pxPowerUpHUD;
	m_pxPowerUpHUD = nullptr;
}


bool GameState::Update(float p_fDeltaTime)
{ 
	sf::Vector2f xMousePosition = static_cast<sf::Vector2f>(sf::Mouse::getPosition(*m_xSystem.m_pxDrawManager->GetRenderWindow()));
	m_xCursorSprite.setPosition(xMousePosition);
	m_xCursorSprite.setTexture(m_xCursor);
	m_xCursorSprite.setOrigin(m_xCursorSprite.getGlobalBounds().width / 2, m_xCursorSprite.getGlobalBounds().height / 2);
	m_fWinTimer += p_fDeltaTime;
	
	m_xSystem.m_pxScoreManager->SetHighScore(m_iScore);

	// Update power up HUD
	m_pxPowerUpHUD->Update(p_fDeltaTime);
	//Skip bubble

	m_xBubbleSkip.setPosition(m_pxPlayer->GetX() - 50, m_pxPlayer->GetY() - 15);


	if (m_pxPlayer->GetState() == AMULET)
	{
		m_fStateTimer += p_fDeltaTime;
		if (m_fBubbleSize > 0);
		{
			m_fBubbleSize -= 0.002;
			m_fBubbleOriginX -= 1.1;
			m_fBubbleOriginY -= 0.35;
		}
	}
	else
	{
		m_fStateTimer = 0;
	}

	if (m_fStateTimer >= 5)
	{
		m_pxPlayer->SetState(DEFAULT);
		m_fStateTimer = 0;
		m_fBubbleSize = 1;
		m_fBubbleOriginX = 0;
		m_fBubbleOriginY = 0;
	}
	m_xBubbleSkip.setOrigin(m_fBubbleOriginX, m_fBubbleOriginY);
	m_xBubbleSkip.setScale(m_fBubbleSize, m_fBubbleSize);

	
	
	
	while (m_xSystem.m_pxDrawManager->GetRenderWindow()->pollEvent(m_xEvent))
	{
		if (m_xEvent.type == sf::Event::Closed)
		{
			m_bQuit = true;
			return false;
		}
		if (m_xEvent.type == sf::Event::KeyPressed/* || m_xEvent.type == sf::Event::MouseButtonPressed*/)
		{
			if (m_xEvent.key.code == sf::Keyboard::G)
			{
				m_bCaveMode = !m_bCaveMode;
			}
			if (m_xEvent.key.code == sf::Keyboard::W || m_xEvent.key.code == sf::Keyboard::A || m_xEvent.key.code == sf::Keyboard::S || m_xEvent.key.code == sf::Keyboard::D || m_xEvent.key.code == sf::Keyboard::Space)
			{
				m_bTutorial = false;
				m_pxBackgroundManager->StartMoving();

			}
			if (m_xEvent.key.code == sf::Keyboard::Escape || m_xEvent.key.code == sf::Keyboard::P)
			{
				m_bPause = !m_bPause;

			}
		}
		if (m_xEvent.type == sf::Event::KeyPressed)
		{
			if (m_xEvent.key.code == sf::Keyboard::Space)
			{
				m_pxPowerUpHUD->MoveToNext();
			}
		}
	}
	if (m_bPause)
	{
		return true;
	}


	m_pxPlayer->Update(p_fDeltaTime, m_xSystem.m_pxDrawManager->GetView());

	m_fLevelTimer += p_fDeltaTime;
	if (m_bTutorial)
	{
	 
	}

	//If the game isn't in tutorial state do game stuff
	if (!m_bTutorial)
	{

		m_iStartTime = m_fLevelTimer;
		if (m_fLevelTimer >= 150)
		{
			m_pxBackgroundManager->StopMoving();


			if (m_fLevelTimer >= 155)
			{
				return false;

			}
		}
		m_xLevelTimerString = std::to_string(m_fLevelTimer);
		m_xLevelTimer.setString(m_xLevelTimerString);
		
		if (m_fLevelTimer >= 145)
		{
			m_xGoldHeapSprite.setPosition(m_xGoldHeapSprite.getGlobalBounds().left - 5, m_xGoldHeapSprite.getGlobalBounds().top);

		}
		
		
		//update Timers 
		
		m_fCoinTimer += p_fDeltaTime;
	m_fBubbleTimer += p_fDeltaTime;

		//Amulett text
		m_xImmune.setPosition(m_pxPlayer->GetX(), m_pxPlayer->GetY() + 100);

		//Score

		m_xScoreString = std::to_string(m_iScore);
		m_xScoreText.setString(m_xScoreString);


		//Multiplier
		m_xMultiplierString = std::to_string(m_iMultiplier);
		m_xMultiPlierText.setString(m_xMultiplierString);

		if (p_fDeltaTime * 1000 >= 1 && m_bMultiplierCheck == true)
		{
			m_fMultiplier += 0.5;
		}
		else if (p_fDeltaTime * 1000 >= 1 && m_bMultiplierCheck == false)
		{
			m_fMultiplier = 0;
			m_iMultiplier = 1;
			m_bMultiplierCheck = true;
		}

		if (m_fMultiplier >= 140)
		{
			m_iMultiplier++;
			m_fMultiplier = 0;
		}

		if (m_bSoundCheck == false)
		{
			soundtimer += 0.1;
			if (soundtimer >= 300)
			{
				m_bSoundCheck = true;
			}
		}

		sf::IntRect m_xMultiplierRect(0, 0, m_fMultiplier, 40);
		m_xMultiplierBar.setTextureRect(m_xMultiplierRect);

		if (m_bSoundCheck == true && soundtimer >= 300)
		{
			m_iRndmSkipLine = Utility::RandomFloat(0, 80);
			if (m_iRndmSkipLine <= 20)
			{
				m_xRndmSkipLine->play();
			}
			else if (m_iRndmSkipLine > 20 && m_iRndmSkipLine < 40)
			{
				m_xRndmSkipLine->play();
			}
			else if (m_iRndmSkipLine > 40 && m_iRndmSkipLine < 60)
			{
				m_xRndmSkipLine3->play();
			}
			else if (m_iRndmSkipLine > 60 && m_iRndmSkipLine < 80)
			{
				m_xRndmSkipLine4->play();
			}
			else if (m_iRndmSkipLine > 80 && m_iRndmSkipLine < 100)
			{
				m_xRndmSkipLine5->play();
			}

			m_bSoundCheck = false;
			soundtimer = 0;
		}

		//Ship
		if (m_fShipTimer >= 8.5 && m_xShipSound->getStatus() != sf::Sound::Playing && m_bShipSound)
		{
		m_xShipSound->play();
		m_bShipSound = false;
		}
		if (m_fShipTimer >= 10 && !m_xShip.IsActive())
		{
			m_xShip.Activate();
	
		}
		if (m_xShip.IsActive())
		{
			m_xShip.Update(p_fDeltaTime);
		}
		if (m_xShip.IsActive() && m_xShip.GetX() >= m_xSystem.m_iScreenWidth)
		{
			m_xShip.Deactivate();
			m_fShipTimer = 0;
			m_bShipSound = true;
		}

		//Luftm�tare
		if (m_iPlayerAir <= 0)
		{
			return false;
		}
	/*	if (p_fDeltaTime * 1000 >= 1 && m_pxPlayer->GetY() >= 50 && m_pxPlayer->GetCharge())
		{
			m_iPlayerAir -= 0.75;
		}*/
		if ((p_fDeltaTime * 1000 >= 1 && m_pxPlayer->GetY() >= 50 && !m_bCaveMode) && m_pxPlayer->GetState() != AMULET)
		{
			m_iPlayerAir -= 0.25;
			m_bUnderWater = true;
			m_fShipTimer = 0.0f;
		}
		
		if ((p_fDeltaTime * 1000 >= 1 && m_pxPlayer->GetY() <= 50 && m_iPlayerAir <= 300 && !m_bCaveMode) || m_pxPlayer->GetState() == AMULET)
		{
			m_iPlayerAir += 1.0;
			m_fShipTimer += p_fDeltaTime;
			if(m_xBigBreath->getStatus() != sf::Sound::Playing && m_bUnderWater)
			{
				m_xBigBreath->play();
				m_bUnderWater = false;
			}

		}

		if (m_iPlayerAir < 100)
		{
			m_xLungs.setScale(1.5, 1.5);
			m_xLungs.setColor(sf::Color::Red);
			m_xHealthBar.setColor(sf::Color::Red);
			m_xLungs.setPosition(80, 460);
			if (m_xHeartBeat->getStatus() != sf::Sound::Playing)
			{
				m_xHeartBeat->play();
			}
		}
		else
		{
			m_xLungs.setScale(1, 1);
			m_xLungs.setColor(sf::Color::White);
			m_xHealthBar.setColor(sf::Color::White);
			m_xLungs.setPosition(80, 460);
			if (m_xHeartBeat->getStatus() == sf::Sound::Playing)
			{
				m_xHeartBeat->stop();
			}
		}

		if (m_iPlayerAir > 300)
		{
			m_iPlayerAir = 300;
		}



		sf::IntRect m_xHealthRect(0, 0, m_iPlayerAir, 60);
		m_xHealthBar.setTextureRect(m_xHealthRect);
		

		//Handle AmuletPower-Up
		if (m_pxAmulet != nullptr)
		{
			m_pxAmulet->Update(p_fDeltaTime);
		}
		if (m_pxAmulet != nullptr && m_pxAmulet->GetX() + m_pxAmulet->GetSprite().getTextureRect().width <= 0)
		{
			delete m_pxAmulet;
			m_pxAmulet = nullptr;
		}

		//Handle FishFood Power-Up
		if (m_pxFishFoodPickup != nullptr)
		{
			m_pxFishFoodPickup->Update(p_fDeltaTime);
		}
		if (m_pxFishFoodPickup != nullptr && m_pxFishFoodPickup->GetX() + m_pxFishFoodPickup->GetSprite().getTextureRect().width <= 0)
		{
			delete m_pxFishFoodPickup;
			m_pxFishFoodPickup = nullptr;
		}




		//Handle GoldenHarpoon power up
		if (m_pxGoldenHarpoonPickup != nullptr)
		{
			m_pxGoldenHarpoonPickup->Update(p_fDeltaTime);
		}
		if (m_pxGoldenHarpoonPickup != nullptr && m_pxGoldenHarpoonPickup->GetX() + m_pxGoldenHarpoonPickup->GetSprite().getTextureRect().width <= 0)
		{
			delete m_pxGoldenHarpoonPickup;
			m_pxGoldenHarpoonPickup = nullptr;
		}

	
		if (Utility::RandomFloat(1, 100) >= 99)
		{
			m_bSpawnCoins = !m_bSpawnCoins;
		}
		// Handle Coins 
		if (m_fCoinTimer >= m_fCoinTimerTarget && m_bSpawnCoins)
		{
			

			{
				bool bFound = false;
				auto it = m_paxCoins.begin();
				while (it != m_paxCoins.end() && m_paxCoins.size() != 0)
				{

					if (!(it)->IsActive() && !bFound)
					{
						(it)->Activate();
						bFound = true;
					}
					it++;
				}
			}

			m_fCoinTimer = 0.0;
		}

		{
			auto it = m_paxCoins.begin();
			while (it != m_paxCoins.end() && m_paxCoins.size() != 0)
			{

				if ((it)->IsActive())
				{
					(it)->Update(p_fDeltaTime);

					if ((it)->GetX() + (it)->GetSprite().getTextureRect().width <= 0)
					{
						(it)->Deactivate();

					}
				}
				it++;
			}
		}
		m_xSystem.m_pxEnemyManager->Update(p_fDeltaTime, m_pxPlayer);
	}


	
	m_pxBackgroundManager->Update(p_fDeltaTime, m_xSystem.m_pxDrawManager, m_bCaveMode);
	CheckCollision(p_fDeltaTime);


	

	m_xSystem.m_pxDrawManager->SetView();
	return true;

}

void GameState::Draw()
{

	m_pxBackgroundManager->DrawBackground(m_xSystem.m_pxDrawManager);

	//Draw Player
	m_xSystem.m_pxDrawManager->Draw(m_pxPlayer->GetSprite());

	// Draw Power Up HUD
	//m_xSystem.m_pxDrawManager->Draw(m_xPowerUpHudBackgroundSprite); 

	


	//Draw Power-Ups
	if (m_pxAmulet != nullptr)
	{
		m_xSystem.m_pxDrawManager->Draw(m_pxAmulet->GetSprite());
	}
	if (m_pxFishFoodPickup != nullptr)
	{
		m_xSystem.m_pxDrawManager->Draw(m_pxFishFoodPickup->GetSprite());
	}

	//Draw golden harpoon
	if (m_pxGoldenHarpoonPickup != nullptr)
	{
		m_xSystem.m_pxDrawManager->Draw(m_pxGoldenHarpoonPickup->GetSprite());
	}
	
	//Draw Harpoon
	if (m_pxPlayer->GetHarpoon() != nullptr)
	{
		m_xSystem.m_pxDrawManager->Draw(m_pxPlayer->GetHarpoon()->GetSprite());
	}

	//Draw Fishfood
	if (m_pxPlayer->GetFishFood() != nullptr)
	{
		m_xSystem.m_pxDrawManager->Draw(m_pxPlayer->GetFishFood()->GetSprite());
	}
	m_xSystem.m_pxEnemyManager->Draw(m_xSystem.m_pxDrawManager);

	m_xSystem.m_pxDrawManager->Draw(m_xGoldHeapSprite);

	{
		auto it = m_paxCoins.begin();
		while (it != m_paxCoins.end() && m_paxCoins.size() != 0)
		{

			if ((it)->IsActive())
			{
				m_xSystem.m_pxDrawManager->Draw((it)->GetSprite());
			}
			it++;
		}
	}




	if (m_xShip.IsActive())
	{
		m_xSystem.m_pxDrawManager->Draw(m_xShip.GetSprite());
	}

	m_pxBackgroundManager->DrawForeground(m_xSystem.m_pxDrawManager);

	//Draw Healthbar

	m_xSystem.m_pxDrawManager->Draw(m_xPlankSprite);
	m_xSystem.m_pxDrawManager->Draw(m_xHealthBarOutline);
	m_xSystem.m_pxDrawManager->Draw(m_xHealthBar);
	m_xSystem.m_pxDrawManager->Draw(m_xLungs);
	m_xSystem.m_pxDrawManager->Draw(m_pxPowerUpHUD->GetSprite());
	m_xSystem.m_pxDrawManager->Draw(m_pxPowerUpHUD->GetHarpoonSprite());
	m_xSystem.m_pxDrawManager->Draw(m_pxPowerUpHUD->GetFishFoodSprite());
	m_xSystem.m_pxDrawManager->Draw(m_pxPowerUpHUD->GetPilSprite());
	m_xSystem.m_pxDrawManager->Draw(m_xMultiplierBar);
	




	//Multiplier bar
	
	


	
	
	//Score

	m_xSystem.m_pxDrawManager->DrawText(m_xScoreText, sf::BlendAlpha);
	m_xSystem.m_pxDrawManager->DrawText(m_xMultiPlierText);
	m_xSystem.m_pxDrawManager->DrawText(m_xLevelTimer);
	
	if (m_pxPlayer->GetState() == AMULET)
	{
		m_xSystem.m_pxDrawManager->Draw(m_xBubbleSkip);
	}
	if (m_bTutorial)
	{
		m_xSystem.m_pxDrawManager->Draw(m_xTutorialSprite);
	}
	if (m_bPause)
	{
		//TODO: Make pretty
		sf::Texture xTexture;
		sf::Sprite xSprite;
		xSprite.setTexture(xTexture);
		xSprite.setTextureRect(sf::IntRect(0, 0, 1920, 1080));
		xSprite.setColor(sf::Color(0,0,0,100));
		m_xSystem.m_pxDrawManager->Draw(xSprite);
		sf::Font font;
		font.loadFromFile("../assets/OldLondon.ttf");
		sf::Text Text;
		Text.setFont(font);
		Text.setColor(sf::Color::White);
		Text.setString("Paused");
		Text.setPosition(sf::Vector2f((m_xSystem.m_iScreenWidth / 2) - 200, m_xSystem.m_iScreenHeight / 2));
		Text.setCharacterSize(120);
		m_xSystem.m_pxDrawManager->DrawText(Text);
	}
	
	m_xSystem.m_pxDrawManager->Draw(m_xCursorSprite);
	
}




void GameState::CheckCollision(float p_fDeltaTime)
{
	if (m_xShip.IsActive() && m_pxPlayer->GetCollitionRect().intersects(m_xShip.GetSprite().getGlobalBounds()))
	{
		m_pxPlayer->IsBoatHit(true);
		m_pxPlayer->GetTakeDamageSound();
	}



	//Check Swordfish Collision

	if (m_xSystem.m_pxEnemyManager->CheckSwordfishCollision(m_pxPlayer->GetSprite().getGlobalBounds()))
	{
		if (m_pxPlayer->GetState() == AMULET)
		{
			//m_pxPlayer->SetState(DEFAULT);
		}
		else
		{
			m_iPlayerAir -= 25;
			m_bMultiplierCheck = false;
			m_pxPlayer->GetTakeDamageSound();
			m_pxPlayer->IsHit(true);
			m_pxPlayer->SetHitColorRed();

		}

	}
	if (m_pxPlayer->GetHarpoon() != nullptr && m_xSystem.m_pxEnemyManager->CheckSwordfishCollision(m_pxPlayer->GetHarpoon()->GetSprite().getGlobalBounds()) && m_pxPlayer->GetHarpoon()->GetState() != RETRACT)
	{
		if (Utility::RandomFloat(0, 100) > 50)
		{
			int iChoosePowerup = Utility::RandomFloat(0, 100);
			if (iChoosePowerup > 80 && m_pxAmulet == nullptr)
			{
				m_pxAmulet = new Amulet(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
			}
			if (iChoosePowerup > 40 && iChoosePowerup < 80 && m_pxFishFoodPickup == nullptr)
			{
				if (Utility::RandomFloat(0, 3) == 2)
				{
					m_pxFishFoodPickup = new FishFoodPickup(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
				}
				if (iChoosePowerup < 40 && m_pxGoldenHarpoonPickup == nullptr)
				{
					m_pxGoldenHarpoonPickup = new GoldenHarpoonPickup(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
				}
			}
		
		}
		if (m_pxPlayer->GetGolden() == true)
		{
			m_iScore += (100 * m_iMultiplier);

		}
		else
		{
			m_iScore += (100 * m_iMultiplier);
			m_pxPlayer->GetHarpoon()->SetState(RETRACT);
		}

		m_iRndmEnemySound = Utility::RandomFloat(1, 20);

		if (m_iRndmEnemySound < 10)
		{
			m_xEnemyDamage->play();
		}
		if (m_iRndmEnemySound > 10)
		{
			m_xEnemyDamage2->play();
		}
	}
			//Check collision Piranha

			if (m_xSystem.m_pxEnemyManager->CheckPiranhaCollision(m_pxPlayer->GetSprite().getGlobalBounds()))
			{
				if (m_pxPlayer->GetState() == AMULET)
				{
					//m_pxPlayer->SetState(DEFAULT);
					m_xPiranhaBite->play();
				}
				else
				{
					m_iPlayerAir -= 50;

					m_bMultiplierCheck = false;
					m_pxPlayer->GetTakeDamageSound();
					m_pxPlayer->IsHit(true);
					m_pxPlayer->SetHitColorRed();
					m_xPiranhaBite->play();
				}
			}
			if (m_pxPlayer->GetHarpoon() != nullptr && m_xSystem.m_pxEnemyManager->CheckPiranhaCollision(m_pxPlayer->GetHarpoon()->GetSprite().getGlobalBounds()) && m_pxPlayer->GetHarpoon()->GetState() != RETRACT)
			{
				if (Utility::RandomFloat(0, 100) > 50)
				{
					int iChoosePowerup = Utility::RandomFloat(0, 100);
					if (iChoosePowerup > 80 && m_pxAmulet == nullptr)
					{
						m_pxAmulet = new Amulet(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
					}
					if (iChoosePowerup > 40 && iChoosePowerup < 80 && m_pxFishFoodPickup == nullptr)
					{
						m_pxFishFoodPickup = new FishFoodPickup(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
					}
					if (iChoosePowerup < 40 && m_pxGoldenHarpoonPickup == nullptr)
					{
						m_pxGoldenHarpoonPickup = new GoldenHarpoonPickup(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);

					}
				}

				if (m_pxPlayer->GetGolden() == true)
				{
					m_iScore += (150 * m_iMultiplier);
				}
				else
				{
					m_iScore += (150 * m_iMultiplier);
					m_pxPlayer->GetHarpoon()->SetState(RETRACT);
				}
				m_iRndmEnemySound = Utility::RandomFloat(1, 20);

				if (m_iRndmEnemySound < 10)
				{
					m_xEnemyDamage->play();
				}
				if (m_iRndmEnemySound > 10)
				{
					m_xEnemyDamage2->play();
				}
			}

			//JellyRoger


			if (m_xSystem.m_pxEnemyManager->CheckJellyRogerCollision(m_pxPlayer->GetSprite().getGlobalBounds()))
			{
				if (m_pxPlayer->GetState() == AMULET)
				{
					//m_pxPlayer->SetState(DEFAULT);
					//m_pxPlayer->GetTakeDamageSound();
				}
				else
				{
					m_iPlayerAir -= 10;
					m_pxPlayer->SetState(STUNNED);
					m_bMultiplierCheck = false;
					m_pxPlayer->GetTakeDamageSound();

					m_pxPlayer->IsHit(true);
					m_pxPlayer->SetHitColorRed();

				}

			}

			if (m_pxPlayer->GetHarpoon() != nullptr && m_xSystem.m_pxEnemyManager->CheckJellyRogerCollision(m_pxPlayer->GetHarpoon()->GetSprite().getGlobalBounds()))
			{
				if (Utility::RandomFloat(0, 100) > 50)
				{
					int iChoosePowerup = Utility::RandomFloat(0, 100);
					if (iChoosePowerup > 80 && m_pxAmulet == nullptr)
					{
						m_pxAmulet = new Amulet(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
					}
					if (iChoosePowerup > 40 && iChoosePowerup < 80 && m_pxFishFoodPickup == nullptr)
					{
						m_pxFishFoodPickup = new FishFoodPickup(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);
					}
					if (iChoosePowerup < 40 && m_pxGoldenHarpoonPickup == nullptr)
					{
						m_pxGoldenHarpoonPickup = new GoldenHarpoonPickup(m_xSystem.m_pxSpriteManager, m_xSystem.m_pxEnemyManager->GetDropRect().left, m_xSystem.m_pxEnemyManager->GetDropRect().top, m_xSystem.m_iScreenWidth, m_xSystem.m_iScreenHeight);

					}
				}
				if (m_pxPlayer->GetGolden() == true)
				{
					m_iScore += (50 * m_iMultiplier);
				}
				else
				{
					m_pxPlayer->GetHarpoon()->SetState(RETRACT);
					m_iScore += (50 * m_iMultiplier);
				}

				m_iRndmEnemySound = Utility::RandomFloat(1, 20);

				if (m_iRndmEnemySound < 10)
				{
					m_xEnemyDamage->play();
				}
				if (m_iRndmEnemySound > 10)
				{
					m_xEnemyDamage2->play();
				}
			}



			if (m_xSystem.m_pxEnemyManager->CheckJellyProjectileCollision(m_pxPlayer->GetSprite().getGlobalBounds()))
			{

				if (m_pxPlayer->GetState() == AMULET)
				{
					//m_pxPlayer->SetState(DEFAULT);
				}
				else
				{
					m_iPlayerAir -= 25;
					m_pxPlayer->GetTakeDamageSound();
					m_bMultiplierCheck = false;
					m_pxPlayer->IsHit(true);
					m_pxPlayer->SetHitColorRed();

					m_pxPlayer->SetState(STUNNED);
				}

			}

			{
				auto it = m_paxCoins.begin();
				while (it != m_paxCoins.end() && m_paxCoins.size() != 0)
				{
					if ((it)->IsActive())
					{

						if (m_pxPlayer->GetCollitionRect().intersects((it)->GetSprite().getGlobalBounds()))
						{

							(it)->Deactivate();
							m_iScore += (500 * m_iMultiplier);
							m_iPlayerAir += 25;
							m_xCoinSound->play();


						}
					}
					it++;
				}
			}



			if (m_pxPlayer->GetHit() == true)
			{
				m_fHitTimer += p_fDeltaTime;

				if (m_fHitTimer >= 0.2f)
				{
					m_pxPlayer->SetHitColorWhite();
					m_fHitTimer = 0;
					m_pxPlayer->IsHit(false);
				}
			}


			if (m_pxPlayer->GetBoatHit() == true)
			{
				m_fHitTimer += p_fDeltaTime;

				if (m_fHitTimer >= 0.2f)
				{
					m_fHitTimer = 0;
					m_pxPlayer->IsBoatHit(false);
				}
			}
			//Powerupljud
			if (m_pxAmulet != nullptr && m_pxPlayer->GetSprite().getGlobalBounds().intersects(m_pxAmulet->GetSprite().getGlobalBounds()))
			{
				m_pxPowerUpHUD->AddPowerup(HUD_AMULET);
				delete m_pxAmulet;
				m_pxAmulet = nullptr;
				m_pxPlayer->GetPowerUpSound();
				m_fBubbleSize = 1.0f;
				m_fBubbleOriginX = 0.0f;
				m_fBubbleOriginY = 0.0f;
				m_fStateTimer = 0.0f;

			}
			if (m_pxFishFoodPickup != nullptr && m_pxPlayer->GetSprite().getGlobalBounds().intersects(m_pxFishFoodPickup->GetSprite().getGlobalBounds()))
			{
				m_pxPowerUpHUD->AddPowerup(HUD_FISHFOOD);
				delete m_pxFishFoodPickup;
				m_pxFishFoodPickup = nullptr;
				m_pxPlayer->GetPowerUpSound();
			}
			if (m_pxGoldenHarpoonPickup != nullptr && m_pxPlayer->GetSprite().getGlobalBounds().intersects(m_pxGoldenHarpoonPickup->GetSprite().getGlobalBounds()))
			{
				m_pxPowerUpHUD->AddPowerup(HUD_HARPOON);
				delete m_pxGoldenHarpoonPickup;
				m_pxGoldenHarpoonPickup = nullptr;
				m_pxPlayer->GetPowerUpSound();
			}
		
}


IState* GameState::NextState()
{
	m_xBackgroundMusic->stop();
	m_xSystem.m_pxEnemyManager->Deactivate();
	if (m_bQuit)
	{
		return nullptr;
	}
	if (m_iPlayerAir <= 0)
	{
		return new LoseState(m_xSystem);
	}
	else
	{
		return new WinState(m_xSystem);
	}
}