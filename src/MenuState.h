#pragma once
#include "IState.h"
#include "stdafx.h"
#define MAX_NUMBER_OF_ITEMS 4


class DrawManager;

class MenuState : public IState
{
public:
	MenuState(System& p_xSystem);
	~MenuState();

	void Draw();
	void MoveUp();
	void MoveDown();
	bool Update(float p_fDeltaTime);
	int GetPressedItem() { return selectedItemIndex; }
	IState* NextState();

private:
	int iMenu;
	int selectedItemIndex;
	
	int width = 1920;
	int height = 1080;
	int fontSize = 100;
	int scoreInt = 0;
	int selectedFontSize = 150;
	float m_fMoveViewY = 540;
	float m_fMoveSkip = 280;
	bool m_bViewBool = false;
	bool m_bButtonBoolPlay = true;
	bool m_bButtonBoolExit = true;
	bool m_bButtonBoolSettings = true;
	bool m_bButtonBoolHigh = true;
	bool m_bQuit = false;
	sf::Font font;
	sf::Text menu[MAX_NUMBER_OF_ITEMS];
	System m_xSystem;
	sf::RenderWindow* p_pxWindow;
	sf::Vector2i xMousePosition;
	sf::Event m_xEvent;
	sf::Texture m_xMenuTexture;
	sf::Texture m_xButtonTexture;
	sf::Texture m_xCursor;
	sf::Texture m_xSkipTexture;
	sf::Sprite m_xSkipSprite;
	sf::Sprite m_xCursorSprite;
	sf::Sprite m_xPlay;
	sf::Sprite m_xSettings;
	sf::Sprite m_xExit;
	sf::Sprite m_xMenuSprite;
	sf::Sprite m_xHighScore;
	sf::Sound* m_xSkipSound;
	sf::Sound* m_xSkipLine;
	sf::Sound* m_xButtonSound;
	std::string m_scorestring;
};
