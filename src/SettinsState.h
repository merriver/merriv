#pragma once
#include "IState.h"
#include "stdafx.h"
#define MAX_NUMBER_OF_ITEMS 3
#define MAX_NUMBER_OF_BUTTONS 2
#define MAX_NUBER_OF_CHOICES 10

class DrawManager;

class SettingsState : public IState
{
public:
	SettingsState(System& p_xSystem);
	~SettingsState();

	void Draw();
	void MoveUp();
	void MoveDown();
	bool Update(float p_fDeltaTime);
	int GetPressedItem() { return selectedItemIndex; }
	IState* NextState();


private:
	int selectedItemIndex;
	int width = 1920;
	int height = 1080;
	int fontSize = 100;
	int selectedFontSize = 150;
	int iVolume = 100;
	sf::Font font;
	sf::Text menu[MAX_NUMBER_OF_ITEMS];
	sf::Text Buttons[MAX_NUMBER_OF_BUTTONS];
	sf::Text Volume[MAX_NUBER_OF_CHOICES];

	sf::Texture m_xCursor;
	sf::Texture m_xMenuTexture;
	sf::Sprite m_xMenuSprite;
	sf::Sprite m_xCursorSprite;
	sf::Sprite m_pxSprite;
	System m_xSystem;
	sf::RenderWindow* p_pxWindow;
	sf::Vector2i m_xMousePosition;
	sf::Event event;
	//sf::Listener m_xVolume;
};