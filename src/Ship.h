#pragma once

#include "IEntity.h"


class sf::Sprite;
class SpriteManager;


class Ship
{
public:
	Ship();
	~Ship();
	bool Initialize();
	void Update(float p_fDeltaTime);
	sf::Sprite GetSprite();
	float GetX();
	float GetY();
	bool IsActive();
	void Deactivate();
	void Activate();

private:
	sf::Sprite m_xSprite;
	sf::Texture m_xTexture;
	float m_fSpeed = 5;
	float m_fX;
	float m_fY;
	int m_iScreenWidth;
	int m_iScreenHeight;
	bool m_bActive = false;
};