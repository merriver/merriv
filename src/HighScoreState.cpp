#include "stdafx.h"
#include "HighScoreState.h"
#include <iostream>
#include "DrawManager.h"
#include "GameState.h"
#include "MenuState.h"



HighScoreState::HighScoreState(System& p_xSystem, int p_iScore, std::string p_sName)
{
	m_xSystem = p_xSystem;
	m_xMenuTexture.loadFromFile("../assets/Start_Menu_2.png");
	m_xPaperTexture.loadFromFile("../assets/Pergament.png");
	m_xCursor.loadFromFile("../assets/muspekare.png");
	m_xCursorSprite.setTexture(m_xCursor);
	m_xCursorSprite.setTextureRect(sf::IntRect(0, 0, 60, 60));
	m_xCursorSprite.setOrigin(30, 30);
	m_xPaperSprite.setTexture(m_xPaperTexture);
	m_xPaperSprite.setPosition((width / 2) - 325, (height / 2) - 440);
	m_xMenuSprite.setTexture(m_xMenuTexture);
	

	if (!font.loadFromFile("../assets/OldLondon.ttf"))
	{
		std::cout << "Unable to load textfile" << std::endl;
	}


	
	testBlend.blendMode = blendMode;

	

	back.setFont(font);
	back.setColor(sf::Color::Blue);
	back.setString("Back");
	back.setPosition(sf::Vector2f(width / 8, height - 900));
	back.setCharacterSize(fontSize);

	m_iScore = p_iScore;
	m_xScoreName = p_sName;
	
	m_xHeadline.setFont(font);
	m_xHeadline.setCharacterSize(fontSize);
	m_xHeadline.setPosition((width / 2)- 100, (height / 2) - 410);
	m_xHeadline.setColor(sf::Color::Black);
	m_xHeadline.setString("Highscore");

	
	SortScore(m_iScore, m_xScoreName);

	{
		auto it = highScoreList.rbegin();
		int index = 0;
		while (index < m_xScore.size() && it != highScoreList.rend())
		{
			m_xScore[index].setString(std::to_string(it->first));
			m_xScore[index].setFont(font);
			m_xScore[index].setCharacterSize(scoreFontSize);
			m_xScore[index].setColor(sf::Color::Black);
			float offset = m_xScore[index].getGlobalBounds().height + 10.0f;
			m_xScore[index].setPosition((width / 2) + 150, ((height / 2) - 200) + (offset * index));
			index++;
			it++;
		}
	}

	{
		auto it = highScoreList.rbegin();
		int index = 0;
		while (index < m_xName.size() && it != highScoreList.rend())									
		{
			m_xName[index].setString(it->second);
			m_xName[index].setFont(font);
			m_xName[index].setCharacterSize(scoreFontSize);
			m_xName[index].setColor(sf::Color::Black);
			
			float offset = m_xScore[index].getGlobalBounds().height + 10.0f;
			m_xName[index].setPosition((width / 2) - 150, ((height / 2) - 200) + (offset * index));
			index++;
			it++;
		}
	}

	
	


}

HighScoreState::~HighScoreState()
{
}



void HighScoreState::Draw()
{
	m_xSystem.m_pxDrawManager->Draw(m_xMenuSprite);

	m_xSystem.m_pxDrawManager->Draw(m_xPaperSprite);


	m_xSystem.m_pxDrawManager->DrawText(back);
	

	m_xSystem.m_pxDrawManager->DrawText(m_xHeadline);

	for (int i = m_xName.size() - 1; i >= 0; i--)
	{
		m_xSystem.m_pxDrawManager->DrawText(m_xName[i]);
	}

	for (int i = m_xScore.size() - 1; i >= 0; i--)
	{
		m_xSystem.m_pxDrawManager->DrawText(m_xScore[i]);
	}

	m_xSystem.m_pxDrawManager->Draw(m_xCursorSprite);

}

void HighScoreState::SortScore(int p_iScoreData, std::string p_sNameData)
{

	m_iScoreData = p_iScoreData;
	m_sNameData = p_sNameData;
	std::ifstream nameFile("../assets/HighScoreName.csv");
	std::ifstream scoreFile("../assets/HighScore.csv");
	
	

	highScoreList.insert(std::pair<int, std::string>(m_iScoreData, m_sNameData));
	scoreArray[0] = m_iScoreData;

	if (nameFile.is_open(), scoreFile.is_open())
	{
		for (int i = 1; i < 11; i++)
		{
			scoreFile >> scoreArray[i];
			nameFile >> name;
			highScoreList.insert(std::pair<int, std::string>(scoreArray[i], name));

		}
		nameFile.close();
		scoreFile.close();
	}

	Sort(scoreArray);

	std::ofstream newNameFile("../assets/HighScoreName.csv", std::ofstream::out | std::ofstream::trunc);
	std::ofstream newScoreFile("../assets/HighScore.csv", std::ofstream::out | std::ofstream::trunc);

	if (newNameFile.is_open(), newScoreFile.is_open())
	{
		for (int i = 0; i < highScoreList.size(); i++)
		{
			std::cout << highScoreList.find(scoreArray[i])->second << "\t" << scoreArray[i] << std::endl;
			newNameFile << highScoreList.find(scoreArray[i])->second << std::endl;
			newScoreFile << scoreArray[i] << std::endl;
			//
			
			
		}
		newNameFile.close();
		newScoreFile.close();
	}
	/*auto it = highScoreList.find(scoreArray[scoreArray.size()]);
	highScoreList.erase(it);*/
}

void HighScoreState::Sort(std::array<int, 11> &data)
{
	for (int i = 10; i >= 1; i--)
	{
		for (int j = 0; j <= i - 1; j++)
		{
			if (data[j] < data[j + 1]) {
				int tmp = data[j];
				data[j] = data[j + 1];
				data[j + 1] = tmp;
			}
		}
	}

}


bool HighScoreState::Update(float p_fDeltaTime)
{

	sf::RenderWindow* xWindow = m_xSystem.m_pxDrawManager->GetRenderWindow();
	sf::Vector2f xMousePosition = static_cast<sf::Vector2f>(sf::Mouse::getPosition(*m_xSystem.m_pxDrawManager->GetRenderWindow()));
	m_xCursorSprite.setPosition(xMousePosition);
	m_xCursorSprite.setTexture(m_xCursor);

	if (back.getGlobalBounds().contains(xMousePosition))
	{
		back.setColor(sf::Color::Red);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			return false;
		}
	}

	


	return true;
}

IState * HighScoreState::NextState()
{
	return new MenuState(m_xSystem);
}


