#include "stdafx.h"
#include "Utility.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Player.h"
#include "JellyProjectile.h"
#include "JellyRoger.h"

JellyProjectile::JellyProjectile()
{
	
}

JellyProjectile::~JellyProjectile()
{
}

bool JellyProjectile::Initialize(SpriteManager * p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight)
{
	m_pxAnimatedTextureDefault = p_pxSpriteManager->CreateAnimatedTexture("../assets/JellyProjectile.png");
	//m_pxAnimatedTextureDefault->AddFrame(120, 123, 73, 65, 0.05);
	//m_pxAnimatedTextureDefault->AddFrame(400, 100, 105, 86, 0.05);
	m_pxAnimatedTextureDefault->AddFrame(595, 0, 287, 300, 0.05);
	m_pxAnimatedTextureDefault->AddFrame(890, 0, 310, 300, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1192, 0, 287, 300, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1490, 0, 310, 300, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(1786, 0, 287, 300, 0.1);
	m_pxAnimatedTextureDefault->AddFrame(2100, 0, 310, 300, 0.1);

	m_pxSprite.setTextureRect(m_pxAnimatedTextureDefault->GetRegion());
	m_pxSprite.setTexture(m_pxAnimatedTextureDefault->GetTexture());
	m_pxSprite.setScale(0.5, 0.5);

	m_fX = p_fX;
	m_fY = p_fY;
	m_pxSprite.setPosition(m_fX, m_fY);
	m_fSpeed = 4.0f;
	m_fDirX = -1.0f;
	m_fDirY = 0.0f;


	m_iScreenWidth = p_iScreenWidth;
	m_iScreenHeight = p_iscreenHeight;

	return true;
}

void JellyProjectile::Update(float p_fDeltaTime)
{
	m_pxAnimatedTextureDefault->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTextureDefault->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTextureDefault->GetRegion());

	m_pxSprite.setOrigin(m_pxSprite.getGlobalBounds().width / 2, m_pxSprite.getGlobalBounds().height / 2);
	m_pxSprite.setPosition(m_fX += m_fDirX * m_fSpeed * 2, m_fY += m_fDirY * m_fSpeed);
	
}

sf::Sprite JellyProjectile::GetSprite()
{
	return m_pxSprite;
}
AnimatedTexture * JellyProjectile::GetTexture()
{
	return m_pxAnimatedTextureDefault;
}

float JellyProjectile::GetX()
{
	return m_fX;
}

float JellyProjectile::GetY()
{
	return m_fY;
}

bool JellyProjectile::IsActive()
{
	return m_bActive;
}

void JellyProjectile::Deactivate()
{
	m_pxSprite.setPosition(2000, 0);
	m_bActive = false;
}

void JellyProjectile::Activate(int p_fX, int p_fY)
{
	m_bActive = true;
	m_fX = p_fX;
	m_fY = p_fY;
	m_pxSprite.setPosition(m_fX, m_fY);
}

EENTITYTYPE JellyProjectile::GetType()
{
	return EENTITYTYPE::ENTITY_JELLYPROJECTILE;
}
