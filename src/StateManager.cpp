#include "stdafx.h"
#include "StateManager.h"
#include "IState.h"
#include "DrawManager.h"

StateManager::StateManager()
{
	m_pxCurrentState = nullptr;
	m_fAccumulator = 0.0f;
	m_fTargetTime = 1.0f / 60.0f;
	m_fFrameTime = 0.0f;
}

StateManager::~StateManager()
{
	// Before using current state we check that it is not null,
	// if it is not null we call the states Exit function and then
	// delete the state and null the pointer.
	if (m_pxCurrentState != nullptr)
	{
		delete m_pxCurrentState;
		m_pxCurrentState = nullptr;
	}
}

bool StateManager::Update()
{

	sf::Time m_xDeltaTime = clock.restart();
	m_fFrameTime = m_xDeltaTime.asSeconds();

	if (m_fFrameTime > 0.1f)
		m_fFrameTime = 0.1f;

	m_fAccumulator += m_fFrameTime;

	while (m_fAccumulator > m_fTargetTime)
	{
		m_fAccumulator -= m_fTargetTime;
		if (m_pxCurrentState != nullptr)
		{
			
			if (!m_pxCurrentState->Update(m_fTargetTime))
			{
				SetState(m_pxCurrentState->NextState());
			}
			if (m_pxCurrentState == nullptr)
			{
				return false;
			}
		}

	}


	return true;
}

void StateManager::Draw()
{
	if (m_pxCurrentState != nullptr)
	{
		m_pxCurrentState->Draw();
	}
}

void StateManager::SetState(IState* p_pxState)
{
	// If the current state is not null then call exit and delete the object
	// and null the pointer.
	if (m_pxCurrentState != nullptr)
	{
		delete m_pxCurrentState;
		m_pxCurrentState = nullptr;
	}
	// Sets the current state to be the state sent as parameter and 
	// call Enter function on the current state.
	m_pxCurrentState = p_pxState;
}


