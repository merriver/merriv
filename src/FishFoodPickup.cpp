#include "stdafx.h"
#include "FishFoodPickup.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Utility.h"


FishFoodPickup::FishFoodPickup(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight)
{
	m_pxAnimatedTexture = p_pxSpriteManager->CreateAnimatedTexture("../assets/fiskmatpick.png");
	m_pxAnimatedTexture->AddFrame(0, 0, 239, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(251, 0, 245, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(506, 0, 250, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(750, 0, 249, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(1001, 0, 245, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(1254, 0, 253, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(1501, 0, 247, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(1750, 0, 243, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(2000, 0, 242, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(2252, 0, 246, 250, 0.05);
	m_pxAnimatedTexture->AddFrame(2504, 0, 247, 250, 0.05);

	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setScale(0.6, 0.6);
	m_fX = p_fX;
	m_fY = p_fY;
	m_pxSprite.setPosition(m_fX, m_fY);
}

FishFoodPickup::~FishFoodPickup()
{
}

void FishFoodPickup::Update(float p_fDeltaTime)
{
	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());

	m_pxSprite.setPosition(m_fX += -5, m_fY += 1);
}

sf::Sprite FishFoodPickup::GetSprite()
{
	return m_pxSprite;
}


float FishFoodPickup::GetX()
{
	return m_fX;
}

float FishFoodPickup::GetY()
{
	return m_fY;
}

EENTITYTYPE FishFoodPickup::GetType()
{
	return ENTITY_POWERUP;
}
