#include "stdafx.h"
#include "Bubble.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Utility.h"

Bubble::Bubble(SpriteManager * p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight)
{
	m_pxAnimatedTexture = p_pxSpriteManager->CreateAnimatedTexture("../assets/BubbleTest.png");
	m_pxAnimatedTexture->AddFrame(87,43,142,134,0.5);
	/*m_pxAnimatedTexture->AddFrame(252, 50, 128, 115, 0.5);
	m_pxAnimatedTexture->AddFrame(416, 68, 87, 87, 0.5);
	m_pxAnimatedTexture->AddFrame(519, 51, 120, 118, 0.5);
	m_pxAnimatedTexture->AddFrame(680, 44, 133, 132, 0.5);*/
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setScale(0.50, 0.50);
	m_fX = p_fX;
	m_fY = p_fY;
	m_pxSprite.setPosition(m_fX, m_fY);
}

Bubble::~Bubble()
{
}

void Bubble::Update(float p_fDeltaTime)
{
	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setScale(1, 1);

	m_pxSprite.setPosition(m_fX += -5, m_fY);
}

sf::Sprite Bubble::GetSprite()
{
	return m_pxSprite;
}

float Bubble::GetX()
{
	return m_fX;
}

float Bubble::GetY()
{
	return m_fY;
}

EENTITYTYPE Bubble::GetType()
{
	return ENTITY_BUBBLE;
}
