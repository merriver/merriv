#include "PowerUpHUD.h"
#include "stdafx.h"
#include <iostream>
#include "DrawManager.h"
#include "GoldenHarpoonPickup.h"
#include "FishFood.h"
#include "Amulet.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"

PowerUpHUD::PowerUpHUD(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight)
{
	m_xBools.m_bGoldenHarpoon = false;
	m_xBools.m_bAmulet = false;
	m_xBools.m_bFishFood = false;
	m_xBools.m_bGoldenHarpoonSelected = false;
	m_xBools.m_bAmuletSelected = true;
	m_xBools.m_bFishFoodSelected = false;
	m_pxSpriteManager = p_pxSpriteManager;
	//TODO: change load from file to get texture
	m_xTextureAmulet.loadFromFile("../assets/od�dlig.png");
	m_xAmulet.setTexture(m_xTextureAmulet);
	m_xAmulet.setTextureRect(sf::IntRect(16, 1, 222, 249));
	m_xAmulet.setPosition(sf::Vector2f(50, 600));
	m_xAmulet.setColor(sf::Color(255, 255, 255, 128));
	m_xAmulet.setScale(0.3, 0.3);


	m_xTextureGoldenHarpoon.loadFromFile("../assets/s harpunis.png");
	m_xGoldenHarpoon.setTexture(m_xTextureGoldenHarpoon);
	m_xGoldenHarpoon.setTextureRect(sf::IntRect(10, 30, 533, 166));
	m_xGoldenHarpoon.setPosition(sf::Vector2f(80, 710));
	m_xGoldenHarpoon.setColor(sf::Color(255, 255, 255, 128));
	m_xGoldenHarpoon.setRotation(-30);
	m_xGoldenHarpoon.setOrigin(250, 75);
	m_xGoldenHarpoon.setScale(0.2, 0.3);
	
	m_xTextureFishFood.loadFromFile("../assets/chummie pickup.png");
	m_xFishFood.setTexture(m_xTextureFishFood);
	m_xFishFood.setTextureRect(sf::IntRect(20, 5, 207, 227));
	m_xFishFood.setPosition(sf::Vector2f(50, 750));
	m_xFishFood.setColor(sf::Color(255, 255, 255, 128));
	m_xFishFood.setScale(0.3, 0.3);

	m_xTexturePil.loadFromFile("../assets/pil.png");
	m_xPil.setTexture(m_xTexturePil);
	m_xPil.setPosition(sf::Vector2f(0, 600));
	m_xPil.setScale(0.75, 0.75);


	PosX = 0;
	PosY = 600;

	m_fX = p_fX;
	m_fY = p_fY;
	m_iScreenHeight = p_iscreenHeight;
	m_iScreenWidth = p_iScreenWidth;
	selectedItemIndex = 1;

}

PowerUpHUD::~PowerUpHUD()
{
}



void PowerUpHUD::MoveToNext()
{
		selectedItemIndex++;
		if (selectedItemIndex > 3)
		{
			selectedItemIndex = 1;
		}
		if (selectedItemIndex == 1)
		{
			m_xPil.setPosition(0, 600);
			m_xBools.m_bAmuletSelected = true;
			m_xBools.m_bGoldenHarpoonSelected = false;
			m_xBools.m_bFishFoodSelected = false;
		}
		else if (selectedItemIndex == 2)
		{
			m_xPil.setPosition(0,  680);
			m_xBools.m_bAmuletSelected = false;
			m_xBools.m_bGoldenHarpoonSelected = true;
			m_xBools.m_bFishFoodSelected = false;
		}
		else if (selectedItemIndex == 3)
		{
			m_xPil.setPosition(0, 760);
			m_xBools.m_bAmuletSelected = false;
			m_xBools.m_bGoldenHarpoonSelected = false;
			m_xBools.m_bFishFoodSelected = true;
		}
}

bool PowerUpHUD::Update(float p_fDeltaTime)
{

	

	if (m_xBools.m_bAmulet)
	{
		m_xAmulet.setColor(sf::Color(255, 255, 255, 255));
	}
	if (m_xBools.m_bFishFood)
	{
		m_xFishFood.setColor(sf::Color(255, 255, 255, 255));
	}
	if (m_xBools.m_bGoldenHarpoon)
	{
		m_xGoldenHarpoon.setColor(sf::Color(255, 255, 255, 255));
	}

	if (!m_xBools.m_bAmulet)
	{
		m_xAmulet.setColor(sf::Color::Black);
	}
	if (!m_xBools.m_bFishFood)
	{
		m_xFishFood.setColor(sf::Color::Black);
	}
	if (!m_xBools.m_bGoldenHarpoon)
	{
		m_xGoldenHarpoon.setColor(sf::Color::Black);
	}
	return true;
}

bools PowerUpHUD::GetBools()
{
	return m_xBools;
}

sf::Sprite PowerUpHUD::GetSprite()
{
	return m_xAmulet;
}

sf::Sprite PowerUpHUD::GetHarpoonSprite()
{
	
	return m_xGoldenHarpoon;
}

sf::Sprite PowerUpHUD::GetFishFoodSprite()
{
	return m_xFishFood;
}

sf::Sprite PowerUpHUD::GetPilSprite()
{
	return m_xPil;
}

void PowerUpHUD::AddPowerup(int p_iPowerup)
{
	if (p_iPowerup == HUD_AMULET)
	{
		m_xBools.m_bAmulet = true;
	}
	if (p_iPowerup == HUD_HARPOON)
	{
		m_xBools.m_bGoldenHarpoon = true;
	}
	if (p_iPowerup == HUD_FISHFOOD)
	{
		m_xBools.m_bFishFood = true;
	}
}

void PowerUpHUD::RemovePowerup(int p_iPowerup)
{
	if (p_iPowerup == HUD_AMULET)
	{
		m_xBools.m_bAmulet = false;
	}
	if (p_iPowerup == HUD_HARPOON)
	{
		m_xBools.m_bGoldenHarpoon = false;
	}
	if (p_iPowerup == HUD_FISHFOOD)
	{
		m_xBools.m_bFishFood = false;
	}
}

float PowerUpHUD::GetX()
{
	return m_fX;
}

float PowerUpHUD::GetY()
{
	return m_fY;
}

EENTITYTYPE PowerUpHUD::GetType()
{
	return EENTITYTYPE::ENTITY_POWERUPHUD;
}

