#pragma once


class AnimatedTexture;
class BackgroundElement;
class DrawManager;
class SpriteManager;

class BackgroundManager
{
public:
	BackgroundManager(SpriteManager* p_pxSpriteManager, int p_iScreenHeight, int p_iScreenWidth);
	~BackgroundManager();

	void CreateBackgroundElement();
	void Update(float p_fDeltaTime, DrawManager* p_pxDrawManager, bool p_bCaveMode);
	void DrawBackground(DrawManager* p_pxDrawManager);
	void DrawForeground(DrawManager* p_pxDrawManager);
	void StopMoving();
	void StartMoving();

private:
	SpriteManager* m_pxSpriteManager;
	std::vector<BackgroundElement> m_apxBackgroundElements;
	std::vector<BackgroundElement> m_apxBackgroundSeafloor;
	std::vector <BackgroundElement> m_apxBackground;
	std::vector <BackgroundElement> m_apxWaves;
	std::vector<sf::IntRect> m_axElementRects;
	std::vector<sf::IntRect> m_axSeafloorRects;
	std::vector<sf::IntRect> m_axBackgroundRects = { 
													sf::IntRect(0,0,2403,1080), sf::IntRect(2403,0,2403,1080), sf::IntRect(4806,0,2403,1080),
													sf::IntRect(0,0,2179,1080), sf::IntRect(2179,0,2179,1080), sf::IntRect(4358,0,2179,1080),
													sf::IntRect(6537,0,2179,1080), sf::IntRect(8716,0,2179,1080), sf::IntRect(10895,0,2179,1080),
													};
	sf::Texture m_xSeafloorObjectTexture;
	sf::Texture m_xMiddleGroundTextures;
	sf::Texture m_xMiddleGroundTextures2;
	sf::Texture m_xSeafloorTexture;
	sf::Texture m_xBackgroundTexture;
	sf::Texture m_xBackgroundTexture2;
	sf::Texture m_xWaveTexture;
	float m_fElementTimer = 0.0;
	float m_fTimerTarget = 0.5;
	int m_iScreenHeight;
	int m_iScreenWidth;
	int m_iBackgroundIndex = 1;
	int m_iCurrentBackground = 0;
	bool m_bStopMoving = true;
};