#include "stdafx.h"
#include "SpriteManager.h"
#include "AnimatedTexture.h"

SpriteManager::SpriteManager()
{

}

SpriteManager::~SpriteManager()
{
	auto it = m_apxAnimatedTextures.begin();
	while (it != m_apxAnimatedTextures.end())
	{
		delete (*it);
		it++;
	}
	m_apxAnimatedTextures.clear();

	m_apxTextures.clear();
}


AnimatedTexture* SpriteManager::CreateAnimatedTexture(const std::string& p_sFilepath)
{
	auto it = m_apxTextures.find(p_sFilepath);
	if (it == m_apxTextures.end())
	{
		sf::Texture xTextures;
		xTextures.loadFromFile(p_sFilepath);

		m_apxTextures.insert(std::pair<std::string, sf::Texture>(p_sFilepath, xTextures));
		it = m_apxTextures.find(p_sFilepath);
	}
	AnimatedTexture* xAnimatedTexture = new AnimatedTexture (it->second);
	m_apxAnimatedTextures.push_back(xAnimatedTexture);
	
	return xAnimatedTexture;
}

sf::Texture SpriteManager::GetTexture(const std::string& p_sFilepath)
{
	auto it = m_apxTextures.find(p_sFilepath);
	if (it == m_apxTextures.end())
	{
		sf::Texture xTextures;
		xTextures.loadFromFile(p_sFilepath);

		m_apxTextures.insert(std::pair<std::string, sf::Texture>(p_sFilepath, xTextures));
		it = m_apxTextures.find(p_sFilepath);
	}
	return it->second;
}


void SpriteManager::LoadTextures()
{
	sf::Texture xTexture;
	xTexture.loadFromFile("../assets/ForegroundObjects.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/ForegroundObjects.png", xTexture));

	xTexture.loadFromFile("../assets/BackgroundUse1.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/BackgroundUse1.png", xTexture));

	xTexture.loadFromFile("../assets/BackgroundUse2.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/BackgroundUse2.png", xTexture));

	xTexture.loadFromFile("../assets/FishFood.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/FishFood.png", xTexture));

	xTexture.loadFromFile("../assets/FishFoodTemp.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/FishFoodTemp.png", xTexture));

	xTexture.loadFromFile("../assets/GoldenHarpoon.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/GoldenHarpoon.png", xTexture));

	xTexture.loadFromFile("../assets/GoldenHarpoon2.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/GoldenHarpoon2.png", xTexture));

	xTexture.loadFromFile("../assets/HealthBar.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/HealthBar.png", xTexture));

	xTexture.loadFromFile("../assets/HealthbarLong.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/HealthbarLong.png", xTexture));

	xTexture.loadFromFile("../assets/HealthbarPlank.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/HealthbarPlank.png", xTexture));

	xTexture.loadFromFile("../assets/JellyProjectile.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/JellyProjectile.png", xTexture));

	xTexture.loadFromFile("../assets/JellyRogerSwims.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/JellyRogerSwims.png", xTexture));

	xTexture.loadFromFile("../assets/Lungor.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Lungor.png", xTexture));

	xTexture.loadFromFile("../assets/MultiplierbarLong.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/MultiplierbarLong.png", xTexture));

	xTexture.loadFromFile("../assets/muspekare.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/muspekare.png", xTexture));

	xTexture.loadFromFile("../assets/MultiplierbarLong.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/MultiplierbarLong.png", xTexture));

	xTexture.loadFromFile("../assets/PiranhaSwims.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/PiranhaSwims.png", xTexture));
	
	xTexture.loadFromFile("../assets/PowerUpAmulett.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/PowerUpAmulett.png", xTexture));
	
	xTexture.loadFromFile("../assets/Rock_tiles.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Rock_tiles.png", xTexture));

	xTexture.loadFromFile("../assets/Rock_tiles_2.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Rock_tiles_2.png", xTexture));

	xTexture.loadFromFile("../assets/Rope.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Rope.png", xTexture));

	xTexture.loadFromFile("../assets/Sand_tiles.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Sand_tiles.png", xTexture));

	xTexture.loadFromFile("../assets/ScorePlank.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/ScorePlank.png", xTexture));

	xTexture.loadFromFile("../assets/Ship.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Ship.png", xTexture));

	xTexture.loadFromFile("../assets/SkipShooting.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/SkipShooting.png", xTexture));

	xTexture.loadFromFile("../assets/SkipSwims.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/SkipSwims.png", xTexture));

	xTexture.loadFromFile("../assets/Skitfulbubbla.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Skitfulbubbla.png", xTexture));

	xTexture.loadFromFile("../assets/Start_Menu.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Start_Menu.png", xTexture));

	xTexture.loadFromFile("../assets/Start_Menu.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Start_Menu.png", xTexture));

	xTexture.loadFromFile("../assets/Start_Menu_2.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Start_Menu_2.png", xTexture));

	xTexture.loadFromFile("../assets/SwordfishAlignCharge.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/SwordfishAlignCharge.png", xTexture));

	xTexture.loadFromFile("../assets/SwordfishSwiming.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/SwordfishSwiming.png", xTexture));

	xTexture.loadFromFile("../assets/TutorialScreen.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/TutorialScreen.png", xTexture));

	xTexture.loadFromFile("../assets/Waves.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Waves.png", xTexture));

	xTexture.loadFromFile("../assets/Bubblaforskip.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Bubblaforskip.png", xTexture));

	xTexture.loadFromFile("../assets/Asset_Plank2.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Asset_Plank2.png", xTexture));

	xTexture.loadFromFile("../assets/HealthbarlongBackground.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/HealthbarlongBackground.png", xTexture));

	xTexture.loadFromFile("../assets/Victory.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/Victory.png", xTexture));

	xTexture.loadFromFile("../assets/SkipElectrocution.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/SkipElectrocution.png", xTexture));

	xTexture.loadFromFile("../assets/VictoryGoldCrew.png");
	m_apxTextures.insert(std::pair<std::string, sf::Texture>("../assets/VictoryGoldCrew.png", xTexture));

}
