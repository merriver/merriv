#include "stdafx.h"
#include "Ship.h"

#include "SpriteManager.h"


Ship::Ship()
{
	
}

Ship::~Ship()
{

}

bool Ship::Initialize()
{
	m_xTexture.loadFromFile("../assets/Ship.png");
	m_xSprite.setTexture(m_xTexture);
	m_xSprite.setTextureRect(sf::IntRect(0, 0, 1350, 560));
	m_xSprite.setScale(-0.5, 0.5);
	
	m_fSpeed = 10;
	m_fX = 0 - m_xSprite.getGlobalBounds().width;
	m_fY = 0 - ((m_xSprite.getGlobalBounds().height / 2) - 50 );

	m_xSprite.setPosition(m_fX, m_fY);

	return true;
}

void Ship::Update(float p_fDeltaTime)
{
	m_xSprite.setPosition(m_fX += m_fSpeed, m_fY);
}

sf::Sprite Ship::GetSprite() { return m_xSprite; }

float Ship::GetX() { return m_fX; }

float Ship::GetY() { return m_fY; }

bool Ship::IsActive()
{
	return m_bActive;
}

void Ship::Deactivate()
{
	m_bActive = false;
}

void Ship::Activate()
{
	m_bActive = true;
	m_fX = 0 - m_xSprite.getGlobalBounds().width;
}
