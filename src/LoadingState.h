#pragma once
#include "IState.h"
#include "stdafx.h"


class LoadingState : public IState
{
public:
	LoadingState(System& p_xSystem);
	~LoadingState();
	void Draw();
	bool Update(float p_fDeltaTime);
	IState* NextState();

private:
	float m_fLoadingTimer;
	float m_fLogoSize;
	int width = 1920;
	int height = 1080;
	sf::Texture m_xLogoTexture;
	sf::Sprite m_xLogoSprite;
	sf::Font font;
	sf::Text m_xLoadingText;
	System m_xSystem;
};