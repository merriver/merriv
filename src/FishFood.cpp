#include "stdafx.h"
#include "FishFood.h"
#include "AnimatedTexture.h"
#include "SpriteManager.h"
#include "Utility.h"


FishFood::FishFood(SpriteManager* p_pxSpriteManager, sf::RenderWindow* p_pxWindow, float p_fX, float p_fY, int p_iScreenWidth, int p_iscreenHeight)
{
	m_pxAnimatedTexture = p_pxSpriteManager->CreateAnimatedTexture("../assets/fishfoodthrow.png");
	m_pxAnimatedTexture->AddFrame(0, 0, 288, 298, 0.5);
	m_pxAnimatedTexture->AddFrame(350, 0, 229, 296, 0.5);
	m_pxAnimatedTexture->AddFrame(628, 0, 235, 298, 5);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	m_pxSprite.setOrigin(50, 50);
	m_pxSprite.setScale(0.6, 0.6);
	m_fX = p_fX;
	m_fY = p_fY;
	m_fVelocity = 4;
	m_pxSprite.setPosition(m_fX, m_fY);

	sf::Vector2i mousePosition = sf::Mouse::getPosition(*p_pxWindow);
	m_fAngle = atan2(mousePosition.y - m_fY, mousePosition.x - m_fX);
}

FishFood::~FishFood()
{
}

void FishFood::Update(float p_fDeltaTime)
{
	m_pxAnimatedTexture->Update(p_fDeltaTime);
	m_pxSprite.setTexture(m_pxAnimatedTexture->GetTexture());
	m_pxSprite.setTextureRect(m_pxAnimatedTexture->GetRegion());
	float cosine = cos(m_fAngle);
	float sine = sin(m_fAngle);
	if (m_fY <= 1000)
	{
		m_pxSprite.setPosition(sf::Vector2f(m_fX -= 5, m_fY += 5));
	}
	else
	{
		m_pxSprite.setPosition(sf::Vector2f(m_fX -= 5, m_fY));
	}
}

sf::Sprite FishFood::GetSprite()
{
	return m_pxSprite;
}


float FishFood::GetX()
{
	return m_fX;
}

float FishFood::GetY()
{
	return m_fY;
}

EENTITYTYPE FishFood::GetType()
{
	return ENTITY_POWERUP;
}
