#pragma once

class DrawManager;
class SpriteManager;
class StateManager;
class InputManager;
class GameState;
class SoundManager;
class EnemyManager;
class ScoreManager;

class Engine
{
public:
	Engine();
	~Engine();
	bool Initialize(); 
	void Shutdown();
	void Update();

private:
	bool m_bRunning;
	InputManager* m_pxInputManager;
	DrawManager* m_pxDrawManager;
	SpriteManager* m_pxSpriteManager;
	StateManager* m_pxStateManager;
	SoundManager* m_pxSoundManager;
	EnemyManager* m_pxEnemyManager;
	ScoreManager* m_pxScoreManager;
};
