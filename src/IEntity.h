#pragma once

#include "stdafx.h"

// If we want to make more entity types we can keep adding it to this list, this is to distinguish what they really are if we
// store different class* that inherit from IEntity in an IEntity* vector for example.
enum EENTITYTYPE
{
	ENTITY_PLAYER,
	ENTITY_HARPOON,
	ENTITY_GOLDENHARPOON,
	ENTITY_SWORDFISH,
	ENTITY_POWERUP,
	ENTITY_POWERUP2,
	ENTITY_JELLYROGER,
	ENTITY_JELLYPROJECTILE,
	ENTITY_PIRANHA,
	ENTITY_BUBBLE,
	ENTITY_POWERUPHUD
	
};

class AnimatedTexture;

class IEntity
{
public:
	~IEntity() {};
	virtual sf::Sprite GetSprite() = 0;
	virtual float GetX() = 0;
	virtual float GetY() = 0;
	virtual EENTITYTYPE GetType() = 0;
};