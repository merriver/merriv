#pragma once
#include "IEntity.h"

class sf::Sprite;
class SpriteManager;
class Player;



class Piranha : public IEntity
{
public:
	Piranha();
	~Piranha();
	bool Initialize(SpriteManager* p_pxSpriteManager, float p_fX, float p_fY, int p_iScreenWidth, int p_iScreenHeight);
	void Update(float p_fDeltaTime, Player* p_pxPlayer);
	void DyingStateUpdate(float p_fDeltaTime);
	sf::Sprite GetSprite();
	AnimatedTexture* GetTexture();
	float GetX();
	float GetY();
	bool IsActive();
	void Kill();
	void Deactivate();
	void Activate();
	EENTITYTYPE GetType();
	sf::FloatRect GetCollitionRect();
	void ReverseDirectionY();
	bool IsDying();

private:
	
	AnimatedTexture* m_pxAnimatedTexture;
	AnimatedTexture* m_pxAnimatedTextureDying;
	sf::Sprite m_pxSprite;
	sf::FloatRect m_xCollitionRect;
	float m_fSpeed;
	float m_fDirY;
	float m_fDirX;
	float m_fY;
	float m_fX;
	float DyingTimer = 0.0;
	float DyingTimerTarget = 0.8;
	int m_iState;
	int m_iScreenWidth;
	int m_iScreenHeight;
	float m_fRand;
	bool m_bActive = false;
	bool m_bDying = false;
};