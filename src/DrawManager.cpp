#include "stdafx.h"
#include "DrawManager.h"
#include "Sprite.h"



DrawManager::DrawManager()
{
}

DrawManager::~DrawManager()
{

}

bool DrawManager::Initialize(int p_iWidht, int p_iHeight)
{
	m_pxview = new sf::View();
	m_pxview->setSize(1920, 1080);
	m_pxview->setCenter(960, 540);
	m_pxWindow = new sf::RenderWindow();
	m_pxWindow->create(sf::VideoMode(p_iWidht, p_iHeight), "Mermaid River v0.01", sf::Style::Default /* | sf::Style::Fullscreen*/);
	m_pxWindow->setMouseCursorVisible(false);
	m_pxWindow->setView(*m_pxview);
	
	return true;

}

void DrawManager::Shutdown()
{
	delete m_pxview;
	m_pxview = nullptr;

	m_pxWindow->close();
	delete m_pxWindow;
	m_pxWindow = nullptr;
}

void DrawManager::Clear()
{
	m_pxWindow->clear(sf::Color::Black);
}
//76fffa

//(0x11, 0x22, 0x33, 0xff)

void DrawManager::Present()
{
	m_pxWindow->display();
}

void DrawManager::Draw(sf::Sprite p_pxSprite)
{
	m_pxWindow->draw(p_pxSprite);
}
void DrawManager::DrawText(sf::Text p_pxText)
{
	m_pxWindow->draw(p_pxText);
}

void DrawManager::DrawText(sf::Text p_pxText, sf::BlendMode p_xBlend)
{
	m_pxWindow->draw(p_pxText, p_xBlend);
}


sf::View* DrawManager::GetView()
{
	return m_pxview;
}

void DrawManager::SetView()
{
	m_pxWindow->setView(*m_pxview);
}

sf::RenderWindow* DrawManager::GetRenderWindow()
{
	return m_pxWindow;
}
